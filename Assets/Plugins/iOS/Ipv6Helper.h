//
//  Ipv6Helper.h
//  ipv6
//
//  Created by n01192 on 2016/06/30.
//  Copyright © 2016年 n01192. All rights reserved.
//

#ifndef Ipv6Helper_h
#define Ipv6Helper_h


@interface Ipv6Helper : NSObject

+(NSString*) getIPv6 : (const char*)mHost : (const char *)mPort;

@end



#endif /* Ipv6Helper_h */
