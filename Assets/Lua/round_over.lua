--import 'UnityEngine.SceneManagement'

local round_over={}

--userScore

function round_over.Start()
	Debug.Log("main menu start")
	this = round_over.this
	round_over.Run()
end

function round_over.Run()
	local uiPrefab = Resources.Load("RoundOverUI")
	local guiOver = GameObject.Instantiate(uiPrefab)
	guiOver.name = "RoundOverUI"
  
  	--[[
	uiBg = guiMainMenu.transform:FindChild("Background")
	uiBgImage = uiBg:GetComponent("RawImage")
	local backgroundTex = Resources.Load("UI/image/background", Texture)
	uiBgImage.texture = backgroundTex
	Debug.Log("back loaded")
	--]]
  local uiRestartBnt=guiOver.transform:FindChild("Button_Restart/Button")
  local restartBtn=uiRestartBnt:GetComponent("Button")
  EventListener.Get(restartBtn.gameObject).onClick=round_over.onEnterRoom

  local uiGoHomeBtn=guiOver.transform:FindChild("Button_Home/Button")
  local homeBtn=uiGoHomeBtn:GetComponent("Button")
  EventListener.Get(homeBtn.gameObject).onClick=round_over.onMainMenu

  local uiScoreText=guiOver.transform:FindChild("bg/ScoreText")
  local uiText = uiScoreText:GetComponent("Text")
  Debug.LogError("uitext to show:" .. userScore)
  uiText.text = userScore

end

function round_over.onRestartGame(go)
	API.Broadcast("restart game")
end

function round_over.onMainMenu(go)
	SceneManager.LoadScene("MainMenu")
end


return round_over



















