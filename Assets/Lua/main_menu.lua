local main_menu={}

local uiCanvas
local uiBg
local uiBgImage
local uiNameText
local uiPasswordText


function main_menu.Start()
	Debug.Log("main menu start")
	this = main_menu.this
	main_menu.Run()
end

function main_menu.Run()
	local uiPrefab = Resources.Load("MainMenuUI")
	local guiMainMenu = GameObject.Instantiate(uiPrefab)
	guiMainMenu.name = "MainMenuUI"
  
	uiBg = guiMainMenu.transform:FindChild("Background")
	uiBgImage = uiBg:GetComponent("RawImage")
	local backgroundTex = Resources.Load("UI/image/background", Texture)
	uiBgImage.texture = backgroundTex
	Debug.Log("back loaded")

  local uistartBnt=guiMainMenu.transform:FindChild("Button_Play")
  local startBtn=uistartBnt:GetComponent("Button")
  EventListener.Get(startBtn.gameObject).onClick=main_menu.onEnterRoom

  local uiFriendSearchBnt=guiMainMenu.transform:FindChild("Button_SearchFriend")
  local friendSearchBtn=uiFriendSearchBnt:GetComponent("Button")
  EventListener.Get(friendSearchBtn.gameObject).onClick=main_menu.onSearchFriend

end

function main_menu.onEnterRoom(go)
	API.Broadcast("main menu start battle")
end

function main_menu.onSearchFriend(go)
	SceneManager.LoadScene("FriendSearch")
end

function main_menu.onNotif(go)
	SceneManager.LoadScene("Notification")
end

function main_menu.onFriendList(go)
	SceneManager.LoadScene("FriendList")
end

return main_menu



















