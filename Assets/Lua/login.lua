local login={}

local guiLogin

local routeIp = "106.185.52.250"
local routePort = 9701
--local routeIp = "192.168.56.101"

function login.Start()
	Debug.Log("login start")
	this = login.this
	login.Run()
end

function login.Run()
	local uiPrefab = Resources.Load("MainMenu/Prefab/MainMenuUI")
	guiLogin = GameObject.Instantiate(uiPrefab)
	guiLogin.name = "MainMenuUI"
  	--[[
	uiBg = guiLogin.transform:FindChild("Background")
	uiBgImage = uiBg:GetComponent("RawImage")
	local backgroundTex = Resources.Load("UI/image/background", Texture)
	uiBgImage.texture = backgroundTex
	Debug.Log("back loaded")

	Debug.Log(backgroundTex)

	Debug.Log(LoginScene)
	]]--

  
  local uistartBnt=guiLogin.transform:FindChild("PlayButton/FreeBattleButton")
  local startBtn=uistartBnt:GetComponent("Button")
  EventListener.Get(startBtn.gameObject).onClick=login.onEnterRoom

  local uiNotifBnt=guiLogin.transform:FindChild("Utility/ThreeButtons/NotifButton")
  local notifBtn=uiNotifBnt:GetComponent("Button")
  EventListener.Get(notifBtn.gameObject).onClick=login.onNotif

  local uiFriendBnt=guiLogin.transform:FindChild("Utility/ThreeButtons/FriendButton")
  local friendBtn=uiFriendBnt:GetComponent("Button")
  EventListener.Get(friendBtn.gameObject).onClick=login.onFriend

  local uiAdBtn=guiLogin.transform:FindChild("Utility/btn_free_coin")
  local adBtn=uiAdBtn:GetComponent("Button")
  EventListener.Get(adBtn.gameObject).onClick=login.onAd

  local uiMyPageBtn=guiLogin.transform:FindChild("UserPhoto")
  local myPageBtn=uiMyPageBtn:GetComponent("Button")
  EventListener.Get(myPageBtn.gameObject).onClick=login.onMyPage
  
  --LoginScene.mInstance:Test()

	--local para = ConnectionPara(routeIp, routePort)

	local para = {}
	para["routePort"] = routePort
	para["routeIp"] = routeIp
  
  API.AddListener("login reset", login.reset)
  API.AddListener("login draw user info", login.drawUserInfo)
	
	if(first_execute==false) then  
		Debug.LogError("lua not first execut login")
		login.drawUserInfo()
   	else
      Debug.LogError("lua first execut login")
   		API.Broadcast("login start login server", para)    		
   	end

end

--user_photo
--user_name
--user_lv
--user_gold
--user_has_notif

function login.drawUserInfo()

  local uiName=guiLogin.transform:FindChild("UserName/UserNameInput")
  local uiNameInput=uiName:GetComponent("InputField")
  local uiNameText = uiName.transform:FindChild("Text")
  local nameText = uiNameText:GetComponent("Text")
  uiNameInput.text = user_name
  --nameText.text = user_name
  login.onBindChangeName(uiNameInput)

  local uiCoin=guiLogin.transform:FindChild("CoinPanel/CoinNumText")
  local coinText=uiCoin:GetComponent("Text")
  coinText.text = user_gold

  local uiNotifIcon = guiLogin.transform:FindChild("Utility/ThreeButtons/NotifButton/NotifIcon")
  if(user_has_notif == true) then
    uiNotifIcon.gameObject:SetActive(true)
  else
    uiNotifIcon.gameObject:SetActive(false)
  end


  -- shop
  
  local uiShopBnt=guiLogin.transform:FindChild("Utility/ThreeButtons/btn_shop")
  local shopBtn=uiShopBnt:GetComponent("Button")
  EventListener.Get(shopBtn.gameObject).onClick=login.onShop

  local uiShopBnt=guiLogin.transform:FindChild("CoinPanel/btn_plus")
  local shopBtn=uiShopBnt:GetComponent("Button")
  EventListener.Get(shopBtn.gameObject).onClick=login.onShop



  local uiPhoto=guiLogin.transform:FindChild("UserPhoto/UserPhotoImage")
  local photoImage=uiPhoto:GetComponent("Image")
  local sprite = Sprite.Create(user_photo, Rect(0, 0, user_photo.width, user_photo.height), Vector2(0.5, 0.5))
  photoImage.sprite = sprite;
  photoImage.color = Color(1,1,1,1)


  local uiLv=guiLogin.transform:FindChild("UserPhoto/UserLv/LvText")
  local uiLvText = uiLv:GetComponent("Text")  
  uiLvText.text = "lv." .. user_lv

  Debug.Log("user lv" .. user_lv);

end

function login.onShop(go)
  Debug.LogError("on shop click")
	SceneManager.LoadScene("Shop");
end

function login.onBindChangeName(go)
  local name = go.text
  Debug.LogError("change name:" .. go.text)
  
	local para = {}
	para["input"] = go
  
  API.Broadcast("login change name", para) 
  
end
  

function login.onEnterRoom(go)
	API.Broadcast("main menu start battle")
end

function login.onSearchFriend(go)
	SceneManager.LoadScene("Friend")
end

function login.onNotif(go)
	Debug.LogError("notif clicked")
	SceneManager.LoadScene("notification");
end

function login.onFriend(go)
	SceneManager.LoadScene("Friend")
end

function login.onMyPage(go)
  API.Broadcast("login mypage") 
end

function login.onAd(go)
  Debug.LogError("play ad")
  	API.Broadcast("play ad")

end

function login.reset()
  Debug.LogError("reset")
  API.RemoveListener("login draw user info", login.drawUserInfo)
  API.RemoveListener("login reset", login.reset)
end

return login



















