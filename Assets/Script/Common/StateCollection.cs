﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StatePara = System.Collections.Generic.Dictionary<string, System.Object>;

public class StateCollection<State>{

	private Dictionary<State, StateGod> mStates;


	public StateCollection(){
	
		mStates = new Dictionary<State, StateGod> ();

	}

	public void AddStateHandler(State state, StateGod stateObject){	
		mStates.Add (state, stateObject);	
	}

	public void ExecStateEnter(State state, StatePara para){
	
		StateGod stateExecutor = mStates[state];
		stateExecutor.EnterState (para);	
	}

	public void ExecStateUpdate(State state, StatePara para){

		StateGod stateExecutor = mStates [state];
		stateExecutor.StateUpdate (para);
	}

	public void ExecStateExit(State state, StatePara para){
		StateGod stateExecutor = mStates [state];
		stateExecutor.ExitState (para);
	}

	public bool HasExecutor(State state){
	
		//Debug.Log("has executor: " + mStates.ContainsKey(state));


		if (mStates.ContainsKey (state)) {
			return true;
		} else {
			return false;
		}
	
	}


}
