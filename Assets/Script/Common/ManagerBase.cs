﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using MsgPack;

public abstract class ManagerBase<TManager, TData> : MonoBehaviour 
	where TManager: ManagerBase<TManager, TData>
{


	protected static TManager instance = null;
	public static TManager Instance{
	
		get{
			return ManagerBase<TManager, TData>.GetInstance();
		}	
	}

	public static bool IsExistInstance{
		get{
			return (instance != null);
		}
	}

	public ManagerBase(string databaseName){
		this.DatabaseName = databaseName;
	}

	public static readonly string managerName = typeof(TManager).Name;

	public string DatabaseName { get; protected set; }

	protected Dictionary<string, TData> Dic { get; set; }


	public static TManager GetInstance(){
		if (instance == null) {
			GameObject obj = new GameObject ();
			obj.AddComponent<TManager> ();
			obj.name = ManagerBase<TManager, TData>.managerName;
		}
		return instance;
	}

	public void OnApplicationQuit(){

		if (instance == null) {
			Destroy(instance);
		}
	}

	public void Awake(){
		if (instance != null) {
			Destroy (this.gameObject);
			return;
		}

		DontDestroyOnLoad (this.gameObject);

		instance = this as TManager;

		this.Dic = new Dictionary<string, TData> ();		
	
	}

	public bool Load (){
	
		string fileName = string.Format("{0}_o0_r0_en.db", this.DatabaseName);
		string path = Application.dataPath + "/Save/Cache/";

		if (fileName.Contains ("assetbundles")) {
		
			fileName = string.Format("{0}_o0_r0_all.db", this.DatabaseName);
		
		}

		bool ret = Utility.LoadLocalFile (path, fileName, delegate(Stream stream) {
		
			/*ObjectPacker objPacker = new ObjectPacker();
			string str = (string) objPacker.Unpack(typeof(string), stream);
			int value = int.Parse((string)(objPacker.Unpack(typeof(string), stream)));

			Dictionary<string, TData> tmpDic = new Dictionary<string, TData>();
			long tmpCount = long.Parse((string)objPacker.Unpack(typeof(string), stream));

			for(long i = 0; i < tmpCount; i++){
				string tid = (string) objPacker.Unpack(typeof(string), stream);
				TData data = (TData) objPacker.Unpack(typeof(TData), stream);
				tmpDic.Add(tid, data);			
			}
			this.Dic = tmpDic;*/

			return true;
		});
		return true;
	
	}

	public TData Get(string id){

		return Dic [id];
	
	}

	public TData Get(int id){
		return Dic [id.ToString ()];
	}

	public static Dictionary<string, TData>.ValueCollection AllData{
		get{
			Dictionary<string, TData> dic = GetInstance().Dic;
			return dic.Values;
		}
	}


}
































