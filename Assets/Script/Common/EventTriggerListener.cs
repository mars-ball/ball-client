﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;



public class EventTriggerListener : UnityEngine.EventSystems.EventTrigger{
	public delegate void VoidDelegate (GameObject go);
	public delegate void VectorDelegate(GameObject go, Vector2 delta);  

	public VoidDelegate onClick;
	public VoidDelegate onDown;
	public VoidDelegate onEnter;
	public VoidDelegate onExit;
	public VoidDelegate onUp;
	public VoidDelegate onSelect;
	public VoidDelegate onUpdateSelect;

	public VoidDelegate onMouseDown;
	public VoidDelegate onMouseUp;

	public VectorDelegate onDrag;
	public VectorDelegate onDragOut;
	
	static public EventTriggerListener Get (GameObject go)
	{
		EventTriggerListener listener = go.GetComponent<EventTriggerListener>();
		if (listener == null) listener = go.AddComponent<EventTriggerListener>();
		return listener;
	}
	public override void OnPointerClick(PointerEventData eventData)
	{
		if(onClick != null) 	onClick(gameObject);
		//Debug.LogError ("click");
	}
	public override void OnPointerDown (PointerEventData eventData){
		if(onDown != null) onDown(gameObject);
		//Debug.LogError ("OnPointerDown");
	}
	public override void OnPointerEnter (PointerEventData eventData){
		if(onEnter != null) onEnter(gameObject);
		//Debug.LogError ("OnPointerEnter");
	}
	public override void OnPointerExit (PointerEventData eventData){
		if(onExit != null) onExit(gameObject);
		//Debug.LogError ("OnPointerExit");
	}
	public override void OnPointerUp (PointerEventData eventData){
		if(onUp != null) onUp(gameObject);
		//Debug.LogError ("OnPointerUp");
	}

	public override void OnSelect (BaseEventData eventData){
		if(onSelect != null) onSelect(gameObject);
		//Debug.LogError ("OnSelect");
	}
	public override void OnUpdateSelected (BaseEventData eventData){
		if(onUpdateSelect != null) onUpdateSelect(gameObject);
		//Debug.LogError ("OnUpdateSelected");
	}
	public override void OnDrag(PointerEventData eventData)  		
	{  		
		if(onDrag != null) onDrag(gameObject, eventData.delta); 		
	}  	
	
	public override void OnEndDrag(PointerEventData eventData)  
	{  		
		if(onDragOut != null) onDragOut(gameObject, eventData.delta);  		
	}  
}


