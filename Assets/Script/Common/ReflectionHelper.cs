﻿/**
 * @file     ReflectionHelper.cs
 * @brief    デバッグ用の選手基底
 * @date     $Date$
 * @version  $Rev$
 * @author   $Author$
 */

using UnityEngine;
using System.Collections;
using System.Reflection;
using System;


/**
 * クラスのメンバーを取る		
 * Debug用 
 */

public static class ReflectionHelper
{
	public static object CallStaticMethodByName(this object obj, string methodName){
		return CallStaticMethodByNameArgs(obj, methodName, new object[]{});


	}


	//------------------------------------------------------------
	/// <summary>
	///	static方法を呼ぶ	<br/>
	/// </summary>
	/// <param name="methodName">方法の名前</param>
	/// <param name="args">パラメーター</param>
	/// <returns>結果</returns>
	//------------------------------------------------------------
	public static object CallStaticMethodByNameArgs(this object obj, string methodName, object[] args){
		Type objType = obj.GetType ();
		MethodInfo[] mms = objType.GetMethods();
		MethodInfo mi = null;

		foreach(MethodInfo info in mms){
			if(info.Name == "Deserialize"){
				mi = info;
				break;
			}
		}
		
		if(mi == null){
			
			Debug.LogError("no such method");
			return null;
		}
		
		object res = mi.Invoke(null, args);	
		return res;
	
	}


	//------------------------------------------------------------
	/// <summary>
	///	値を設定する	<br/>
	/// </summary>
	/// <returns>パス。</returns>
	//------------------------------------------------------------
	public static void SetFieldValue(this object obj, string fieldName, object val){
	
		Type objType = obj.GetType ();
		FieldInfo info = GetFieldInfo (objType, fieldName);
		if (info == null) {
			Debug.LogError ("field not found");
		}

		info.SetValue(obj, val);
	
	}

	//------------------------------------------------------------
	/// <summary>
	///	メンバーの情報を取得する	<br/>
	/// </summary>
	/// <returns>パス。</returns>
	//------------------------------------------------------------
	private static FieldInfo GetFieldInfo(Type type, string fieldName)
	{
		FieldInfo fieldInfo = null;
		do {
			fieldInfo = type.GetField (fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
			FieldInfo[] infos = type.GetFields (BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
			type = type.BaseType;
		} while(fieldInfo == null && type != null);
		return fieldInfo;
	}

	//------------------------------------------------------------
	/// <summary>
	///	メンバーの値を取得する	<br/>
	/// </summary>
	/// <returns>パス。</returns>
	//------------------------------------------------------------
	public static object GetFieldValue(this object obj, string fieldName){
		Type objType = obj.GetType ();

		FieldInfo info = GetFieldInfo (objType, fieldName);
		if (info == null) {
			Debug.LogWarning ("failed to search for info");
		}
		return info.GetValue(obj);

	}

	public static Type GetFieldType(this object obj, string fieldName){
		Type objType = obj.GetType ();

		FieldInfo info = GetFieldInfo (objType, fieldName);
		if (info == null) {
			Debug.LogWarning ("failed to search for info");
		}
		return info.GetType ();

	}


	/*
	//------------------------------------------------------------
	/// <summary>
	///	メンバーの値を取得する	<br/>
	/// </summary>
	/// <returns>パス。</returns>
	//------------------------------------------------------------
	private static PropertyInfo GetPropertyInfo(Type type, string propertyName)
	{
		PropertyInfo propInfo = null;
		do
		{
			propInfo = type.GetProperty(propertyName,
			                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			type = type.BaseType;
		}
		while (propInfo == null && type != null);
		return propInfo;
	}

	//------------------------------------------------------------
	/// <summary>
	///	メンバーの値を取得する	<br/>
	/// </summary>
	/// <returns>パス。</returns>
	//------------------------------------------------------------
	public static object GetPropertyValue(this object obj, string propertyName)
	{
		if (obj == null)
			throw new ArgumentNullException("obj");
		Type objType = obj.GetType();

		PropertyInfo propInfo = GetPropertyInfo(objType, propertyName);
		if (propInfo == null)
			throw new ArgumentOutOfRangeException("propertyName",
			                                      string.Format("Couldn't find property {0} in type {1}", propertyName, objType.FullName));
		return propInfo.GetValue(obj, null);
	}
	
	public static void SetPropertyValue(this object obj, string propertyName, object val)
	{
		if (obj == null)
			throw new ArgumentNullException("obj");
		Type objType = obj.GetType();
		PropertyInfo propInfo = GetPropertyInfo(objType, propertyName);
		if (propInfo == null)
			throw new ArgumentOutOfRangeException("propertyName",
			                                      string.Format("Couldn't find property {0} in type {1}", propertyName, objType.FullName));
		propInfo.SetValue(obj, val, null);
	}
	*/
}