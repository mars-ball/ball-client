#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
// #define RECEIPT_VALIDATION
#endif

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif

// Reference the Unity Analytics namespace
using UnityEngine.Analytics;

/// <summary>
/// An example of basic Unity IAP functionality.
/// To use with your account, configure the product ids (AddProduct)
/// and Google Play key (SetPublicKey).
/// </summary>
public class IAP :  IStoreListener
{

	private static IAP m_instance;

	public static IAP getInstance(){
	
		if (m_instance == null) {
			m_instance = new IAP ();
		}
		return m_instance;
	}


	private IAP(){
		m_isInit = false;
	}

	// Unity IAP objects 
	public IStoreController m_Controller;
	private IAppleExtensions m_AppleExtensions;

	public bool m_isInit;

	public int m_itemId;
	public int m_itemIndex;

	public delegate bool OnBuyVerify(int itemId, string receipt, PLATFORM_TYPE platform);
	public OnBuyVerify m_onBuyVerify;

	public delegate void OnBuyComplete(bool ok);
	public OnBuyComplete m_onBuyComplete;

	#if RECEIPT_VALIDATION
	private CrossPlatformValidator validator;
	#endif

	public void Buy(int itemId, int itemIndex){
		if(itemIndex < 0 || itemIndex >= m_Controller.products.all.Length){
			Debug.LogError("item index failed, itemid:" + itemId + " itemIndex:" + itemIndex);
			return;
		}

		m_itemId = itemId;
		m_itemIndex = itemIndex;

		m_Controller.InitiatePurchase(m_Controller.products.all[itemIndex]); 
	}


	/// <summary>
	/// This will be called when Unity IAP has finished initialising.
	/// </summary>
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_Controller = controller;
		m_AppleExtensions = extensions.GetExtension<IAppleExtensions> ();

		m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);

		m_isInit = true;

		Debug.Log("Available items:");
		foreach (var item in controller.products.all)
		{
			if (item.availableToPurchase)
			{
				Debug.Log(string.Join(" - ",
					new[]
					{
						item.metadata.localizedTitle,
						item.metadata.localizedDescription,
						item.metadata.isoCurrencyCode,
						item.metadata.localizedPrice.ToString(),
						item.metadata.localizedPriceString
					}));
			}
		}
	}

	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		Debug.Log("Purchase OK: " + e.purchasedProduct.definition.id);
		Debug.Log("Receipt: " + e.purchasedProduct.receipt);

		PLATFORM_TYPE platform = PLATFORM_TYPE.PLATFORM_TYPE_INVALID;

		if(Application.platform == RuntimePlatform.Android){
			platform = PLATFORM_TYPE.PLATFORM_TYPE_ANDROID;
		}else if(Application.platform == RuntimePlatform.IPhonePlayer){
			platform = PLATFORM_TYPE.PLATFORM_TYPE_IPHONE;
		}

		// Use this call for each and every place that a player triggers a monetization event
//		Analytics.Transaction(string productId, decimal price,
//			string currency, string receipt,
//			string signature);

		Analytics.Transaction(m_Controller.products.all[m_itemIndex].metadata.localizedTitle, 
							  m_Controller.products.all[m_itemIndex].metadata.localizedPrice, 
							  m_Controller.products.all[m_itemIndex].metadata.localizedPriceString, 
							  null, 
							  null);

		/*
		if(m_onBuyVerify != null){
			if(m_onBuyComplete != null){
				if(m_onBuyVerify(m_itemId, e.purchasedProduct.receipt, platform)){
					m_onBuyComplete(true);
				}else{
					m_onBuyComplete(false);
				}			
			}
		}*/

		if(m_onBuyVerify != null){
			m_onBuyVerify(m_itemId, e.purchasedProduct.receipt, platform);
		}

		#if RECEIPT_VALIDATION
		if (Application.platform == RuntimePlatform.Android ||
			Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) {
			try {
				var result = validator.Validate(e.purchasedProduct.receipt);
				Debug.Log("Receipt is valid. Contents:");
				foreach (IPurchaseReceipt productReceipt in result) {
					Debug.Log(productReceipt.productID);
					Debug.Log(productReceipt.purchaseDate);
					Debug.Log(productReceipt.transactionID);

					GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google) {
						Debug.Log(google.purchaseState);
						Debug.Log(google.purchaseToken);
					}

					AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple) {
						Debug.Log(apple.originalTransactionIdentifier);
						Debug.Log(apple.cancellationDate);
						Debug.Log(apple.quantity);
					}
				}
			} catch (IAPSecurityException) {
				Debug.Log("Invalid receipt, not unlocking content");
				return PurchaseProcessingResult.Complete;
			}
		}
		#endif

		// You should unlock the content here.

		// Indicate we have handled this purchase, we will not be informed of it again.x
		return PurchaseProcessingResult.Complete;
	}

	/// <summary>
	/// This will be called is an attempted purchase fails.
	/// </summary>
	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Debug.Log("Purchase failed: " + item.definition.id);
		Debug.Log(r);

	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("Billing failed to initialize!");
		switch (error)
		{
			case InitializationFailureReason.AppNotKnown:
				Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
				break;
			case InitializationFailureReason.PurchasingUnavailable:
				// Ask the user if billing is disabled in device settings.
				Debug.Log("Billing disabled!");
				break;
			case InitializationFailureReason.NoProductsAvailable:
				// Developer configuration error; check product metadata.
				Debug.Log("No products available for purchase!");
				break;
		}
	}

	public void Init()
	{
		if (m_isInit) {
			return;
		}

		var module = StandardPurchasingModule.Instance();

		// The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
		// developer ui (initialization, purchase, failure code setting). These correspond to 
		// the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
		module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

		var builder = ConfigurationBuilder.Instance(module);
		// This enables the Microsoft IAP simulator for local testing.
		// You would remove this before building your release package.
		builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;
		builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAniVuFj3+iYWn/4GTwip0uX02d9+C/F7VvNVfLGI4ySRG4Rcj3RF+u83iXF/MzgyZYU8fZbSIRPgtIDnwiF9a9WovxqzBcfvptyAEZYufRaHbNmW2CRtxDVWb7yjyfw1NTYK8tRsiokUOXgbUBhV/dKQzppuOSOu8xzYc8b686Ga9bK51YW7cQe/qTnsJBzaoGHZB4PGt6nntDi1CTIlX8+lUy780DuINnQ/TAyODCM1lbnt5kmmj2JajlJH/g02JIMWtsTXR7Px1sXps48eZXv2xPFceaSsvWqxpxY4ZjZaABFICNlliLjAOubTf3d3Z0ar7YeVbTS+AKZnOGQKTJwIDAQAB");

		// Define our products.
		// In this case our products have the same identifier across all the App stores,
		// except on the Mac App store where product IDs cannot be reused across both Mac and
		// iOS stores.
		// So on the Mac App store our products have different identifiers,
		// and we tell Unity IAP this by using the IDs class.
		builder.AddProduct("100.gold.coins", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.100", GooglePlay.Name},
				{"brave.bubble.final.100", AppleAppStore.Name},
			});
		builder.AddProduct("200.gold.coins", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.200", GooglePlay.Name},
				{"brave.bubble.final.200", AppleAppStore.Name},
			});
		builder.AddProduct("300.gold.coins", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.300", GooglePlay.Name},
				{"brave.bubble.final.300", AppleAppStore.Name},
			});
		builder.AddProduct("500.gold.coins", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.500", GooglePlay.Name},
				{"brave.bubble.final.500", AppleAppStore.Name},
			});
		builder.AddProduct("800.gold.coins", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.800", GooglePlay.Name},
				{"brave.bubble.final.800", AppleAppStore.Name},
			});
		builder.AddProduct("1000.gold.coins=", ProductType.Consumable, new IDs
			{
				{"brave.bubble.final.1000", GooglePlay.Name},
				{"brave.bubble.final.1000", AppleAppStore.Name},
			});

		builder.AddProduct("finishAdd", ProductType.NonConsumable, new IDs
			{
				{"brave.bubble.final.adremoval", GooglePlay.Name},
				{"brave.bubble.final.adremoval", AppleAppStore.Name},
			});
		
//		builder.AddProduct("Ad.removal", ProductType.NonConsumable, new IDs
//			{
//				{"brave.bubble.final.adremoval", GooglePlay.Name}
//			});
//		builder.AddProduct("subscription", ProductType.Subscription, new IDs
//			{
//				{"brave.bubble.final.subscription", GooglePlay.Name}
//			});


		// Write Amazon's JSON description of our products to storage when using Amazon's local sandbox.
		// This should be removed from a production build.
		builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);

		#if RECEIPT_VALIDATION
		validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
		#endif

		// Now we're ready to initialize Unity IAP.
		UnityPurchasing.Initialize(this, builder);
	}

	/// <summary>
	/// This will be called after a call to IAppleExtensions.RestoreTransactions().
	/// </summary>
	private void OnTransactionsRestored(bool success)
	{
		Debug.Log("Transactions restored.");
	}

	/// <summary>
	/// iOS Specific.
	/// This is called as part of Apple's 'Ask to buy' functionality,
	/// when a purchase is requested by a minor and referred to a parent
	/// for approval.
	/// 
	/// When the purchase is approved or rejected, the normal purchase events
	/// will fire.
	/// </summary>
	/// <param name="item">Item.</param>
	private void OnDeferred(Product item)
	{
		Debug.Log("Purchase deferred: " + item.definition.id);
	}


}