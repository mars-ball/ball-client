﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;


[System.AttributeUsage( System.AttributeTargets.Field)]
public class CallBackClassAttribute:System.Attribute{
	
	public Type mType {
		get;
		private set;
	}
	
	public CallBackClassAttribute(Type type){
		this.mType = type;
	}	
}

public static class CallBackClassAttributeExtensions{
	
	public static Type GetType(this Enum value){
		string name = value.ToString ();
		FieldInfo info = value.GetType ().GetField (name);
		CallBackClassAttribute attr = (CallBackClassAttribute)System.Attribute.GetCustomAttribute (info, typeof(CallBackClassAttribute));
		return attr.mType;
		
	}
}
