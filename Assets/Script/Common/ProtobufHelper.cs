﻿using UnityEngine;
using System.Collections;
using ProtoBuf;
using System.IO;

public class ProtobufHelper : MonoBehaviour {
	
	private T ReadOneDataConfig<T>(string fileName){
		FileStream fileStream;
		fileStream = GetDataFileStream (fileName);
		if (null != fileName) {
			T t = Serializer.Deserialize<T>(fileStream);
			//Serializer.S
			fileStream.Close();
			return t;
		}
		return default(T);
	}

	private FileStream GetDataFileStream(string fileName){
		string filePath = GetDataConfigPath (fileName);
		if (File.Exists (filePath)) {
			FileStream fileStream = new FileStream(filePath, FileMode.Open);
			return fileStream;		
		}
		return null;
	}


	private string GetDataConfigPath(string fileName){
		return Application.streamingAssetsPath + "/DataConfig/" + fileName + ".data";
	}





}
