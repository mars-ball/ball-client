﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;


[System.AttributeUsage( System.AttributeTargets.Field)]
public class TypeBindAttribute:System.Attribute{
	
	public Type mType {
		get;
		private set;
	}
	
	public TypeBindAttribute(Type type){
		this.mType = type;
	}	
}

public static class TypeBindAttributeExtensions{
	
	public static Type GetTypeX(this Enum value){
		string name = value.ToString ();
		FieldInfo info = value.GetType ().GetField (name);
		TypeBindAttribute attr = (TypeBindAttribute)System.Attribute.GetCustomAttribute (info, typeof(TypeBindAttribute));
		return attr.mType;
		
	}
}
