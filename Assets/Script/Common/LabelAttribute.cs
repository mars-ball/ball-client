﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;


[System.AttributeUsage( System.AttributeTargets.Field)]
public class LabelAttribute:System.Attribute{

	public string mLabel {
		get;
		private set;
	}

	public LabelAttribute(string label){
		this.mLabel = label;
	}


}

public static class LabelAttributeExtensions{

	public static string GetLabel(this Enum value){
		string name = value.ToString ();
		FieldInfo info = value.GetType ().GetField (name);
		LabelAttribute attr = (LabelAttribute)System.Attribute.GetCustomAttribute (info, typeof(LabelAttribute));
		return attr.mLabel;
		
	}
}
