﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Security.Cryptography;


public class Utility : MonoBehaviour {

	public delegate bool CallBackLoadFile(Stream stream);
	public delegate bool CallbackSaveFile();

	public static readonly int READ_BUFFER_SIZE = 1024 * 10;

	private static readonly int ENCRYPT_RIJNDAEL_SALT_LENGTH = 8;
	private static readonly int HASH_SIZE = 256/8;

	private static readonly string[] LOCAL_SAVS = {"UserBasicData"};
	public static readonly string LOCAL_SAVE_EXT = ".sav";
	public static readonly string LOCAL_DATA_EXT = ".data";
	
	public static string GetCachePath(){
		string path = Application.persistentDataPath + "/Save/Cache";
		return path;	
	}

	private static byte[] EncodeByRijindael(byte[] bin, byte[] salt){

		return RijndaelEnhanced.GetInstance().EncryptToBytes(bin);

		/*
		RijndaelManaged rijn = new RijndaelManaged ();

		byte[] key, iv;

		string phrase = "(wQL/^xpT9pha5xWZ:gD8CNj90ekfh+4";

		Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes (phrase, salt);

		deriveBytes.IterationCount = 1;

		key = deriveBytes.GetBytes (rijn.KeySize / 8);
		iv = deriveBytes.GetBytes (rijn.BlockSize / 8);

		rijn.Key = key;
		rijn.IV = iv;

		MemoryStream memStream = new MemoryStream ();
		ICryptoTransform decryptor = rijn.CreateDecryptor ();
		CryptoStream cryptoStream = new CryptoStream (memStream, decryptor, CryptoStreamMode.Write);
		int len;
		cryptoStream.Write(bin, 0, bin.Length);
		cryptoStream.FlushFinalBlock();

		byte[] result = memStream.ToArray();

		memStream.Close ();
		cryptoStream.Close ();
		decryptor.Dispose ();

		return result;*/
	}


	private static byte[] DecodeByRijndael(byte[] bin, byte[] salt){
		return RijndaelEnhanced.GetInstance().DecryptToBytes(bin);
		/*
		RijndaelManaged rijn = new RijndaelManaged ();

		byte[] key, iv;

		string phrase = "(wQL/^xpT9pha5xWZ:gD8CNj90ekfh+4";

		Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes (phrase, salt);

		deriveBytes.IterationCount = 1;

		key = deriveBytes.GetBytes (rijn.KeySize / 8);
		iv = deriveBytes.GetBytes (rijn.BlockSize / 8);

		rijn.Key = key;
		rijn.IV = iv;

		MemoryStream memStream = new MemoryStream (bin);
		ICryptoTransform decryptor = rijn.CreateDecryptor ();
		CryptoStream cryptoStream = new CryptoStream (memStream, decryptor, CryptoStreamMode.Read);
		MemoryStream outStream = new MemoryStream ();
		byte[] tmp = new byte[READ_BUFFER_SIZE];
		int len;
		while ((len = cryptoStream.Read(tmp, 0, tmp.Length)) > 0) {
			outStream.Write(tmp, 0, len);
		}

		memStream.Close ();
		cryptoStream.Close ();
		decryptor.Dispose ();
		outStream.Close ();

		return outStream.ToArray ();*/
	}


	private static byte[] Decode(byte[] bin){
	
		int index = 0;

		byte[] salt = new byte[ENCRYPT_RIJNDAEL_SALT_LENGTH];
		Array.Copy (bin, index, salt, 0, salt.Length);

		index += salt.Length;

		int dataSize = bin.Length - index;

		byte[] data = new byte[dataSize];

		Array.Copy (bin, index, data, 0, dataSize);

		byte[] res = DecodeByRijndael (data, salt);

		return res;
	
	}


	private static byte[] Encode(byte[] bin){

		byte[] salt = new byte[ENCRYPT_RIJNDAEL_SALT_LENGTH];

		for (int i = 0; i < salt.Length; i++) {

			salt [i] = (byte)UnityEngine.Random.Range (Byte.MinValue, Byte.MaxValue);
		}
		byte[] coded = EncodeByRijindael (bin, salt);

		byte[] res = new byte[coded.Length + ENCRYPT_RIJNDAEL_SALT_LENGTH];
		Array.Copy (salt, 0, res, 0, ENCRYPT_RIJNDAEL_SALT_LENGTH);
		Array.Copy (coded, 0, res, ENCRYPT_RIJNDAEL_SALT_LENGTH, coded.Length);

		return res;

	}

	public static bool HasLocalSaveAll(){
		foreach (string file in LOCAL_SAVS) {
			string fullname = Utility.GetCachePath() + "/" + file + LOCAL_SAVE_EXT;
			if(!File.Exists(fullname)){
				return false;
			}else{
				return true;
			}	
		}
		return true;
	}


	public static bool SaveLocalFile(string path, string filename, byte[] data, CallbackSaveFile callback){
		try{
			if(!Directory.Exists(path)){
			
				Directory.CreateDirectory(path);
				Debug.LogError("create folder:" + path);
			}


			string fullpath = path + "/" + filename;

			Debug.LogError("fullpath:" + fullpath + " path:" + path);

			using(Stream stream = File.Open(fullpath, FileMode.OpenOrCreate)){
				//byte[] coded = Encode (data);

				stream.Write(data, 0, data.Length);			
			}



		}catch(IOException e){
			Debug.LogError("save file error " + filename + "  " + e.Message);
			return false;
		}
		return true;
	}


	public static bool LoadLocalFile(string path, string filename, CallBackLoadFile callback){
	
		bool ret = false;

		try{

			string fullPath = path + "/" + filename;

			using(Stream stream = File.Open(fullPath, FileMode.Open)){
				MemoryStream memStream = new MemoryStream();
				byte[] buf = new byte[READ_BUFFER_SIZE];
				int readLen;
				while((readLen = stream.Read (buf, 0, buf.Length)) > 0){			//need repair
					memStream.Write(buf, 0, readLen);				
				}

				memStream.Close();
				byte[] bin = memStream.ToArray();
				stream.Close();

				//bin = Decode(bin);

				MemoryStream decodedStream = new MemoryStream(bin);
				ret = callback(decodedStream);		
			
			}
		}catch(IOException e){
			Debug.LogError("load file error " + filename);
		}

		return ret;
	
	}
}

