﻿using UnityEngine;
using System.Collections;
using StatePara = System.Collections.Generic.Dictionary<string, System.Object>;


public class StateGod{

	//private Comm mComm;

	public StateGod(){
		//mComm = Comm.GetInstance ();	
	}

	public virtual void EnterState(StatePara para){
	}
	public virtual void ExitState(StatePara para){
	}
	public virtual void StateUpdate(StatePara para){
	}
}
