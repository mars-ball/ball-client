﻿using UnityEngine;
using System.Collections;
using System;

public class Singleton<T>{

	private static T instance;

	static public T GetInstance(){

		if(instance == null){
			instance = (T)Activator.CreateInstance(typeof(T), new object[]{});
		}
		return instance;
	}

	protected Singleton(){
	}


}
