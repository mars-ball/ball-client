﻿public class Pair<T, U>	{
	public Pair() {
	}

	public Pair(T first, U second) {
		this.First = first;
		this.Second = second;
	}

	public override int GetHashCode()
	{
		return First.GetHashCode();
	}

	public override bool Equals(object obj)
	{
		Pair<T, U> other = obj as Pair<T, U>;
		return other != null && other.First.Equals(this.First) && other.Second.Equals(this.Second);
	}


	public override string ToString()
	{
		return "Pair:" + First.ToString() + ", " + Second.ToString();
	}

	public T First { get; set; }
	public U Second { get; set; }
};