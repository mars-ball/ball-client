﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class HTime: MonoBehaviour{

	public static Int64 m_serverPacketTime;

	public static Int64 m_serverToLocalTimeDelta;


	public static void SetServerPacketTime(Int64 packetTime){
		m_serverPacketTime = packetTime;
	}

	public static Int64 GetServerPacketTime(){
		return m_serverPacketTime;
	}

	public static Int64 NowTimeMs(){

		return (Int64)DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
	}	

	public static Int64 GetTimeMs(){
		return (Int64)Mathf.Round(Time.time * 1000);
	}

	public Int64 ServerTimeToLocal(Int64 timestamp){
		return timestamp + m_serverToLocalTimeDelta;
	}


	public static Int64 LocalTimeToServer(Int64 timestamp){
		return timestamp + m_serverToLocalTimeDelta;	
	}

	public static Int64 GetNowServerTime(){
		return LocalTimeToServer(GetTimeMs());
	}

	public static void SyncTime(Int64 nowTime){
		//Debug.LogWarning ("htime sync:" + nowTime);
		m_serverToLocalTimeDelta = nowTime - GetTimeMs ();
		
	}

}
