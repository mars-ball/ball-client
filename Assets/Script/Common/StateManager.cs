﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using StatePara = System.Collections.Generic.Dictionary<string, System.Object>;

public class StateManager<State, Msg, StateCo>
	where State: struct,IConvertible
	where Msg : struct, IConvertible
	where StateCo : class
{

	protected State mDefaultState;
	protected State prevState;
	protected State currState;
	protected Dictionary<State, Dictionary<Msg, State>> stateMsgs = null;
	protected StateCollection<State> mStateCollection = null;

	public void Reset(){
		currState = mDefaultState;
	}

	public void Reset(State state){
		currState = state;
	}

	public StateManager(){
	
		mDefaultState = (State)(object)1;
		mStateCollection = typeof(StateCo).GetMethod ("GetInstance").Invoke (null, null) as StateCollection<State>;
	}

	private State GetStateEnum(int t){
		
		Type enumType = typeof(State);
		Enum value = (Enum)Enum.ToObject (enumType, t);
		return (State)(object)value;
	}

	private Msg GetMsgEnum(int m){
		Type enumType = typeof(Msg);
		Enum value = (Enum)Enum.ToObject (enumType, m);
		return (Msg)(object)value;
	}


	public virtual void getMsg(Msg msg){
		Debug.Log ("curr state: " + currState + " msg: " + msg);
		if ((int)(object)msg == 1) {
			
			bool ret = NextState (mDefaultState, null);
			if (!ret) {
				Debug.Log ("default state failed");
			}
			return;
		} else {
			if(!stateMsgs.ContainsKey(currState)){
				Debug.Log("no such state msg " + currState);
				return;
			}
			Dictionary<Msg, State> stateMsg = stateMsgs [currState];
			if (stateMsg.ContainsKey (msg)) {

				bool ret = NextState (stateMsg [msg], null);
				if (!ret) {
					Debug.Log ("next state failed " + msg);
				} 
			}else{
				Debug.Log ("not contain msg: " + msg + "for state : " + currState);
				
			}
		}			
	}
	
	public virtual void getMsg(Msg msg, Dictionary<string, object>para){

		Debug.Log ("curr state: " + currState + " msg: " + msg);
		if ((int)(object)msg == 1) {
			
			bool ret = NextState (mDefaultState, para);
			if (!ret) {
				Debug.Log ("default state failed");
			}
			return;
		} else {
			if(!stateMsgs.ContainsKey(currState)){
				Debug.Log("no such state msg " + currState);
				return;
			}
			Dictionary<Msg, State> stateMsg = stateMsgs [currState];
			if (stateMsg.ContainsKey (msg)) {

				Debug.Log ("not contains the key");
				bool ret = NextState (stateMsg [msg], para);
				if (!ret) {
					Debug.Log ("next state failed " + msg);
				}
				return;		
			} else {
				Debug.Log ("error get msg, msg " + msg + " currState:" + currState);
				return;
			}
		}
		return;
	}
	
	public void UpdateState(StatePara para){
			
		if (!mStateCollection.HasExecutor (currState)) {
			return;
		}
		
		mStateCollection.ExecStateUpdate (currState, para);
		
		return;
	
	}


	public State GetCurrState(){
		return currState;
	}
	
	
	private bool NextState(State nextState, StatePara para){
		
		bool ret = false;
		
		ret = ExitState (currState, para);
		if (!ret) {
			Debug.Log ("exit last state failed");
			return false;
		}
		
		prevState = currState;
		currState = nextState;
		
		ret = EnterState (nextState, para);
		if (!ret) {
			Debug.Log ("enter new state");
			return false;
		}
		
		return true;
	}
	
	
	protected bool EnterState(State state, StatePara para){
		
		//StateCollection<State> stateCollection = typeof(StateCo).GetMethod ("GetInstance").Invoke (null, null) as StateCollection<State>;
		//mStateCollection = typeof(StateCo).GetMethod ("GetInstance").Invoke (null, null) as StateCollection<State>;
		if (!mStateCollection.HasExecutor (state)) {
			return true;
		}
		
		mStateCollection.ExecStateEnter (state, para);
		
		return true;
	}
	
	protected bool ExitState(State state, StatePara para){
		
		//StateCollection<State> stateCollection = typeof(StateCo).GetMethod ("GetInstance").Invoke (null, null) as StateCollection<State>;
		
		if (!mStateCollection.HasExecutor (state)) {
			return true;
		}
		
		mStateCollection.ExecStateExit (state, para);
		
		return true;
	}
	
	
	protected class MsgStatePair{
		
		Msg playerMsg;
		State playerState;
		
		public MsgStatePair(Msg msg, State state){
			
			playerMsg = msg;
			playerState = state;
			
		}
		
	}
	
}
