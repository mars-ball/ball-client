﻿using UnityEngine;
using System.Collections;

public enum InputDirection
{
	Invalid = 0,
	Left = 1,
	Right = 2,
	Top = 3,
	Bottom =4
}
