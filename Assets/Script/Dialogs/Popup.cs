﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Popup : MonoBehaviour {

	public Text lbl_Title;
	public Text lbl_Content;
	public Text lbl_LeftButton;
	public Text lbl_RightButton;

	static GameObject prefab = null;
	static Popup sPopup = null;

	public delegate void OnLeftCallback();
	public delegate void OnRightCallback();

	static OnLeftCallback m_leftCallback;
	static OnRightCallback m_rightCallback;

	static Popup(){
		Debug.LogError ("aaaaaaa");
		if (prefab == null) {
			string assetPath = "Prefab/popup_shop";    
			prefab = (GameObject)Resources.Load (assetPath);
		}
	}
		
	public static void show_popup_confirm (GameObject parentCanvase, OnLeftCallback leftCallback, OnRightCallback rightCallback) {

		GameObject popObj = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		popObj.transform.parent = parentCanvase.transform;

		Popup popup = popObj.GetComponent<Popup> ();
		popObj.transform.localPosition = Vector3.zero;

		m_leftCallback = leftCallback;
		m_rightCallback = rightCallback;

		if (sPopup == null) {
			sPopup = popup;
		}
	}

	public static void changeTitle(string title){
		sPopup.lbl_Title.text = title;
	}

	public static void changeContent(string content){
		sPopup.lbl_Content.text = content;
	}

	public static void changeLeftButtonLabel(string lButton){
		sPopup.lbl_LeftButton.text = lButton;
	}

	public static void changeRightButtonLabel(string rButton){
		sPopup.lbl_RightButton.text = rButton;
	}

	public void closeFromInternal(){
		Destroy (gameObject);
		m_leftCallback = null;
		m_rightCallback = null;

	}

	public static void closePopup() {
		if (sPopup) {
			Destroy (sPopup.gameObject);
			m_leftCallback = null;
			m_rightCallback = null;
		}
	}

	public void OnClickLeft(){
		if (m_leftCallback != null) {
			m_leftCallback ();
		}
	
	}

	public void OnClickRight(){
		if(m_rightCallback != null){
			m_rightCallback ();
		}
	}
}
