﻿Shader "Tut/Lighting/Forward/Lab_0/FwdAdd" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
	SubShader {
        pass{
        Tags { "RenderType"="ForwardBase" }
        
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma multi_compile_fwdbase
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"
        #include "Lighting.cginc"


        struct vertOut{
            float4 pos:SV_POSITION;
            float4 color:COLOR;
        };
        
        vertOut vert(appdata_base v){
            vertOut o;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            o.color=float4(0,0.5,1,1);
            
            
            
            
            
            return o;        
        }
        
        float4 frag(vertOut i):COLOR{
            return i.color;
        }
	ENDCG
	}//end pass

	}
}