﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using StateMsg = System.Collections.Generic.Dictionary<GameMsg,GameState>;
using StatePara = System.Collections.Generic.Dictionary<string, System.Object>;


public class GameStateManager :StateManager<GameState, GameMsg, GameStateCollection> {
	private static GameStateManager instance;
	private GameStateCollection mStateCollection;
	
	private GameStateManager(GameState initState){
		currState = initState;	
		mStateCollection = GameStateCollection.GetInstance ();
		stateMsgs = new Dictionary<GameState, Dictionary<GameMsg, GameState>>
		{
			{GameState.Default, new StateMsg{{GameMsg.RoundOver, GameState.RoundOver}, {GameMsg.RoundStart, GameState.RoundStart}}},
			{GameState.RoundOver, new StateMsg{{GameMsg.RoundStart, GameState.RoundStart}}},
			{GameState.RoundStart, new StateMsg{{GameMsg.RoundGo, GameState.RoundPlaying}, {GameMsg.RoundOver, GameState.RoundOver}}}
		};
		mStateCollection.AddStateHandler (GameState.RoundOver, new GameStateStateRoundOver());
		mStateCollection.AddStateHandler (GameState.RoundStart, new GameStateStateRoundStart ());
		
	}
	
	static GameStateManager(){
		
		instance = new GameStateManager (GameState.Default);
		
	}
	
	public static GameStateManager getInstance(){
		return instance;
	}

}
