﻿using UnityEngine;
using System.Collections;

public class GameRuntimeData{

	private static GameRuntimeData m_instance; 

	private GameRuntimeData(){
	}

	static GameRuntimeData(){

		m_instance = new GameRuntimeData ();

	}

	public static GameRuntimeData GetInstance(){
	
		return m_instance;
	}

	public int m_roundId;
	public int m_stageId;

}
