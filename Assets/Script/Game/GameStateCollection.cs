﻿using UnityEngine;
using System.Collections;

public class GameStateCollection:StateCollection<GameState> {

	private static GameStateCollection mInstance;
	
	private GameStateCollection(){
	}
	
	static GameStateCollection(){
		mInstance = new GameStateCollection ();
	}
	
	public static GameStateCollection GetInstance(){
		return mInstance;
	}
}
