﻿using UnityEngine;
using System.Collections;

public class GameStateStateRoundStart : StateGod{
	public override void EnterState (System.Collections.Generic.Dictionary<string, object> para)
	{
		base.EnterState (para);

		int roundId = GameRuntimeData.GetInstance ().m_roundId;
		int stageId = GameRuntimeData.GetInstance ().m_stageId;

		RoundData roundInfo = RoundDataManager.GetInstance ().Get (roundId);

		if (roundInfo == null) {
			Debug.Log ("failed get roundinfo " + roundId);					
		}

		Stage stageInfo = roundInfo.GetStageById (stageId);

		if (stageInfo == null) {
			Debug.Log ("stageInfo null");
			return;
		}
	}

	public override void ExitState (System.Collections.Generic.Dictionary<string, object> para)
	{
		base.ExitState (para);
	}

	public override void StateUpdate (System.Collections.Generic.Dictionary<string, object> para)
	{
		base.StateUpdate (para);
	}

}
