﻿using UnityEngine;
using System.Collections;

public enum GameState{
	Invalid = 0,
	Default = 1,
	RoundOver = 2,
	RoundStart = 3,
	RoundPlaying = 4,
}

public enum GameMsg{
	
	Invalid = 0,
	Default = 1,
	RoundOver = 2,
	RoundStart = 3,
	RoundGo = 4
}
