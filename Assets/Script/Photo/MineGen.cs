﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using tnt_deploy;

public class MineGen: Singleton<MineGen>  {

	MineDataManager m_mineMgr;

	Dictionary<int, Color> m_mineColor;
	Dictionary<int, Texture2D> m_mineTex;

	public const int MINE_RADIUS = 50;
	public static readonly Color COLOR_TRANS = new Color(0, 0, 0, 0);
	public static readonly Color COLOR_DEFAULT = new Color(1.0f, 0, 0);

	public void LoadAllMine(){

		m_mineColor = new Dictionary<int, Color>();
		m_mineTex = new Dictionary<int, Texture2D>();

		m_mineMgr = MineDataManager.GetInstance();	
		m_mineMgr.Load();

		foreach(var item in m_mineMgr.m_allData){
			MINE mine = item.Value;
			string color_str = System.Text.Encoding.ASCII.GetString(mine.color);
			Color color = colorFromHexString(color_str);
			Texture2D tex = createMine(color);
		
			int mineId = mine.id;
			m_mineTex[mineId] = tex;
			m_mineColor[mineId] = color;
		}
		
	}

	public Texture2D createMine(Color color){
		Texture2D texture = new Texture2D(MINE_RADIUS*2, MINE_RADIUS*2);

		Vector2 center = new Vector2(MINE_RADIUS, MINE_RADIUS);


		for(int row = 0; row < texture.height; row++){
			for(int col = 0; col < texture.width; col++){

				Color applyColor = color;

				int x = col - (int)(center.x);
				int y = row - (int)(center.y);

				float r = Mathf.Sqrt(x*x + y*y);

				if(r > MINE_RADIUS){
					applyColor = COLOR_TRANS;
				}
				texture.SetPixel(row, col, color);
			}	
		}
		texture.Apply();
		return texture;	
	}

	public Color colorFromHexString(string cs){

		int idx = cs.IndexOf("#");
		cs = cs.Substring(idx);

		string rs = cs.Substring(1, 2);
		string gs = cs.Substring(3, 2);
		string bs = cs.Substring(5, 2);

		int ri = Convert.ToInt32(rs, 16);
		int gi = Convert.ToInt32(gs, 16);
		int bi = Convert.ToInt32(bs, 16);

		return new Color(ri/255.0f, gi/255.0f, bi/255.0f);

	
	}

	public Texture2D getTextureById(int id){
	
		if(!m_mineTex.ContainsKey(id)){
			Debug.LogError("not contain this id:"+ id);
			return null;
		}
		return m_mineTex[id];
	
	}

	public Color getColorById(int id){
	
		if(!m_mineColor.ContainsKey(id)){
			Debug.LogError("not contain this id:" + id);
			return COLOR_DEFAULT;
		}
		return m_mineColor[id];	
	}

}
