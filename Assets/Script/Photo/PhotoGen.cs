﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using tnt_deploy;

public class PhotoGen: Singleton<PhotoGen>  {
	
	PhotoDataManager m_photoMgr;

	public List<int> m_allId;
	Dictionary<int, Pair<Color, Color>> m_colors;
	Dictionary<int, string> m_images;

	const float BORDER_PERCENT = 0.8f;
	int TEX_SIZE;
	public static readonly Color COLOR_TRANS = new Color(0, 0, 0, 0);


	public void LoadAllPhoto(){
	
		m_colors = new Dictionary<int, Pair<Color, Color>>();
		m_images = new Dictionary<int, string>();
		m_allId = new List<int> ();

		m_photoMgr = PhotoDataManager.GetInstance();	
		m_photoMgr.Load();

		foreach(var item in m_photoMgr.m_allData){
			PHOTO photo = item.Value;

			if(PhotoDataManager.isColorPhoto(photo)){

				m_colors[photo.id] = new Pair<Color, Color>(new Color(photo.r0/255.0f, photo.g0/255.0f, photo.b0/255.0f), new Color(photo.r1/255.0f, photo.g1/255.0f, photo.b1/255.0f));

			}else if(PhotoDataManager.isPhotoPhoto(photo)){

				m_images[photo.id] = System.Text.Encoding.ASCII.GetString(photo.photo_name);

			}

			m_allId.Add (photo.id);
		}
			
	}


	public Texture2D getPhotoTexture(int id, int width = 256, int height = 256){
		if(m_colors.ContainsKey(id)){
			return createColorTexture(id,width, height);		
		}else if(m_images.ContainsKey(id)){
			return loadImageTexture(id);
		}
		return null;
	}

	public Texture2D loadImageTexture(int id){
	
		if(!m_images.ContainsKey(id)){
			Debug.LogError("image not contain id:"+ id);
			return null;
		}

		//return Resources.Load("Photos/" + m_images[id], typeof(Texture2D)) as Texture2D;
		return BundleLoader.LoadFromLocalBundle<Texture2D>(BundleLoader.BUNDLE_TYPE.PHOTO, m_images[id]);
	}


	public Texture2D createColorTexture(int id, int width,int height){
		Texture2D texture = new Texture2D(width, height);

		if(!m_colors.ContainsKey(id)){
			Debug.LogError("color not contain this id:" + id);
		}

		Vector2 center = new Vector2(width/2, height/2);

		Pair<Color, Color> colorPair = m_colors[id];

		TEX_SIZE = (width > height ? height: width)/2;

		float borderPix = TEX_SIZE * BORDER_PERCENT;

		for(int row = 0; row < texture.height; row++){
			for(int col = 0; col < texture.width; col++){
				texture.SetPixel(row, col, new Color(0, 0, 1));
			}
		}

		for(int row = 0; row < texture.height; row++){
			for(int col = 0; col < texture.width; col++){
				Color color;

				int x = col - (int)(center.x);
				int y = row - (int)(center.y);

				float r = Mathf.Sqrt(x*x + y*y);

				if(r > TEX_SIZE){
					color = COLOR_TRANS;
				}else if(r > borderPix && r < TEX_SIZE){
					color = colorPair.Second;				
				}else{
					color = colorPair.First;
				}		
				texture.SetPixel(row, col, color);
			}	
		}
		texture.Apply();
		return texture;	
	}
}
