﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	private static SoundManager instance;

	private SoundManager() {}

	public static SoundManager GetInstance()
	{
		if (instance == null)
		{
			GameObject soundManager = new GameObject("SoundManager");
	
			instance = soundManager.AddComponent<SoundManager>();

			//  给 MusicPlayer 游戏对象添加音乐播放器脚本
			soundManager.AddComponent<BGM_Manager>();

			//  跨场景不删除这个单例
			DontDestroyOnLoad(instance);
		}

		return instance;
	}

	public void PlayMenuMusic () {

		getBGM_Manager().PlayMusic (SoundResource.BGM_Menu_filename);
	}

	public void PlayBattleMusic () {
		
		getBGM_Manager().PlayMusic (SoundResource.BGM_Battle_filename);
	}

	BGM_Manager getBGM_Manager() {
		
		BGM_Manager sm = GetComponentInChildren<BGM_Manager> ();
		if (sm == null) {
			return null;
		}
		return sm;
	}
}
