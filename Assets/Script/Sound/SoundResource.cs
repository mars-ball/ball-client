﻿using UnityEngine;
using System.Collections;

public class SoundResource {

	// path相对于Resources文件夹，扩展名必须被忽略。
	public static string BGM_Menu_filename = "Sound/menu_bgm";

	public static string BGM_Battle_filename = "Sound/battle_bgm";

}
