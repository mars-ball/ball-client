﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class BGM_Manager : MonoBehaviour {

	private AudioClip music;
	private AudioSource musicPlayer;

	void Awake()
	{
		//  获取 AudioSource
		musicPlayer = this.GetComponent<AudioSource>();
		//  打开单曲循环
		musicPlayer.loop = true;
		//  关闭开始时就播放的属性
		musicPlayer.playOnAwake = false;
	}

	public void PlayMusic(string musicFile)
	{
		if (musicPlayer.clip != null) {
			string namePlaying = musicPlayer.clip.name;
			if (musicFile.Contains (namePlaying)) {
				return;
			}
		}

		if (musicPlayer.isPlaying)
			musicPlayer.Stop ();
		
		//  读取音乐
		music = (AudioClip) Resources.Load(musicFile);

		//  将读取到的音乐赋值给 audioClip
		musicPlayer.clip = music;

		//  播放
		musicPlayer.Play();
	}
}
