﻿using UnityEngine;
using UnityEngine.SceneManagement;
//using System.Collections;
using UnityEngine.Advertisements;
using pbmsg;
using pbmsg.login;
using ProtoBuf;


public class UnityADs : MonoBehaviour {

	public void ShowAd()
	{
		if (Advertisement.IsReady())
		{
			Advertisement.Show();
		}
	}

	public void ShowRewardedAd()
	{
		const string RewardedZoneId = "rewardedVideo";

		if (Advertisement.IsReady (RewardedZoneId)) { //"rewardedVideoZone"))
			var options = new ShowOptions { resultCallback = HandleShowResult };
//			Advertisement.Show ("rewardedVideoZone", options);
			Advertisement.Show (RewardedZoneId, options);
		} else {
			Debug.Log (string.Format ("Ads not ready for zone '{0}'", RewardedZoneId));
		}
	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			//
			// YOUR CODE TO REWARD THE GAMER
			// Give coins etc.
			new CSWatchAdEndMessage (pbmsg.login.AdType.AD_TYPE_LONG).Send (
				delegate(IExtensible message) {
					HMessage msg = (HMessage)message;
					if (msg.msgType != MSGID.CSWatchAdEnd_Response) {
						Debug.LogError ("msg id error " + msg.msgType);
						return false;
					}

					CSWatchAdEndResponse rsp = msg.response.adEnd;
					if (rsp == null) {
						Debug.LogError ("watch end rsp null ");
						return false;			
					}
					if (rsp.ret == Ret.Success) {
						Debug.LogError ("change name success");
					} else {
						Debug.LogError ("change name failed");				
					}

					UserManager.GetInstance().GetMainUser().mBasicInfo.gold += rsp.addGold;
					SceneManager.LoadScene("Login");

					return true;
				}
			);

			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}
	}
}