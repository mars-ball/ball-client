﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResBank : MonoBehaviour {

	private Queue<GameObject> m_unitBank;
	private Dictionary<int, Sprite> m_spriteBank;
	private Queue<GameObject> m_mineBank;

	private GameObject m_unitPrefab;
	private GameObject m_minePrefab;

	private Sprite m_mineSprite;

	public const int MINE_INIT_SIZE = 20;
	public const int MINE_INCREASE_SIZE = 10;
	public const int MINE_LEAST_SIZE = 5;
	public const int UNIT_INIT_SIZE = 15;
	public const int UNIT_INCREASE_SIZE = 5;
	public const int UNIT_LEAST_SIZE = 7;
	public static readonly string UNIT_PREFAB_PATH = "Battle/Prefab/player_unit";
	public static readonly string MINE_PREFAB_PATH = "Battle/Prefab/MineNormal";
	public static readonly string MINE_TEXTURE_PATH = "Battle/mine";

	public static readonly Vector3 WAIT_POS = new Vector3(0, -1000, 0);

	void Awake(){

		m_unitBank = new Queue<GameObject> ();
		m_mineBank = new Queue<GameObject> ();
		m_spriteBank = new Dictionary<int, Sprite> ();

		m_unitPrefab = Resources.Load (UNIT_PREFAB_PATH) as GameObject;


		for (int ii = 0; ii < UNIT_INIT_SIZE; ii++) {
			GameObject newUnit = GameObject.Instantiate (m_unitPrefab, WAIT_POS, Quaternion.identity)  as GameObject;
			newUnit.SetActive (false);
			m_unitBank.Enqueue (newUnit);
		}

		PhotoGen photoGen = PhotoGen.GetInstance ();

		foreach (var photoId in photoGen.m_allId) {

			Texture2D tex = photoGen.getPhotoTexture(photoId);
			if(tex == null){
				Debug.LogError("tex create failed");
				return;
			}

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

			m_spriteBank [photoId] = sprite;

		}

		m_minePrefab = Resources.Load(MINE_PREFAB_PATH) as GameObject;
		Sprite mineTex = Resources.Load(MINE_TEXTURE_PATH, typeof(Sprite)) as Sprite;
		m_mineSprite = mineTex;//Sprite.Create(mineTex, new Rect(0, 0, mineTex.width, mineTex.height), new Vector2(0.5f, 0.5f));

		for (int ii = 0; ii < MINE_INIT_SIZE; ii++) {
			GameObject newUnit = GameObject.Instantiate (m_minePrefab, WAIT_POS, Quaternion.identity)  as GameObject;
			newUnit.SetActive (false);
			SpriteRenderer render = newUnit.GetComponent<SpriteRenderer>();
			render.sprite = m_mineSprite;


			m_mineBank.Enqueue (newUnit);
		
		}
	}

	public void IncreaseUnitBankCount(){
		
		for (int ii = 0; ii < UNIT_INCREASE_SIZE; ii++) {
			GameObject newUnit = GameObject.Instantiate (m_unitPrefab, WAIT_POS, Quaternion.identity)  as GameObject;
			newUnit.SetActive (false);
			m_unitBank.Enqueue (newUnit);
		}
	}

	public void IncreaseMineBankCount(){

		for (int ii = 0; ii < MINE_INIT_SIZE; ii++) {
			GameObject newUnit = GameObject.Instantiate (m_minePrefab, WAIT_POS, Quaternion.identity)  as GameObject;
			newUnit.SetActive (false);
			SpriteRenderer render = newUnit.GetComponent<SpriteRenderer>();
			render.sprite = m_mineSprite;
			m_mineBank.Enqueue (newUnit);

		}
	
	}


	public GameObject GetUnit(int photoId){
		if (m_unitBank.Count < 1) {
			Debug.LogError ("unit bank is empty");
			IncreaseUnitBankCount ();
		}

		GameObject newObj = m_unitBank.Dequeue ();
		newObj.SetActive (true);

		if (m_unitBank.Count <= UNIT_LEAST_SIZE) {
			IncreaseUnitBankCount ();
		
		}

		SpriteRenderer render = newObj.GetComponent<SpriteRenderer>();
		if (!m_spriteBank.ContainsKey (photoId)) {
			Debug.LogError ("sprite bank is not contain photoId:" + photoId);
			return null;
		}

		Sprite newSprite = m_spriteBank[photoId];

		render.sprite = newSprite;
		return newObj;

	}

	public GameObject GetMine(int colorId){
		if (m_mineBank.Count < 1) {
			Debug.LogError ("mine bank is empty");
			IncreaseMineBankCount ();		
		}

		GameObject newObj = m_mineBank.Dequeue ();
		newObj.SetActive (true);

		if (m_mineBank.Count <= MINE_LEAST_SIZE) {
			IncreaseMineBankCount ();
		
		}

		Color mineColor = MineGen.GetInstance().getColorById(colorId);
		if(mineColor == null){
			Debug.LogError("no such color:" + colorId);
			return null;
		}

		SpriteRenderer render = newObj.GetComponent<SpriteRenderer>();
		render.color = new Color(mineColor[0], mineColor[1], mineColor[2]);

		return newObj;
	
	}

	public void ReturnUnit(GameObject obj){

		obj.transform.localPosition = WAIT_POS;
		obj.SetActive (false);
		m_unitBank.Enqueue (obj);
	
	}

	public void ReturnMine(GameObject obj){

		obj.transform.localPosition = WAIT_POS;
		obj.SetActive (false);
		m_mineBank.Enqueue (obj);
	
	}



}
















































