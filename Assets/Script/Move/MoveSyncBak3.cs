﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pbmsg.room;
using System;

public class MoveSync3 : MonoBehaviour {

	private static MoveSync3 m_instance;

	private const long SYNC_BEFORE_DELTA = 15;

	private const long SYNC_DELTA_MS = 65;

	private const long TEST_JOY_UP_MS = 1200;
	private bool m_testOnce = false;
	private List<Vector3> m_testDirs;
	private static int m_testIndex = 0;

	private const long PIXEL_PER_UNIT = 256;
	private const long INIT_SIZE = 2;

	private float m_CurrTimePercent = 1.0f;

	private static int m_syncMoveCounter = 0;
	private static int m_syncMoveTot = 5;

	private Vector2 m_prevSpeed = new Vector2(0, 0);

	private Vector3 INIT_POS;

	private bool m_isStart;

	//private bool m_isWaitForSync;
	private int m_waitNum;
	private bool m_isSyncing;

	private Int64 m_prevMoveStartTime;
	private Int64 m_prevMoveServerTime;
	private Int64 m_prevMoveEndTime;
	private Int64 m_fixedTimeCounter;

	private HashSet<Int32> m_prevIdSet;
	private HashSet<Int32> m_currIdSet;

	private Int64 m_serverToLocalTimeDelta;

	class Player{
		public int m_uid;
		public int m_num;
		public List<Unit> m_units;

		public class Unit{
			public GameObject m_obj;
			public Transform m_trans;
			public PlayerMove m_move;
			public Rigidbody2D m_rigid;

			public Unit(int _id, GameObject obj){
				if(obj == null){
					Debug.LogError("create player unit null:");
					return;
				}
				m_obj = obj;
				m_trans = obj.GetComponent<Transform>();
				m_move = obj.GetComponent<PlayerMove>();
				m_rigid = obj.GetComponent<Rigidbody2D>();

				m_move.uid = _id;

			}

			public void SetR(float r){
				float rScale = r/INIT_SIZE;
				m_trans.localScale = new Vector3(rScale, rScale, 1.0f);
			}


			public bool IsOk(){
				return (m_obj != null) && (m_trans != null) && (m_move != null) && (m_rigid != null);
			}

		}

		public Player(int _uid, List<GameObject> objs){
			m_uid = _uid;
			if(objs == null){
				Debug.LogError("create player obj null:" + _uid);
			}
			m_units = new List<Unit>();
			foreach(GameObject obj in objs){
				Unit unit = new Unit(_uid, obj);
				m_units.Add(unit);
			}
		}

		public Player(int _uid){
			m_uid = _uid;
			m_units = new List<Unit>();
		}

		public void SetUnitPos(int idx, Vector3 pos){
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}

			//m_units[idx].m_trans.localPosition = pos;
		}

		public void SetUnitSpeed(int idx, Vector2 speed){
		
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}

			m_units[idx].m_rigid.velocity = speed;
		}

		public void SetUnitRadius(int idx, float rScale){
			
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}
			
			//m_units[idx].m_trans.localScale = new Vector3(rScale, rScale, 1.0f);
		}


		public void AddUnit(Unit unit){
			m_units.Add (unit);
		}

		public bool IsOk(){
			foreach(Unit unit in m_units){
				if(!unit.IsOk()){
					return false;
				}			
			}
			return true;
		}

		public void Stop(){
			foreach(Unit unit in m_units){			
				if(unit == null || unit.m_rigid == null){
					Debug.LogError("an unit is invalid, uid:" + m_uid);
					continue;
				}			
				unit.m_rigid.velocity = new Vector2(0, 0);
			}
		}

		public void DestroyAll(){
			foreach(Unit unit in m_units){
				GameObject.Destroy(unit.m_obj);			        
			}
		}	
	}

	private Dictionary<int, Player> m_players;

	private Queue<int> m_useless;
	private const int KEEP_COUNT = 30;


	public class MovePacket{
		public Int64 m_localTime;
		public Int64 m_serverTime;
		public CSDirectMoveSycnDown m_packet;
		public MovePacket(Int64 time_, CSDirectMoveSycnDown packet_){
			m_localTime = time_;
			m_packet = packet_;
			m_serverTime = packet_.timestamp;
		}
	}

	private Queue<MovePacket> m_frames;

	public static MoveSync3 GetInstance(){

		if (m_instance == null) {
			GameObject obj = new GameObject ();
			obj.name = "Move Sync";
			m_instance = obj.AddComponent <MoveSync3> ();
			m_instance.m_frames = new Queue<MovePacket> ();
			m_instance.m_players = new Dictionary<int, Player>();
			m_instance.m_useless = new Queue<int>();
		}
		return m_instance;
	}


	public Int64 ServerTimeToLocal(Int64 timestamp){
	
		return timestamp + m_serverToLocalTimeDelta;
	}



	public void ReceiveMovePacket(CSDirectMoveSycnDown packet, Int64 time){
		//Debug.LogError("recv 0");
		if(!m_isStart){
			Debug.LogError("not start yet");
			return;
		}
		//Debug.LogError("recv 1");

		if(!m_isSyncing && m_frames.Count == 0){
			m_serverToLocalTimeDelta = HTime.NowTimeMs() - packet.timestamp;
			m_CurrTimePercent = 1.0f;
		//}else if(!m_isSyncing && m_frames.Count == 1){
			m_isSyncing = true;
			//Debug.LogError("recv 2");
			ApplyMove(new MovePacket (HTime.NowTimeMs(), packet), 1.0f);
			m_isSyncing = true;
			m_fixedTimeCounter = SYNC_DELTA_MS - SYNC_BEFORE_DELTA;
			m_prevMoveStartTime = (long)(Time.fixedTime * 1000);
			m_prevMoveEndTime = m_prevMoveStartTime + m_fixedTimeCounter;
			m_prevMoveServerTime = packet.timestamp;
			return;
		}else if(m_isSyncing && m_frames.Count == 0){		
			Int64 prevDelta = packet.timestamp - m_prevMoveServerTime;
			if(prevDelta <= 0){
				Debug.LogError("prev delta less than 0");
			}else{
				m_prevMoveEndTime = m_prevMoveStartTime + prevDelta;
			}
		}else if(m_isSyncing && m_frames.Count > 1){
			m_CurrTimePercent = 0.5f;
			
		}else if(m_isSyncing && m_frames.Count <= 1){
			m_CurrTimePercent = 1.0f;
		}


		//Debug.LogError("recv 4");
		m_frames.Enqueue (new MovePacket (HTime.NowTimeMs(), packet));
		//Debug.LogError("packet pos x:" + packet.move[0].units[0].pos.x + " y:" + packet.move[0].units[0].pos.y);
	}

	Player CreateNewPlayer(int uid, CSBasicMovement move){

		if(m_players.ContainsKey(uid)){
			Debug.LogError("already in use");
			return null;
		}

		Player player = new Player(uid);

		foreach(CSBasicMovementUnit moveUnit in move.units){
			GameObject newObj = Instantiate(Resources.Load("player_unit")) as GameObject;
			Player.Unit unit = new Player.Unit(uid, newObj);

			float scaleR = 2*moveUnit.r/INIT_SIZE;
			unit.SetR(scaleR);

			player.AddUnit(unit);

		}	
		return player;
	}


	public void stopAllPlayer(){
	
		foreach(var pl in m_players){
		
			Player player = m_players[pl.Key];
			if(player == null){
				continue;
			}

			player.Stop();		
		
		}
	
	}



	public int setPlayerMove(int uid, CSBasicMovement move, float mod){
		Debug.LogError("move pos :(" + move.units[0].pos.x + "," + move.units[0].pos.y + ") speed :(" + move.units[0].v.x + ", " + move.units[0].v.y + ") frame count:" + m_frames.Count);
		if(!m_players.ContainsKey(uid)){
			Player newPlayer = CreateNewPlayer(uid, move);
			if(newPlayer == null){
				Debug.LogError("new user null");
				return -1;			
			}

			m_players[uid] = newPlayer;
		}

		Player player = m_players[uid];

		if(!player.IsOk()){
			Debug.LogError("player not full:" + uid);

			return -1;
		}

		if(player.m_units.Count != move.units.Count){
			Debug.LogError("count net matched");
			return -1;											//fixme
		}

		for(int ii = 0; ii< move.units.Count; ii++){

			pbmsg.room.Vector pos = move.units[ii].pos;
			Vector3 local = player.m_units [0].m_trans.position + new Vector3(50, 50, 0);
			Vector3 sub = local - new Vector3(pos.x, pos.y, 0);
			Debug.LogError ("local pos :(" + local.x + "," + local.y + ") localSubServer :(" + sub.x + ", " + sub.y + ")");
			if(m_syncMoveCounter == 0){
				player.SetUnitPos(ii, new Vector3(pos.x, pos.y, 0));
			}

			pbmsg.room.Vector speed = move.units[ii].v;

			Vector2 currSpeed = new Vector2(speed.x*mod, speed.y*mod);
			/*
			if((currSpeed - m_prevSpeed).magnitude > 50){*/
				player.SetUnitSpeed(ii, new Vector2(speed.x*mod, speed.y*mod));
				/*m_prevSpeed = currSpeed;
			}*/

			float radius = move.units[ii].r;

			float tmp = 2*move.units[ii].r/INIT_SIZE;

			player.SetUnitRadius(ii, tmp);

		}

		return 0;
	}



	public void ApplyMove(MovePacket packet, float mod){
		CSDirectMoveSycnDown movePacket = packet.m_packet;
		List<CSBasicMovement> moves = movePacket.move;
		m_currIdSet.Clear();

		if(moves == null || moves.Count == 0){
			Debug.Log("move packet error");
		
		}

		foreach(CSBasicMovement move in moves){
			int uid = move.uid;
			m_currIdSet.Add(move.uid);
			int ret = setPlayerMove(uid, move, mod);
			if(ret != 0){
				Debug.LogError("set player move failed");
				return;
			}		
		}	
		HandleIdSet();
		m_syncMoveCounter = (m_syncMoveCounter + 1)%m_syncMoveTot;
		m_testOnce = false;
	}


	public void HandleIdSet(){
	
		m_prevIdSet.ExceptWith(m_currIdSet);

		foreach(int uid in m_prevIdSet){
			if(!m_useless.Contains(uid)){
				m_useless.Enqueue(uid);
			}	
		}

		while(m_useless.Count > KEEP_COUNT){
			int id = m_useless.Dequeue();
			if(!m_players.ContainsKey(id)){
				continue;
			}
			Player player = m_players[id];
			m_players.Remove(id);
			player.DestroyAll();		
		}	
	
	}

	public void StartMove(){
		m_isStart = true;
		m_isSyncing = false;
	}
		
	public void ReSync(){
		m_isSyncing = false;
		m_frames.Clear();
	}


	// Use this for initialization
	void Awake () {
		INIT_POS = new Vector3(0, -100, 0);
		m_isSyncing = false;
		m_isStart = false;

		m_prevIdSet = new HashSet<Int32>();
		m_currIdSet = new HashSet<Int32>();

		m_testDirs = new List<Vector3> ();
		m_testDirs.Add (new Vector3 (1, 0, 0));
		m_testDirs.Add (new Vector3 (0, 1, 0));
		m_testDirs.Add (new Vector3 (-1, 0, 0));
		m_testDirs.Add (new Vector3 (0, -1, 0));

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Debug.Log ("fixed, count:" + m_fixedTimeCounter + " time:" + Time.fixedTime + " time delta:" + (long)(Time.fixedDeltaTime*1000 + 0.001));
		if(!m_isStart){
			return;
		}

		if(!m_isSyncing){
			return;
		}

		Int64 currTime = (long)(Time.fixedTime * 1000);// HTime.NowTimeMs();

		if (currTime > m_prevMoveStartTime + TEST_JOY_UP_MS && !m_testOnce) {
			m_testOnce = true;
			//new DirectMoveSyncDUpMessage(m_testDirs[m_testIndex], 1.0f).Send();
			Debug.LogError("send " + m_testIndex + " move sync:" + m_testDirs[m_testIndex]);

			m_testIndex = (m_testIndex + 1) % m_testDirs.Count;
		}

		/*
		if(currTime < m_prevMoveEndTime){
			return;
		}*/

		m_fixedTimeCounter -= (long)(Time.fixedDeltaTime*1000 + 0.001)    ;
		if(m_fixedTimeCounter > 0){
			return;
		}

		if(m_frames.Count < 1){
			m_frames.Clear();
			m_isSyncing = false;	
			stopAllPlayer ();
			return;
		}

		MovePacket packet = m_frames.Dequeue();

		if (m_frames.Count <= 1) {
			m_CurrTimePercent = 1.0f;
		}

		float timeMod = 1.0f;

		Int64 preLocalDelta = currTime - m_prevMoveStartTime;
		Int64 preServerDelta = packet.m_serverTime - m_prevMoveServerTime;
		Int64 preGap = preLocalDelta - preServerDelta;

		if(preGap < 0){
			preGap = 0;
		}

		Int64 toEndDelta = SYNC_DELTA_MS;
		if(m_frames.Count >= 1){		
			toEndDelta = m_frames.Peek().m_serverTime - packet.m_serverTime - SYNC_BEFORE_DELTA;		
		}

		if (toEndDelta <= 0) {
			toEndDelta = 20;
		}

		Int64 toEndRestDelta = toEndDelta + m_fixedTimeCounter;
		if(toEndRestDelta <= 0){
			toEndRestDelta = 0;
		}

		if(toEndDelta > 0 && toEndRestDelta > 0){
		
			timeMod = 1.0f * toEndRestDelta/toEndDelta;

			if(timeMod <0.5f){
				timeMod = 0.5f;
			}
		}

		timeMod *= m_CurrTimePercent;

		toEndRestDelta = (Int64)(1.0f*toEndDelta*timeMod);

		Debug.Log("timemod:" + timeMod + "to End:" + toEndDelta + "toEndRest:" + toEndRestDelta + "curr delta:" + (m_prevMoveStartTime - currTime) + "exp delta:" + (currTime - m_prevMoveEndTime) + "fixed:"+Time.deltaTime);

		m_fixedTimeCounter = toEndRestDelta;
		m_prevMoveStartTime = currTime;
		m_prevMoveServerTime = packet.m_serverTime;

		m_prevMoveEndTime = m_prevMoveStartTime + toEndRestDelta;

		//Debug.LogError("apply move");
		if(timeMod < 0.7){
			timeMod = 0.7f;
		}

		ApplyMove(packet, 1.0f);
		//m_CurrTimePercent = 1.0f;
		/*
		Int64 thresh = m_prevMoveTime + SYNC_DEVI_MS;
		if(thresh > currTime){
			Debug.LogError("thresh exceed curr time, thresh:"+ thresh + " curr:" + currTime);
			thresh = m_prevMoveTime;	
		}

		if(thresh <= 0){
			Debug.LogError("thresh is less than 0 curr:" + m_currWaiDelta);
		}

		if(Mathf.Abs(currTime - m_prevMoveTime - SYNC_DELTA_MS) < 15 || currTime - m_prevMoveTime > SYNC_DELTA_MS){

			Debug.LogError("gap time:" + (currTime - m_prevMoveTime) + " frame count:" + m_frames.Count);

			if(m_frames.Count < 1){			
				ReSync();	
				return;
			}

			while(m_frames.Count >0){
				MovePacket packet = m_frames.ToArray()[0];
				if(packet.m_localTime <= thresh){
					packet = m_frames.Dequeue();
					ApplyMove(packet);
				}else{
					break;
				}
			}

			m_prevMoveTime = currTime;

		}*/


	}
}































