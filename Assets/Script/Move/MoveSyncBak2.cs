﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pbmsg.room;
using System;

public class MoveSyncBak2 : MonoBehaviour {

	private static MoveSync m_instance;

	private const long SYNC_DELTA_MS = 50;
	private const long SYNC_DEVI_MS = 20;

	private const long PIXEL_PER_UNIT = 256;
	private const long INIT_SIZE = 2;

	private Vector3 INIT_POS;

	private bool m_isStart;

	private bool m_isWaitForSync;
	private int m_waitNum;
	private bool m_isSyncing;

	private Int64 m_prevMoveTime;
	private Int64 m_currWaiDelta;

	private HashSet<Int32> m_prevIdSet;
	private HashSet<Int32> m_currIdSet;

	class Player{
		public int m_uid;
		public int m_num;
		public List<Unit> m_units;

		public class Unit{
			public GameObject m_obj;
			public Transform m_trans;
			public PlayerMove m_move;
			public Rigidbody2D m_rigid;

			public Unit(int _id, GameObject obj){
				if(obj == null){
					Debug.LogError("create player unit null:");
					return;
				}
				m_obj = obj;
				m_trans = obj.GetComponent<Transform>();
				m_move = obj.GetComponent<PlayerMove>();
				m_rigid = obj.GetComponent<Rigidbody2D>();

				m_move.uid = _id;

			}

			public void SetR(float r){
				float rScale = r/INIT_SIZE;
				m_trans.localScale = new Vector3(rScale, rScale, 1.0f);
			}


			public bool IsOk(){
				return (m_obj != null) && (m_trans != null) && (m_move != null) && (m_rigid != null);
			}

		}

		public Player(int _uid, List<GameObject> objs){
			m_uid = _uid;
			if(objs == null){
				Debug.LogError("create player obj null:" + _uid);
			}
			m_units = new List<Unit>();
			foreach(GameObject obj in objs){
				Unit unit = new Unit(_uid, obj);
				m_units.Add(unit);
			}
		}

		public Player(int _uid){
			m_uid = _uid;
			m_units = new List<Unit>();
		}

		public void SetUnitPos(int idx, Vector3 pos){
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}

			m_units[idx].m_trans.localPosition = pos;
		}

		public void SetUnitSpeed(int idx, Vector2 speed){
		
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}

			m_units[idx].m_rigid.velocity = speed;
		}

		public void SetUnitRadius(int idx, float rScale){
			
			if(idx < 0 || idx >= m_units.Count){
				Debug.LogError("idx error:" + idx);
				return;			
			}
			
			m_units[idx].m_trans.localScale = new Vector3(rScale, rScale, 1.0f);
		}


		public void AddUnit(Unit unit){
			m_units.Add (unit);
		}

		public bool IsOk(){
			foreach(Unit unit in m_units){
				if(!unit.IsOk()){
					return false;
				}			
			}
			return true;
		}

		public void DestroyAll(){
			foreach(Unit unit in m_units){
				GameObject.Destroy(unit.m_obj);			        
			}
		}	
	}

	private Dictionary<int, Player> m_players;

	private Queue<int> m_useless;
	private const int KEEP_COUNT = 30;


	public class MovePacket{
		public Int64 m_localTime;
		public Int64 m_serverTime;
		public CSDirectMoveSycnDown m_packet;
		public MovePacket(Int64 time_, CSDirectMoveSycnDown packet_){
			m_localTime = time_;
			m_packet = packet_;
		}
	}

	private Queue<MovePacket> m_frames;

	public static MoveSync GetInstance(){

		if (m_instance == null) {
			GameObject obj = new GameObject ();
			obj.name = "Move Sync";
			m_instance = obj.AddComponent <MoveSync> ();
//			m_instance.m_frames = new Queue<MovePacket> ();
//			m_instance.m_players = new Dictionary<int, Player>();
//			m_instance.m_useless = new Queue<int>();
		}
		return m_instance;
	}

	public void ReceiveMovePacket(CSDirectMoveSycnDown packet, Int64 time){
		if(!m_isStart){
			Debug.LogError("not start yet");
			return;
		}

		m_frames.Enqueue (new MovePacket (HTime.NowTimeMs(), packet));
	}

	Player CreateNewPlayer(int uid, CSBasicMovement move){

		if(m_players.ContainsKey(uid)){
			Debug.LogError("already in use");
			return null;
		}

		Player player = new Player(uid);

		foreach(CSBasicMovementUnit moveUnit in move.units){
			GameObject newObj = Instantiate(Resources.Load("player_unit")) as GameObject;
			Player.Unit unit = new Player.Unit(uid, newObj);

			float scaleR = 2*moveUnit.r/INIT_SIZE;
			unit.SetR(scaleR);

			player.AddUnit(unit);

		}	
		return player;
	}


	public int setPlayerMove(int uid, CSBasicMovement move){
	
		if(!m_players.ContainsKey(uid)){
			Player newPlayer = CreateNewPlayer(uid, move);
			if(newPlayer == null){
				Debug.LogError("new user null");
				return -1;			
			}

			m_players[uid] = newPlayer;
		}

		Player player = m_players[uid];

		if(!player.IsOk()){
			Debug.LogError("player not full:" + uid);

			return -1;
		}

		if(player.m_units.Count != move.units.Count){
			Debug.LogError("count net matched");
			return -1;											//fixme
		}

		for(int ii = 0; ii< move.units.Count; ii++){

			pbmsg.room.Vector pos = move.units[ii].pos;

			player.SetUnitPos(ii, new Vector3(pos.x, pos.y, 0));

			pbmsg.room.Vector speed = move.units[ii].v;

			player.SetUnitSpeed(ii, new Vector2(speed.x, speed.y));

			float radius = move.units[ii].r;

			float tmp = 2*move.units[ii].r/INIT_SIZE;

			player.SetUnitRadius(ii, tmp);

		}
		return 0;
	}



	public void ApplyMove(MovePacket packet){
		CSDirectMoveSycnDown movePacket = packet.m_packet;
		List<CSBasicMovement> moves = movePacket.move;
		m_currIdSet.Clear();

		foreach(CSBasicMovement move in moves){
			int uid = move.uid;
			m_currIdSet.Add(move.uid);
			int ret = setPlayerMove(uid, move);
			if(ret != 0){
				Debug.LogError("set player move failed");
				return;
			}		
		}	
		HandleIdSet();
	}


	public void HandleIdSet(){
	
		m_prevIdSet.ExceptWith(m_currIdSet);

		foreach(int uid in m_prevIdSet){
			if(!m_useless.Contains(uid)){
				m_useless.Enqueue(uid);
			}	
		}

		while(m_useless.Count > KEEP_COUNT){
			int id = m_useless.Dequeue();
			if(!m_players.ContainsKey(id)){
				continue;
			}
			Player player = m_players[id];
			m_players.Remove(id);
			player.DestroyAll();		
		}
	
	
	}




	public void StartMove(){
		m_isStart = true;
		m_isWaitForSync = true;
		m_isSyncing = false;
	}
		
	public void ReSync(){
		m_isWaitForSync = true;
		m_isSyncing = false;	
	}


	// Use this for initialization
	void Awake () {
		INIT_POS = new Vector3(0, -100, 0);
		m_isWaitForSync = false;
		m_isSyncing = false;
		m_isStart = false;
		m_currWaiDelta = 2*SYNC_DELTA_MS;

		m_prevIdSet = new HashSet<Int32>();
		m_currIdSet = new HashSet<Int32>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!m_isStart){
			return;
		}

		if(m_isWaitForSync && m_frames.Count > 0){
			m_isWaitForSync = false;
			m_isSyncing = true;		
			m_prevMoveTime = HTime.NowTimeMs();
			//m_currWaiDelta = SYNC_DELTA_MS;
		}

		if(!m_isSyncing){
			return;
		}

		Int64 currTime = HTime.NowTimeMs();

		Int64 thresh = m_prevMoveTime + SYNC_DEVI_MS;
		if(thresh > currTime){
			Debug.LogError("thresh exceed curr time, thresh:"+ thresh + " curr:" + currTime);
			thresh = m_prevMoveTime;	
		}

		if(thresh <= 0){
			Debug.LogError("thresh is less than 0 curr:" + m_currWaiDelta);
		}

		if(Mathf.Abs(currTime - m_prevMoveTime - SYNC_DELTA_MS) < 15 || currTime - m_prevMoveTime > SYNC_DELTA_MS){

			Debug.LogError("gap time:" + (currTime - m_prevMoveTime) + " frame count:" + m_frames.Count);

			if(m_frames.Count < 1){			
				ReSync();	
				return;
			}

			while(m_frames.Count >0){
				MovePacket packet = m_frames.ToArray()[0];
				if(packet.m_localTime <= thresh){
					packet = m_frames.Dequeue();
					ApplyMove(packet);
				}else{
					break;
				}
			}

			m_prevMoveTime = currTime;

		}


	}
}































