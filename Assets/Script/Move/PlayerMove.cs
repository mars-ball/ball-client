﻿using UnityEngine;
using System.Collections;
using ProtoBuf;
using pbmsg;
using pbmsg.room;

public class PlayerMove : MonoBehaviour {

	public int uid;
	public int unitId;
	public CircleCollider2D m_collider;
	public Transform m_trans;
	public MoveSync m_sync;

	void Awake(){
		m_trans = GetComponent<Transform>();
		m_collider = GetComponent<CircleCollider2D>();
		m_sync = MoveSync.GetInstance();
	}

	void OnTriggerStay2D(Collider2D other){
		if (uid != UserManager.GetInstance ().GetMainUser ().mBasicInfo.uid) {
			return;
		}
		//Debug.LogError("trigger enter");
		if(other.tag == "Player"){
			//Debug.LogError("is a player");
			Vector3 pos0 = m_trans.position;

			float r0 = m_trans.localScale.x;//m_collider.radius;

			PlayerMove collider = other.gameObject.GetComponent<PlayerMove>();

			if(collider.uid == uid){
				return;			
			}

			Vector3 pos1 = collider.m_trans.position;

			float r1 = collider.transform.localScale.x;//collider.m_collider.radius;
		
			float dist = (pos1 - pos0).magnitude;
			if(r0 > r1 && dist < r0/*-r1*/){
				Debug.Log("eat enemy uid:" + uid + " unitId:" + unitId + " uid2:" + collider.uid + " unit2:" + collider.unitId + " r0:" + r0 + " r1:" + r1 + " pos0:" + pos0.x + ", " + pos0.y + " pos1:" + pos1.x + ", " + pos1.y +  " dist:" + dist + " time:" + HTime.GetNowServerTime());
				m_sync.RemoveUnit(collider.uid, collider.unitId);
				

				new CSEatEnemyMessage(uid, unitId, collider.uid, collider.unitId, HTime.GetNowServerTime()).Send(
					delegate(IExtensible message){
						RoomMessage msg = (RoomMessage)message;
						if(msg.msgType != MSGID.CSEatEnemy_Response){
							Debug.LogError ("msg id error " + msg.msgType);
							return false;
						}
						CSEatEnemyResponse rsp = msg.response.eatEnemy;
						if(rsp == null){
							Debug.LogError ("create user rsp null ");
							return false;			
						}
						/*
						Ret csRet = rsp.ret;
						if(csRet == Ret.Success){

						}*/
						return true;
					}
				);
			}
		}else if(other.tag == "Mine"){
			EatMine(other.gameObject);
		}
	}

	void EatMine(GameObject mine){
	
		MineMove mineMove = mine.GetComponent<MineMove>();

		if(mineMove == null){
			Debug.LogError("no such minemove");
			return;
		}

		if (mineMove.m_mineId == 0) {
			Debug.LogError ("get a invalid mine");
			return;
		}

		Vector3 minePos = mine.transform.position;
		Vector3 pos = m_trans.position;

		float dist = (pos - minePos).magnitude;


		if(dist < m_trans.localScale.x*MoveSync.INIT_SIZE){
		
			new CSEatMineSyncUpMessage(unitId, mineMove.m_mineId, HTime.GetNowServerTime()).Send();
			Debug.LogWarning("eat mine, x:" + pos.x + ", y:" + pos.y + " mine, x:" + minePos.x + ", y:"+ minePos.y + " dist:"+ dist + " ,local R:" + m_trans.localScale.x);
			MoveSync.GetInstance().RemoveMine(mineMove.m_mineId);
		}






	
	}
}
