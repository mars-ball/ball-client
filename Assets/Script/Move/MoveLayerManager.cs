﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using UnitID=Pair<int, int>;
using UnitInfo=Pair<Pair<int, int>, float>;

public class MoveLayerManager : MonoBehaviour {
	
	public Dictionary<UnitID, int> unitToLayer;
	public LinkedList<UnitInfo> rank;
	public List<float> unitR;


	public static readonly string BATTLE_LAYER_NAME = "battleMo";
	public const int PLAYER_LAYER_BASE = 1000;

	void Awake(){
		unitToLayer = new Dictionary<UnitID, int>();
		rank = new LinkedList<UnitInfo>();
	}
		

	public void RemoveUnit(UnitID id){

		if(!unitToLayer.ContainsKey(id)){
			Debug.LogError("not contains the id:" + id);
			return;	
		}
		/*
		int layer = unitToLayer[id];

		if(layer < 0 || layer>= rank.Count){
			Debug.LogError("layer exceed the border:" + layer);
			return;		
		}*/

		for(var node = rank.First; node != rank.Last.Next; node = node.Next){

			UnitInfo info = node.Value;
			if(node.Value.First.Equals(id)){
				rank.Remove(node);
				break;
			}		
		}

		unitToLayer.Remove(id);
	
	}


	public void ChangeUnitR(UnitID id, float r){
	
		RemoveUnit(id);
		AddUnit(id, r);
	
	}

	public void AddUnit(UnitID id, float r){

		if(unitToLayer.ContainsKey(id)){
			Debug.LogError("repeated unit:" + id);
			return;		
		}			

		int idx = PLAYER_LAYER_BASE;
		bool found = false;

		if(rank.Count <1){

			rank.AddFirst(new UnitInfo(id, r));
			unitToLayer[id] = idx;
			return;		
		}

		for(var node = rank.First; node != rank.Last.Next; node = node.Next, idx++){
		
			UnitInfo info = node.Value;

			if(r < info.Second  && !found){
				rank.AddBefore(node, new UnitInfo(id, r));			
				unitToLayer[id] = idx;
				idx += 1;
				found = true;
			}

			unitToLayer[info.First] = idx;
		
		}

		if(!found){
			rank.AddLast(new UnitInfo(id, r));
			unitToLayer[id] = idx;	
		}


	}


	public int getLayerById(UnitID id){
	
		if(!unitToLayer.ContainsKey(id)){
			Debug.LogError("not allowed to readd unit:" + id);
			return PLAYER_LAYER_BASE;		
		}		

		return unitToLayer[id];
	}
		




}



























