﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using pbmsg.room;
using System;
using UnitID=Pair<int, int>;
using MineInfo=MineMgr.MineInfo;

public class MoveSync : MonoBehaviour {

	public bool isSpeedZero = false;

	private static MoveSync m_instance;

	public BattleScene m_battleScene;

	public MineMgr m_mineMgr;

	public CameraMove m_camera;

	private ResBank m_resBank;

	private PlayerUI m_playerUI;

	private RemeberLoseManager m_remLose;

	public Text m_downSpeed;

	public const long SYNC_DELTA_MS = 80;
	private long SYNC_LOOSE_WAIT = 80;
	private long SYNC_TIGHT_WAIT = 80;

	private const long MAX_RESERVER_FRAME = 3;

	private const long TEST_JOY_UP_MS = 70;
	private bool m_testOnce = false;
	private List<Vector3> m_testDirs;
	private static int m_testIndex = 0;

	private const long PIXEL_PER_UNIT = 256;
	public const float INIT_SIZE_IN_SERVER = 100;
	public const float MOVE_FORCE_SYNC_DISTANCE = 200;
	public const float UNIT_INIT_SCALE = INIT_SIZE_IN_SERVER/INIT_SIZE;
	public const float INIT_SIZE = 1.28f;
	public const float INIT_COL_R = 1.28f;

	public const float SPLIT_INIT_SPEED = 2000f;
	public const float SPLIT_DAMPING_SPEED = -1200f;

	private float m_CurrTimePercent = 1.0f;

	private Vector2 m_prevSpeed = new Vector2(0, 0);

	private Vector3 INIT_POS;

	public bool m_isStart;

	//private bool m_isWaitForSync;
	private int m_waitNum;
	private bool m_isSyncing;
	private bool m_isWaitReserveTime;
	private float m_reserveTimePoint;

	private Int64 m_prevMoveStartTime;
	private Int64 m_prevMoveServerTime;

	private HashSet<Int32> m_prePlayerIdSet;
	private HashSet<Int32> m_currPlayerIdSet;

	private HashSet<Int32> m_preMineIdSet;
	private HashSet<Int32> m_currMineIdSet;

	private Dictionary<int, Player> m_players;

	private Dictionary<int, MineMove> m_mines;


	public int m_frameSeq;
	private int m_frameSeqMod;


	public static Int64 m_currFrameServerTime;
	public static Int64 m_prevFrameServerTime;

	private Queue<MovePacket> m_frames;

	public MoveLayerManager m_layerManager;


	public class Player{

		public MoveSync m_move;
		public int m_uid;
		public int m_num;
		public Dictionary<int, Unit> m_units;
		public Unit m_mainUnit;
		public int m_photo;

		public class Unit{
			public Player m_owner;
			public int m_uid;
			public int m_unitId;
			public float m_oldR;
			public float m_r;
			public Vector2 m_toPos;
			public Vector2 m_speed;
			public Vector2 m_acc;
			public bool m_isSplitNotDel;
			public int m_splitNotDelCnt = 10;
			public bool m_isSplitKeepR;

			public GameObject m_obj;
			public Transform m_trans;
			public SpriteRenderer m_sprite;
			public PlayerMove m_move;
			public CircleCollider2D m_col;
			public Transform m_arrow;
			public TextMesh m_nameMesh;
			public MeshRenderer m_nameRender;

			public bool m_isSyncSpeed;
			public Int64 m_timeline;

			//public Rigidbody2D m_rigid;

			public const float RADIUS_CHANGE_THRESHOLD = 1;

			public Unit(int _id, int _unitId, GameObject obj){
				if(obj == null){
					Debug.LogError("create player unit null:");
					return;
				}
				m_uid = _id;
				m_unitId = _unitId;
				m_obj = obj;
				m_trans = obj.GetComponent<Transform>();
				m_move = obj.GetComponent<PlayerMove>();
				m_col = obj.GetComponent<CircleCollider2D>();
				m_sprite = obj.GetComponent<SpriteRenderer>();
				m_isSyncSpeed = true;

				GameObject arrow = m_trans.FindChild("Arrow").gameObject;
				m_arrow = arrow.GetComponent<Transform>();

				GameObject nameMeshObj = m_trans.FindChild("Name").gameObject;
				m_nameMesh = nameMeshObj.GetComponent<TextMesh>();
				m_nameRender = nameMeshObj.GetComponent<MeshRenderer>();
				m_nameRender.sortingLayerName = "Default";

				//m_rigid = obj.GetComponent<Rigidbody2D>();

				m_move.uid = _id;
				m_move.unitId = _unitId;
				m_isSplitNotDel = false;
				m_isSplitKeepR = false;
			}

			public float GetScale(){
				return m_r/INIT_SIZE;			
			}

			public void SetR(float r, bool changeLayer = false){
				float rScale = r/INIT_SIZE;
				m_trans.localScale = new Vector3(rScale, rScale, 1.0f);
				m_r = r;

				if(!changeLayer){
					m_oldR = r;
				}

				if(changeLayer && Mathf.Abs(r - m_oldR) > RADIUS_CHANGE_THRESHOLD){
					m_oldR = m_r;
					MoveSync.GetInstance().m_layerManager.ChangeUnitR(new Pair<int, int>(m_uid, m_unitId), r);					
				}
			}

			public void SetSpeed(Vector2 speed){
				m_speed = speed;			
			}

			public void SetTargetPos(Vector2 targetPos){
				m_toPos = targetPos;

				Vector3 currPos3 = m_trans.localPosition;		
				Vector2 currPos = new Vector2 (currPos3.x, currPos3.y);

				if ((targetPos - currPos).magnitude < m_speed.magnitude * 1.0f *SYNC_DELTA_MS/1000f + MOVE_FORCE_SYNC_DISTANCE) {	
					return;
				}
			
				Vector2 offsetPos = targetPos - currPos;

				if (offsetPos.magnitude < Mathf.Epsilon) {
					Debug.LogError ("to pos is too near target pos");
					return;
				}

				Vector2 dirNorm = (targetPos - currPos).normalized;

				Vector2 newPos = targetPos - dirNorm * (m_speed.magnitude * 1.0f * SYNC_DELTA_MS / 1000f);

				Debug.LogError ("reset pos, offset:" + (targetPos - currPos).magnitude + " auto move offset:" + m_speed.magnitude * 1.0f *SYNC_DELTA_MS/1000f + " speed:" + m_speed + " target:" + targetPos + " local" + m_trans.localPosition + " new Pos:" + newPos);
				m_trans.localPosition = new Vector3(newPos.x, newPos.y, 0);
			
			}

			public bool IsOk(){
				return (m_obj != null) && (m_trans != null) && (m_move != null);// && (m_rigid != null);
			}

			public void SetPos(Vector2 pos){
				m_trans.localPosition = new Vector3 (pos.x, pos.y, 0);
			}

			public void UnitUpdate(){

				if (m_isSyncSpeed) {
					float mag = m_speed.magnitude;
					if (mag < 1) {
						mag = 20;
					}
					m_trans.localPosition = Vector3.MoveTowards (m_trans.localPosition, new Vector3 (m_toPos.x, m_toPos.y, 0), mag * Time.deltaTime);
				} else {

					Int64 currTime = HTime.GetNowServerTime ();

					if (currTime > m_timeline) {
						m_isSyncSpeed = true;
						SetSpeed (new Vector2 (0, 0));
						Debug.LogError ("unitid:" + m_unitId + "fly end , currTime:" + currTime + " pos:" + m_trans.localPosition + " v:" + m_speed);
						return;
					}

					m_trans.localPosition += new Vector3 (m_speed.x, m_speed.y, 0) * Time.deltaTime;
					m_speed += m_acc * Time.deltaTime;


					//Debug.LogError ("unitId:" + m_unitId + " pos:" + m_trans.localPosition + " v:" + m_speed + " timestamp:" + HTime.NowTimeMs () + " sertime:" + HTime.GetNowServerTime ());
				
				}
			}

			public void Delete(){
				m_owner.m_move.RemoveUnitFromFrames(m_uid, m_unitId);

				m_owner.m_move.m_remLose.LoseUnit(m_uid, m_unitId);		

				m_owner.m_move.m_resBank.ReturnUnit(m_obj);	

			}
		}

		public Player(int _uid, int _photo, MoveSync _move){
			m_uid = _uid;
			m_photo = _photo;
			m_units = new Dictionary<int, Unit>();
			m_move = _move;
		}

		public void SetUnitPos(int unitId, Vector3 pos){
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("idx error:" + unitId);
				return;			
			}

			m_units[unitId].m_trans.localPosition = pos;
		}

		public bool GetUnitPos(int unitId, out Vector3 pos){
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("idx error:" + unitId);
				pos = new Vector3(0, 0, 0);
				return false;			
			}
			pos = m_units[unitId].m_trans.localPosition;
			return true;
		}

		public void SetUnitToPos(int unitId, Vector2 pos, Vector2 speed){
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("idx error:" + unitId);
				return;			
			}

			m_units [unitId].SetTargetPos (pos);
		}

		public void SetUnitSpeed(int unitId, Vector2 speed){
		
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("idx error:" + unitId);
				return;			
			}

			m_units [unitId].SetSpeed (speed);
		}
		/*
		public void SetUnitSpeedByTarget(int idx, Vector2 pos){

			Vector3 targetPos = new Vector3 (pos.x, pos.y);
			Vector3 currPos = m_units [idx].m_trans.localPosition;
			if ((targetPos - currPos).magnitude < 5) {
				return;
			}

			Vector3 speed = (targetPos - currPos) / (1.0f * SYNC_DELTA_MS / 1000.0f);

			if (speed.magnitude > MAX_SPEED) {
				speed = MAX_SPEED * speed.normalized;
			}
			m_units [idx].m_rigid.velocity = speed;

			//Debug.LogError ("seet speed by pos, pos (" + currPos.x + ", " + currPos.y + ") target (" + targetPos.x + ", " + targetPos.y + ") speed (" + speed.x + ", " + speed.y + ")" + " mag:" + (targetPos - currPos).magnitude);
		}*/



		public void SetUnitRadius(int unitId, float rScale){
			
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("idx error:" + unitId);
				return;			
			}
			
			m_units[unitId].SetR(rScale, true);
		}


		public void AddUnit(Unit unit){
			if (m_units.Count <= 0) {
				m_mainUnit = unit;
			}

			m_units[unit.m_unitId] = unit;

			MoveSync.GetInstance().m_layerManager.AddUnit(new Pair<int, int>(m_uid, unit.m_unitId), unit.m_r);
		}

		public Unit GetUnit(int unitId){

			if (!m_units.ContainsKey (unitId)) {
				return null;
			}
			return m_units [unitId];
		
		}

		public void RemoveUnit(int unitId){
			if(!m_units.ContainsKey(unitId)){
				Debug.LogError("no such unit id:" + unitId);
				return;
			}
			Unit unit = m_units[unitId];
			unit.Delete();
			m_units.Remove(unitId);

			foreach(var unitPair in m_units){
				m_mainUnit = unitPair.Value;
				break;
			}

			MoveSync.GetInstance().m_layerManager.RemoveUnit(new Pair<int, int>(m_uid, unit.m_unitId));

		}

		public Unit GetMainUnit(){
			return m_mainUnit;
		}

		public void UpdateMove(){
			foreach(var unit in m_units){
				if(!unit.Value.IsOk()){
					return;
				}			
				unit.Value.UnitUpdate ();
			}
		}

		public bool IsOk(){
			foreach(var unit in m_units){
				if(!unit.Value.IsOk()){
					return false;
				}			
			}
			return true;
		}

		public void Stop(){
			foreach(var unit in m_units){			
				if(unit.Value == null || unit.Value.m_trans == null){
					Debug.LogError("an unit is invalid, uid:" + m_uid);
					continue;
				}			
				unit.Value.SetTargetPos(unit.Value.m_trans.localPosition);
			}
		}

		public void DestroyAll(){
			foreach(var unit in m_units){
				unit.Value.Delete ();
			}
		}	
	}




	public class MovePacket{
		public Int64 m_localTime;
		public Int64 m_serverTime;
		public CSDirectMoveSycnDown m_packet;
		public MovePacket(Int64 time_, CSDirectMoveSycnDown packet_){
			m_localTime = time_;
			m_packet = packet_;
			m_serverTime = packet_.timestamp;
		}
	}


	public static MoveSync GetInstance(){

		if (m_instance == null) {
			GameObject obj = new GameObject ();
			obj.name = "Move Sync";
			m_instance = obj.AddComponent <MoveSync> ();
			m_instance.m_remLose = obj.AddComponent <RemeberLoseManager>();
			m_instance.m_remLose.m_move = m_instance;
			m_instance.m_frames = new Queue<MovePacket> ();
			m_instance.m_players = new Dictionary<int, Player>();
			m_instance.m_mines = new Dictionary<int, MineMove>();
			m_instance.m_resBank = obj.AddComponent <ResBank> ();
			m_instance.m_mineMgr = obj.AddComponent <MineMgr> ();
		}
		return m_instance;
	}


	public void Reset(){
		m_camera.StopFollow ();
		if(m_instance != null){
			GameObject obj = m_instance.gameObject;
			if(obj != null){
				GameObject.Destroy(obj);
			}
			m_instance = null;		
		}	
	}

	public void RemoveMineFromFrames(int mineId){

		foreach(var frame in m_frames){

			CSDirectMoveSycnDown move = frame.m_packet;
			/*List<CSBasicMinePos> mines = move.mines;
			mines.RemoveAll(item => item.mid == mineId);*/
			m_mineMgr.RemoveMineById (move.mineByte, mineId);
		}
	}

	public void RemoveUnitFromFrames(int uid, int unitId){
	
		Player player = m_players[uid];
		if(player.m_units.Count <1){
			RemovePlayerFromFrames(uid);
			return;
		}

		foreach(var frame in m_frames){

			CSDirectMoveSycnDown move = frame.m_packet;

			CSBasicMovement bmove = move.move.Find(item => item.uid == uid);

			if (bmove == null || bmove.units == null) {
			
				continue;
			}

			bmove.units.RemoveAll(item => item.id == unitId);	
		
		}	
	
	}


	public void RemovePlayerFromFrames(int uid){
		foreach(var frame in m_frames){

			CSDirectMoveSycnDown move = frame.m_packet;

			move.move.RemoveAll(item => item.uid == uid);
		}	
	}


	public void ReceiveSplitPacket(CSSplitResponse split, Vector3 dir, Int64 prevTimeStamp, Int64 prevFrameEndTime){

		Debug.LogError("received split");

		if (split == null) {
			Debug.LogError ("split packet null");
			return;
		}

		Int64 timestamp = HTime.GetNowServerTime();

		foreach(var move in split.moves){
			Int32 uid = move.uid;
			if(!m_players.ContainsKey(uid)){
				return;
			}
			Int32 unitId = move.unitId;
			Int64 timeline = move.timeline;
			Int32 newUnitId = move.newUnitId;

			setPlayerSplit (uid, unitId, newUnitId, dir, timeline, timestamp, prevTimeStamp, prevFrameEndTime);
		}	
	}




	public void ReceiveMovePacket(CSDirectMoveSycnDown packet){
		//Debug.LogError("recv 0");
		if(!m_isStart){
			Debug.LogError("not start yet");
			return;
		}

		if (packet.urgent != null && packet.urgent.Count > 0) {
		
			foreach (var urgent in packet.urgent) {
				if (urgent.split != null && urgent.split.Count > 0) {
					foreach (var split in urgent.split) {
						setPlayerSplitFromUrgent (packet.move, split);					
					}
				} else if (urgent.splitEnd != null && urgent.splitEnd.Count > 0) {
					foreach (var splitEnd in urgent.splitEnd) {
						setPlayerSplitEndFromUrgent (packet.move, splitEnd);
					
					}				
				}
			}		
		}

		if (!m_isSyncing && m_frames.Count == 0) {
			//HTime.SyncTime(packet.timestamp);
			m_isSyncing = true;
			m_isWaitReserveTime = true;
			m_reserveTimePoint = HTime.GetTimeMs () + SYNC_DELTA_MS/2;
			Debug.LogError ("resync receive first packet, time:" + HTime.GetTimeMs () + " reserve point:" + m_reserveTimePoint);
		}
		/*
		foreach (var move in packet.move) {
			if (UserBasicDataManager.GetInstance ().GetData ().id == move.uid) {
				Debug.LogWarning("receive move down:" + move.units [0].v.x + ", " + move.units [0].v.y + " recvv packet, time:" + Time.fixedTime + " frame count:" + m_frames.Count + " sertime:" + packet.timestamp);

			}
		}*/

		//Debug.LogError("recv 4");

		if (m_frames.Count > 0 && packet.timestamp < m_frames.Peek ().m_packet.timestamp) {
			AddFrameWithSort (new MovePacket (packet.timestamp, packet));
		} else {
			m_frames.Enqueue (new MovePacket (packet.timestamp, packet));

			if (!m_isSyncing || (m_isSyncing && m_isWaitReserveTime)) {
				Debug.LogError ("syning frame count:" + m_frames.Count + " time:" + HTime.GetTimeMs());
			}

		}
	}

	void AddFrameWithSort(MovePacket packet){
		Queue<MovePacket> newQ = new Queue<MovePacket> ();
		while (m_frames.Count > 0) {

			if (m_frames.Peek ().m_packet.timestamp < packet.m_packet.timestamp) {
				newQ.Enqueue (m_frames.Dequeue ());
			} else {
				break;
			}
		}

		newQ.Enqueue (packet);
		while (m_frames.Count > 0) {
			newQ.Enqueue (m_frames.Dequeue ());
		}
		m_frames = newQ;

	}
		
	Player.Unit CreateNewUnit(Player owner, int uid, CSBasicMovementUnit move){
		/*GameObject newObj = Instantiate(Resources.Load("player_unit")) as GameObject;
		GameObject textObj = newObj.transform.FindChild("Name").gameObject;
		TextMesh nameMesh = textObj.GetComponent<TextMesh>();
		nameMesh.text = ow
		SpriteRenderer render = newObj.GetComponent<SpriteRenderer>();
		Texture2D tex = PhotoGen.GetInstance().getPhotoTexture(owner.m_photo, (int)(render.sprite.rect.width), (int)(render.sprite.rect.height));
		if(tex == null){
			Debug.LogError("tex create failed");
			return null;
		}
		render.sprite = Sprite.Create(tex, render.sprite.rect, new Vector2(0.5f, 0.5f));*/

		GameObject newObj = m_resBank.GetUnit (owner.m_photo);

		if (newObj == null) {
			Debug.LogError ("new obj is null");
			return null;		
		}

		Player.Unit unit = new Player.Unit(uid, move.id, newObj);
		unit.m_uid = uid;
		unit.m_unitId = move.id;
		unit.m_r = move.r;
		unit.m_owner = owner;

		if (owner.m_move.m_battleScene.m_playerInfos.ContainsKey (uid)) {
			var info = owner.m_move.m_battleScene.m_playerInfos[uid];
			unit.m_nameMesh.text = info.name;
		}

		unit.SetR (move.r);
		unit.SetSpeed (new Vector2(move.v.x, move.v.y));
		unit.SetPos (new Vector2(move.pos.x, move.pos.y));
		unit.SetTargetPos (new Vector2(move.pos.x, move.pos.y));
		return unit;
	}


	Player CreateNewPlayer(int uid, CSBasicMovement move){

		if(m_players.ContainsKey(uid)){
			Debug.LogError("already in use");
			return null;
		}

		Player player = new Player(uid, move.photoId, this);

		foreach(CSBasicMovementUnit moveUnit in move.units){
			Player.Unit unit = CreateNewUnit (player, uid, moveUnit);
			player.AddUnit(unit);

		}	

		return player;
	}


	public void stopAllPlayer(){
	
		foreach(var pl in m_players){
		
			Player player = m_players[pl.Key];
			if(player == null){
				continue;
			}

			player.Stop();		
		
		}	
	}

	public void setPlayerSplitFromUrgent(List<CSBasicMovement> moves, CSMoveUrgentSplit split){

		int uid = split.uid;
		int unitId = split.unitId;
		int newUnitId = split.newUnitId;
		Int64 timeline = split.timeline;

		Player player = m_players [uid];
		if (!m_players.ContainsKey (uid)) {
			Debug.LogError ("not contain unit, uid:" + uid);
			return;
		}

		CSBasicMovementUnit unitMov = null;
		foreach (var move in moves) {
			if (move.uid == uid) {
				foreach (var uu in move.units) {
				
					if (uu.id == newUnitId) {
						unitMov = uu;
						break;
					}
				}
				break;				
			}		
		}

		if (unitMov == null) {
			Debug.LogError ("unit is null");
			return;		
		}

		GameObject newObj = m_resBank.GetUnit(player.m_photo);
		if (newObj == null) {
			Debug.LogError ("new obj is null");
			return;		
		}

		Player.Unit newUnit = new Player.Unit (uid, newUnitId, newObj);

		player.AddUnit (newUnit);

		newUnit.SetR (unitMov.r, true);
		newUnit.m_owner = player;
		newUnit.m_isSplitNotDel = true;
		newUnit.m_isSyncSpeed = false;
		newUnit.m_timeline = split.timeline;
		newUnit.m_speed = new Vector3(unitMov.v.x, unitMov.v.y, 0);

		Vector2 speedDir = new Vector2 (unitMov.v.x, unitMov.v.y);

		newUnit.m_acc = speedDir * SPLIT_DAMPING_SPEED;

	
	}


	public void setPlayerSplitEndFromUrgent(List<CSBasicMovement> moves, CSMoveUrgentSplitEnd splitEnd){

		int uid = splitEnd.uid;
		int unitId = splitEnd.unitId;

		if (!m_players.ContainsKey (uid)) {
			Debug.LogError ("no uid:" + uid);
			return;
		}

		Player player = m_players [uid];

		if (!player.m_units.ContainsKey (unitId)) {
			Debug.LogError ("no such unit, uid:" + uid + " unitId:" + unitId);
			return;
		}

		Player.Unit unit = player.m_units [unitId];

		if (unit == null || unit.m_isSyncSpeed) {
		
			Debug.LogError ("unit is null?" + (unit == null) + " is  sync uid:" + uid + " unitId:" + unitId);
			return;		
		}

		unit.m_isSyncSpeed = true;

	}




	public void setPlayerSplit(int uid, int unitId, int newUnitId, Vector3 dir, Int64 timeline, Int64 timestamp, Int64 prevTimestamp, Int64 prevEndFrameTime){

		if (prevTimestamp < prevEndFrameTime) {
			Debug.LogError ("split timestamp error, prevTimestamp:" + prevTimestamp + " prev end:" + prevEndFrameTime);
		}


		//return true;
		if(!m_players.ContainsKey(uid)){
			return;
		}

		Player player = m_players [uid];

		if (!m_players.ContainsKey (uid)) {
		
			Debug.LogError ("not contain unit, uid:" + uid);
			return;
		
		}


		Player.Unit unit = player.m_units [unitId];

		if (!player.m_units.ContainsKey (unitId)) {

			Debug.LogError ("not contain unit, uid:" + uid + " unitId:" + unitId);
			return;

		}


		//GameObject newObj = Instantiate(Resources.Load("player_unit")) as GameObject;
		GameObject newObj = m_resBank.GetUnit(player.m_photo);
		if (newObj == null) {
			Debug.LogError ("new obj is null");
			return;		
		}

		Player.Unit newUnit = new Player.Unit (uid, newUnitId, newObj);

		player.AddUnit (newUnit);

		Vector2 initSpeed = dir * SPLIT_INIT_SPEED;
		Vector2 initAcc = dir * SPLIT_DAMPING_SPEED;

		float newR = unit.m_r / Mathf.Sqrt (2.0f);
		float timeDeltaS = 1.0f * (timestamp - prevTimestamp) / 1000f;

		newUnit.SetR (newR, true);
		newUnit.m_owner = player;
		newUnit.m_isSplitNotDel = true;
		newUnit.m_isSyncSpeed = false;
		newUnit.m_timeline = timeline;
		newUnit.m_acc = initAcc;

		if (m_battleScene.m_playerInfos.ContainsKey (uid)) {
			var info = m_battleScene.m_playerInfos[uid];
			newUnit.m_nameMesh.text = info.name;
		}

		if (!player.m_units.ContainsKey (unitId)) {
			Debug.LogError ("no unit before split");
			return;
		}

		/*
		unit.m_speed = new Vector2 (0, 0);
		unit.m_acc = new Vector2 (0, 0);
		unit.m_isSyncSpeed = false;
		unit.m_timeline = timeline;
		unit.SetR (newR, true);
		unit.m_isSplitKeepR = true;
*/
		Vector3 oldPos = unit.m_trans.position;


		Vector2 newInitPos = oldPos + dir * 2.0f * newR;

		Vector2 speed = initSpeed;
		Vector2 acc = initAcc;
		Vector2 pos = newInitPos;
		Int64 timeMs = prevTimestamp;

		int frameNUm = (int)Mathf.Round(1.0f * (m_currFrameServerTime - prevEndFrameTime) / SYNC_DELTA_MS) + 1;

		for (int ii = 0; ii < frameNUm + 1 && timeMs < timeline && timeMs < m_currFrameServerTime + SYNC_DELTA_MS + 10; ii++) {

			if (ii == 0) {
				pos += speed * 1.0f * (prevEndFrameTime - prevTimestamp) / 1000.0f;
			} else if (ii == frameNUm + 1) {
				Int64 tmp = timestamp - timeMs;

				if(tmp > SYNC_DELTA_MS){
				
					tmp = SYNC_DELTA_MS;
				
				}


				pos += speed * 1.0f * tmp/ 1000.0f;

			}else{
				pos += speed * 1.0f * SYNC_DELTA_MS/1000.0f;
			}


			if (ii == 0) {
				timeMs += (prevEndFrameTime - prevTimestamp);
				speed += acc * 1.0f * (prevEndFrameTime - prevTimestamp) / 1000f;
			} else if (ii == frameNUm + 1) {
				Int64 tmp = timestamp - timeMs;

				if(tmp > SYNC_DELTA_MS){

					tmp = SYNC_DELTA_MS;

				}
				timeMs += tmp;

				speed += acc*1.0f * tmp/1000.0f;;

			}else{
				timeMs += SYNC_DELTA_MS;
				speed += acc*1.0f * SYNC_DELTA_MS/1000.0f;;
			}



		}

		newUnit.SetPos (pos);
		newUnit.SetSpeed (speed);



		Debug.LogError("received split: unitId" + unitId + " dir:" + dir + " timeline:" + timeline + " timestamp:" + timestamp + " pos:" + newUnit.m_trans.localPosition);
		/*newUnit.m_speed = speed;
		newUnit.m_toPos = toPos;

		Int64 flyPassTime = LocalTimeToServer (GetFixedMs()) - timestamp;

		if (flyPassTime <= 0) {
			flyPassTime = 0;
		}

		Vector2 newPos = fromPos + speed*flyPassTime/1000.0f;
		*/



		return;
	}



	List<Vector3> giz;

	void OnDrawGizmos(){

		foreach (var pos in giz) {
			Gizmos.DrawSphere (pos, 1);
		
		}

	}

	public int setPlayerMove(int uid, CSBasicMovement move, long packetTime){
		//Debug.LogError("set player move uid:" + uid);


		if(!m_players.ContainsKey(uid)){

			if(m_remLose.isPlayerInRemeberLose(move)){
				return 0;			
			}

			Player newPlayer = CreateNewPlayer(uid, move);
			if(newPlayer == null){
				Debug.LogError("new user null");
				return -1;			
			}
			m_players[uid] = newPlayer;
		}

		Player player = m_players[uid];

		if(!player.IsOk()){
			Debug.LogError("player not full:" + uid);

			return -1;
		}

		if(move.units.Count < player.m_units.Count){
			HashSet<int> idSet = new HashSet<int>();
			HashSet<int> rmIdSet = new HashSet<int>();
			foreach(var unit in move.units){
				idSet.Add(unit.id);			
			}

			foreach(var unit in player.m_units){
				if (idSet.Contains (unit.Key)) {
					continue;
				}

				if (unit.Value.m_isSplitNotDel) {
					if (unit.Value.m_splitNotDelCnt <= 0) {
						rmIdSet.Add (unit.Key);
					} else {
						unit.Value.m_splitNotDelCnt -= 1;
					}
				}else{
					rmIdSet.Add(unit.Key);
				}
			}

			foreach(var rmId in rmIdSet){
				//player.m_units[rmId].Delete();
				//player.m_units.Remove(rmId);
				player.RemoveUnit(rmId);
				Debug.LogError ("remove unit, uid:" + uid + " unit id:" + rmId);
			}		
		}

		foreach(var unit in move.units){
			//Debug.LogWarning ("move unitId:" + unit.id + " local pos:(" + player.GetMainUnit().m_trans.localPosition.x + ", " + player.GetMainUnit().m_trans.localPosition.y +") " + " to pos :(" + unit.pos.x + "," + unit.pos.y + ") speed :(" + unit.v.x + ", " + unit.v.y + ") frame count:" + m_frames.Count + " packet time:" + packetTime + " frame time:" + m_currFrameServerTime);

			giz.Add (player.GetMainUnit ().m_trans.position);

			if (!player.m_units.ContainsKey (unit.id)) {
				if(m_remLose.isUnitInRememberLose(uid, unit.id)){				
					continue;
				}

				Player.Unit newUnit = CreateNewUnit (player, uid, unit);
				player.AddUnit (newUnit);
				continue;
			}

			Player.Unit playerU = player.GetUnit(unit.id);
			if( playerU == null){
				continue;
			}

			if (!playerU.m_isSyncSpeed) {

				Int64 currTimeStamp = HTime.GetNowServerTime ();

				if (currTimeStamp > playerU.m_timeline) {
					playerU.m_isSyncSpeed = false;
					playerU.m_speed = new Vector2 (0, 0);
					playerU.m_acc = new Vector2 (0, 0);
				} else {
					continue;
				
				}
			
			}

			Int32 unitId = unit.id;

			pbmsg.room.Vector pos = unit.pos;
			pbmsg.room.Vector speed = unit.v;

			if (player.m_units [unitId].m_isSplitNotDel) {
				player.m_units [unitId].m_isSplitNotDel = false;
				player.GetMainUnit ().m_isSplitKeepR = false;
			}

			//set speed has to be ahead of set topos
			player.SetUnitSpeed(unitId, new Vector2(speed.x, speed.y));
			player.SetUnitToPos (unitId, new Vector2(pos.x, pos.y), new Vector2(speed.x, speed.y));

			/*
			if(speed.
			Vector3 newPos = new Vector3(pos.x, 0, pos.y);
			Vector3 oldPos;
			if(!player.GetUnitPos(unitId, out oldPos)){
				Debug.LogError("get Pos failed");
				return -1;
			}

			Vector3 newSpeed = (newPos - oldPos)/(SYNC_DELTA_MS/1000.0f);
			*/



			if (!player.m_units [unitId].m_isSplitKeepR) {
				player.SetUnitRadius (unitId, unit.r);
			}

		}

		if(move.uid == UserManager.GetInstance().GetMainUser().mBasicInfo.uid){
		
			if(move.score != null){			
				m_battleScene.m_scoreText.text = move.score.ToString();
			}

			m_playerUI.UpdateArrow(GetMainUnit());
		
		}

		return 0;
	}

	public void SetMines(List<uint> mineBytes){

		List<int> mineIndex = m_mineMgr.getAllMineIndex (mineBytes);

		if (mineIndex == null) {
			Debug.LogError ("mine index failed");
			return;
		}

		Player.Unit mainUnit = GetMainUnit();
		if (mainUnit == null) {
			Debug.LogError ("main unit is null");
			return;
		}

		List<int> filteredIndexes = m_mineMgr.FilterByDetectRadius(mineIndex, mainUnit.m_trans.localPosition);
		if (filteredIndexes == null) {
			Debug.LogError ("filter index failed");
			return;		
		}

		foreach(int index in filteredIndexes){

			MineInfo mine = m_mineMgr.GetMineInfoByIndex (index);
			if (mine == null) {
				Debug.LogError ("mine null, index:" + index);
				continue;			
			}

			if (!m_mines.ContainsKey (mine.mid) && !m_remLose.isMineInRememberLose (mine.mid)) {

				int mineColorId = mine.colorId;

				/*
				Color mineColor = MineGen.GetInstance().getColorById(mineColorId);
				if(mineColor == null){
					Debug.LogError("no such color:" + mineColorId);
					return;
				}

				GameObject newObj = Instantiate(Resources.Load("MineNormal")) as GameObject;
				newObj.transform.position = new Vector3(mine.pos.x, mine.pos.y, 0);
				//newObj.transform.localScale = new Vector3(10f, 10f, 0);
				SpriteRenderer render = newObj.GetComponent<SpriteRenderer>();

				render.color = new Color(mineColor[0], mineColor[1], mineColor[2]);*/

				GameObject newObj = m_resBank.GetMine (mineColorId);
				newObj.transform.position = new Vector3 (mine.pos.x, mine.pos.y, 0);

				MineMove mineMove = newObj.GetComponent<MineMove> ();
				mineMove.m_mineId = mine.mid;

				m_mines [mine.mid] = mineMove;

				m_currMineIdSet.Add (mine.mid);
			} else if (!m_remLose.isMineInRememberLose (mine.mid)) {
				m_currMineIdSet.Add (mine.mid);
			}
		}
	}



	public void ApplyMove(MovePacket packet){
		HTime.SyncTime(m_currFrameServerTime);
		HTime.SetServerPacketTime (packet.m_serverTime);

		//Debug.LogWarning ("sync time, server:" + m_currFrameServerTime + " local time:" + HTime.GetTimeMs());

		m_frameSeq = (m_frameSeq + 1)%m_frameSeqMod;

		CSDirectMoveSycnDown movePacket = packet.m_packet;
		List<CSBasicMovement> moves = movePacket.move;

		if(moves == null || moves.Count == 0){
			Debug.Log("move packet error");
			return;
		
		}

		foreach(CSBasicMovement move in moves){
			
			int ret = setPlayerMove (move.uid, move, packet.m_serverTime);

			if (ret != 0) {
				Debug.LogError ("set player move failed");
				return;
			}	

			m_currPlayerIdSet.Add (move.uid);

		}	

		if(movePacket.mineByte != null && movePacket.mineByte.Count > 0){
			SetMines(movePacket.mineByte);		
		}

		HandleIdSet();

		if(movePacket.rank != null && movePacket.rank.Count > 0){
			BattleScene.GetInstance().SetRankBoard(movePacket.rank);
		
		}

		if(movePacket.newPlayer != null && movePacket.newPlayer.Count > 0){

			foreach(var player in movePacket.newPlayer){
				m_battleScene.m_playerInfos[player.uid] = new BattleScene.GamePlayerInfo(player.uid, player.name, player.photoId);			
			}		
		}


		foreach(var playerDic in m_players){
			Player player = playerDic.Value;
		
			foreach(var unitPair in player.m_units){
				Player.Unit unit = unitPair.Value;

				UnitID unitId = new UnitID(player.m_uid, unit.m_unitId);

				int layer = m_layerManager.getLayerById(unitId);

				unit.m_sprite.sortingOrder = layer;
				unit.m_nameRender.sortingOrder = layer + 1;

			}		
		}
	}


	public void HandleIdSet(){
	
		m_prePlayerIdSet.ExceptWith(m_currPlayerIdSet);

		foreach(int uid in m_prePlayerIdSet){
			if(!m_players.ContainsKey(uid)){
				continue;
			}
			if (!m_players.ContainsKey (uid)) {
				Debug.LogError ("players not contain uid:" + uid);
				continue;
			}

			Player player = m_players [uid];
			player.DestroyAll ();
			m_players.Remove(uid);
		}	

		m_preMineIdSet.ExceptWith (m_currMineIdSet);

		foreach (int mineId in m_preMineIdSet) {
			if (!m_mines.ContainsKey (mineId)) {
				continue;			
			}
			if (!m_mines.ContainsKey (mineId)) {
				Debug.LogError ("mines not contain mineid:" + mineId);
				continue;
			}
			MineMove mineMove = m_mines[mineId];
			m_mines.Remove (mineId);
			m_resBank.ReturnMine (mineMove.gameObject);

		}

		m_prePlayerIdSet = m_currPlayerIdSet;
		m_currPlayerIdSet = new HashSet<int> ();

		m_preMineIdSet = m_currMineIdSet;
		m_currMineIdSet = new HashSet<int> ();

	
	}

	public void StartMove(){
		m_isStart = true;
		m_isSyncing = false;
		m_isWaitReserveTime = true;
		m_prevMoveStartTime = 0;
		SYNC_LOOSE_WAIT = SYNC_TIGHT_WAIT = SYNC_DELTA_MS;
	}
		
	public void ReSync(){
		m_isSyncing = false;
		m_isWaitReserveTime = false;
		m_frames.Clear();
		m_prevMoveStartTime = 0;
		SYNC_LOOSE_WAIT = SYNC_TIGHT_WAIT = SYNC_DELTA_MS;
		stopAllPlayer ();
	}

	private Rigidbody2D m_player_test;
	private Rigidbody2D m_player_test1;
	private Rigidbody2D m_player_test2;
	private Rigidbody2D m_player_test3;
	private Rigidbody2D m_player_test4;
	private Rigidbody2D m_player_test5;

	// Use this for initialization
	void Awake () {
		INIT_POS = new Vector3(0, -100, 0);
		m_isSyncing = false;
		m_isStart = false;
		m_isWaitReserveTime = false;

		m_prePlayerIdSet = new HashSet<Int32>();
		m_currPlayerIdSet = new HashSet<Int32>();

		m_preMineIdSet = new HashSet<int> ();
		m_currMineIdSet = new HashSet<int> ();

		m_layerManager = this.gameObject.AddComponent<MoveLayerManager>();
		m_playerUI = this.gameObject.AddComponent<PlayerUI>();

		MineGen.GetInstance().LoadAllMine();

		GameObject camera = GameObject.Find("/Main Camera");
		if(camera == null){
			Debug.LogError("no camera");
			return;		
		}

		m_camera = camera.GetComponent<CameraMove>();

		if(m_camera == null){
			Debug.LogError("camera move module is missing");
			return;			
		}

		m_frameSeq = 0;
		m_frameSeqMod = Int32.MaxValue;

		m_testDirs = new List<Vector3> ();
		m_testDirs.Add (new Vector3 (1, 0, 0));
		m_testDirs.Add (new Vector3 (0, 1, 0));
		m_testDirs.Add (new Vector3 (-1, 0, 0));
		m_testDirs.Add (new Vector3 (0, -1, 0));

		giz = new List<Vector3> ();
	}





	void Start(){
		m_playerUI.m_joy = m_battleScene.m_joy;
	
	
	}
		

	// Update is called once per frame
	void Update () {

		//Debug.Log ("time delta :" + GetFixedMs () + " prev:" + m_prevMoveStartTime + " waiting bool:" + m_isWaitReserveTime + " reserve time:" + m_reserveTimePoint);
		if(!m_isStart){
			return;
		}

		if(!m_isSyncing){
			return;
		}

		Int64 currTime = HTime.GetTimeMs ();
		bool syncSucceed = false;

		//Debug.Log ("currTime :" + currTime + " delta:" + (currTime - m_prevMoveStartTime));

		if (m_isWaitReserveTime){
			Debug.LogError ("wait reserve, time:" + currTime + " reserve time:" + m_reserveTimePoint + " frame count:" + m_frames.Count + " prev start:" + m_prevMoveStartTime);
			if (currTime < m_reserveTimePoint && m_frames.Count <= 1) {
				return;
			} else {
				m_isWaitReserveTime = false;
				syncSucceed = true;
			}
		}

		if (syncSucceed) {
			Debug.LogError ("sync succeed, time:" + currTime + "frame count:" + m_frames.Count + " prev start:" + m_prevMoveStartTime + " loose:" + SYNC_LOOSE_WAIT + " tight:" + SYNC_TIGHT_WAIT);
		}

		do {

			if(!syncSucceed){
				if (currTime < m_prevMoveStartTime + SYNC_LOOSE_WAIT) {
					break;		
				}
			}
			//Debug.LogError ("here 3");

			if (m_frames.Count < 1) {
				ReSync ();
				Debug.LogError ("resync");
				break;
			}
			//Debug.LogError ("here 4");

			MovePacket packet = m_frames.Dequeue ();
			MovePacket nextPacket = null;

			if (m_frames.Count > MAX_RESERVER_FRAME) {
				do {
					packet = m_frames.Dequeue ();
				} while(m_frames.Count > MAX_RESERVER_FRAME);
			}


			long offset = currTime - (m_prevMoveStartTime + SYNC_TIGHT_WAIT);

			if(!syncSucceed){

				SYNC_TIGHT_WAIT = SYNC_DELTA_MS - offset;
				if(SYNC_TIGHT_WAIT < 0){
					SYNC_TIGHT_WAIT = 0;
				}
				SYNC_LOOSE_WAIT = SYNC_DELTA_MS - 2*offset;
				if(SYNC_LOOSE_WAIT < 0){
					SYNC_LOOSE_WAIT = 0;
				}

				m_currFrameServerTime = m_prevFrameServerTime;
			}else{
				m_currFrameServerTime = packet.m_serverTime - SYNC_DELTA_MS;
			}

			//Debug.LogError("offset:" + offset + " tight:" + SYNC_TIGHT_WAIT + " loose:" + SYNC_LOOSE_WAIT + " frames count:" + m_frames.Count + " prev start:" + m_prevMoveStartTime);

			m_prevMoveStartTime = currTime;

			/*
			if (currTime > m_prevMoveStartTime + TEST_JOY_UP_MS && !m_testOnce) {
				m_testOnce = true;
				new DirectMoveSyncDUpMessage(m_testDirs[m_testIndex], 1.0f).Send();
				Debug.LogError("send " + m_testIndex + " move sync:" + m_testDirs[m_testIndex]);

				m_testIndex = (m_testIndex + 1) % m_testDirs.Count;
			}

			if (nextPacket != null) {
				Debug.LogError ("move sertime:" + packet.m_packet.timestamp + "target sertime:" + nextPacket.m_packet.timestamp);
			}*/

			ApplyMove (packet);

			m_prevFrameServerTime = packet.m_serverTime;

		} while(false);

		foreach (var player in m_players) {
			player.Value.UpdateMove ();
		}

		Player mainPlayer = GetMainPlayer();
		if(mainPlayer == null){
			Debug.LogError("get main player is null");
			return;
		}

		m_camera.SetTarget(mainPlayer);


	}

	public Player GetMainPlayer(){

		do{
			UserInfo mainUser = UserManager.GetInstance().GetMainUser();
			if(mainUser == null){
				Debug.LogError("user info is null");
				break;			
			}

			if(mainUser.mBasicInfo == null){
				Debug.LogError("main user basic is null");
				break;
			}

			int mainId = mainUser.mBasicInfo.uid;

			if(!m_players.ContainsKey(mainId)){
				Debug.LogError("player not contain id:"+ mainId);
				break;			
			}

			Player mainPlayer = m_players[mainId];

			return mainPlayer;

		}while(false);
		return null;
	}

	public Player.Unit GetMainUnit(){
		Player mainPlayer = GetMainPlayer();
		if(mainPlayer == null){
			Debug.LogError("main palyer is null");
			return null;		
		}
		return mainPlayer.GetMainUnit();	
	
	}



	public void RemoveUnit(int uid, int unitId){

		if(!m_players.ContainsKey(uid)){
			Debug.LogError("no such uid:"+ uid);
			return;
		}

		Player player = m_players[uid];

		player.RemoveUnit(unitId);	



	}

	public void RemoveMine(int mineId){
		if(m_mines.ContainsKey(mineId)){			
			m_resBank.ReturnMine(m_mines[mineId].gameObject);
			m_mines.Remove(mineId);
		}
		RemoveMineFromFrames(mineId);

		m_remLose.LoseMine (mineId);

	}



}































