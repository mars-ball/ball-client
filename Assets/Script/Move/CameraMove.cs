﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

	private MoveSync.Player m_targetPlayer;
	private Transform m_trans;
	private Vector3 m_targetPos;
	private float m_targetSize;
	public const float FOLLOW_SPEED = 2f;
	public const float Z_POS = -10;
	public bool stop = false;

	public Vector3 m_vel;
	public float SMOOTH_TIME = 0.08f;

	private float prevSingleSize;

	private Camera m_camera;
	//private double m_camera_original_size;

	//public int PLAYER_SIZE_INIT = 66;
	//public int CAMREA_SIZE_INIT = 800;

	public const float CAMERA_MULTI_TAGET_EDGE_BUFF = 50f;
	public const float CAMERA_SINGLE_TARGET_EDGE_BUFF = 700f;

	//private Vector3 m_previous_scale;

	//private float delta_mass = 0;

	private float m_zoomSpeed;
	private Vector3 m_moveSpeed;
	private const float DAMP_TIME = 0.2f;

	void Awake(){
		SetInitPostioin(Vector3.zero);	
		m_camera = gameObject.GetComponent<Camera> ();
		prevSingleSize = MoveSync.INIT_SIZE_IN_SERVER + CAMERA_SINGLE_TARGET_EDGE_BUFF;
		//m_previous_scale = m_trans.localScale;
		//m_camera_original_size = m_camera.orthographicSize;
	}


	public void SetInitPostioin(Vector3 target){
		m_trans = transform;
		transform.position = target;
		SetTargetPostion(target);
	
	}

	public void SetTargetPostion(Vector3 target){
		m_targetPos = target;	
	}

	public void SetTarget(MoveSync.Player target){
		m_targetPlayer = target;
	}

	public void StopFollow(){
		stop = true;
	}
		
	/*
	// Update is called once per frame
	void Update () {
		if (stop) {
			return;
		}

		//transform.position = Vector3.Lerp (transform.position, m_targetPos, Time.deltaTime * FOLLOW_SPEED);
		if(m_targetTrans == null){
			Debug.LogError("target transform is missing");
			return;		
		}

		transform.position = Vector3.SmoothDamp(transform.position, new Vector3(m_targetTrans.position.x, m_targetTrans.position.y, Z_POS), ref m_vel, SMOOTH_TIME);

		// Smoothly zoom in/out camera, along with the change of player's size
		if (m_previous_scale != m_targetTrans.localScale) {
			
//			m_camera.orthographicSize = (CAMREA_SIZE_INIT / PLAYER_SIZE_INIT) * Mathf.Lerp(m_previous_scale.x, m_targetTrans.localScale.x, 1.0f);

			// if there is still some previous animation left
			if (delta_mass != 0) {
				m_camera.orthographicSize += delta_mass;
			}

			delta_mass = m_targetTrans.localScale.x - m_previous_scale.x;
			print ("=========" + delta_mass);

			m_previous_scale = m_targetTrans.localScale;
		}
			
		if (delta_mass >= 1) {
			m_camera.orthographicSize++;
			delta_mass -= 1;
		} else if (delta_mass <= -1) {
			m_camera.orthographicSize--;
			delta_mass += 1;
		} else {
			delta_mass = 0;
		}
	}
	*/

	void Update () {
		if (stop) {
			return;
		}
		if(m_trans == null){
			Debug.LogError("camera trans is missing");
			return;		
		}

		if (m_targetPlayer == null) {
			return;
		}

		UpdatePos ();

		UpdateSize ();



	}


	private void UpdatePos(){

		FindAveragePosition ();

		m_trans.position = Vector3.SmoothDamp(transform.position, m_targetPos, ref m_moveSpeed, DAMP_TIME);
	}

	private void UpdateSize(){
		float unitSize = m_targetPlayer.m_mainUnit.m_r;
		float size = unitSize + CAMERA_SINGLE_TARGET_EDGE_BUFF;

		if (m_targetPlayer.m_units.Count == 1) {
			m_targetSize = size;
			prevSingleSize = size;
		} else if (m_targetPlayer.m_units.Count > 1) {

			float multiSize = FindRequiredSize ();
			if (multiSize > prevSingleSize) {
				size = multiSize;
			}

			m_targetSize = size;
		}

		m_camera.orthographicSize = Mathf.SmoothDamp (m_camera.orthographicSize, m_targetSize, ref m_zoomSpeed, DAMP_TIME);
	}


    private void FindAveragePosition ()
    {
        Vector3 averagePos = new Vector3 ();
		int numTargets = 0;


		foreach(var unitEntry in m_targetPlayer.m_units)
        {			
			MoveSync.Player.Unit unit = unitEntry.Value;

			if (!unit.m_trans)
                continue;

			averagePos += unit.m_trans.position;
			numTargets++;
        }

        if (numTargets > 0)
            averagePos /= numTargets;
		
		averagePos.z = Z_POS;

		SetTargetPostion (averagePos);
        //m_DesiredPosition = averagePos;
    }

    private float FindRequiredSize ()
    {

        float size = 0f;

		foreach(var unitEntry in m_targetPlayer.m_units)
		{			
			MoveSync.Player.Unit unit = unitEntry.Value;

			if (!unit.m_trans)
				continue;

			Vector3 targetLocalPos = unit.m_trans.position;

            Vector3 desiredPosToTarget = targetLocalPos - m_targetPos;

			size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y + unit.m_r));

			size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x + unit.m_r) / m_camera.aspect);
        }

		size += CAMERA_MULTI_TAGET_EDGE_BUFF;



        return size;
    }


}











































