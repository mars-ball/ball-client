﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg.room;


public class MineMgr : MonoBehaviour {

	public class MineInfo{

		public int mid;
		public int index;
		public int colorId;
		public MineType type;
		public Vector2 pos;

	}

	private List<MineInfo> m_mineByIndex;
	private Dictionary<int, MineInfo> m_mineById;
	public float m_detectRadius;
	public int m_mineNum;

	public void InitDetectRadius(){

		//Vector3 leftBottom = new Vector3 (0, 0, 0);
		//Vector3 rightTop = new Vector3 (Screen.width, Screen.height, 0);
		Camera cam = Camera.main;

		Vector3 leftBottomWorld = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, cam.nearClipPlane));
		Vector3 rightTopWorld = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, cam.nearClipPlane));
		Vector3 diag = rightTopWorld - leftBottomWorld;
		//Vector3 diag = rightTop - leftBottom;
		float diagMag = diag.magnitude;

		m_detectRadius = diagMag * 0.6f;


	}

	public void RemoveMine(List<uint> mineBytes, int index){

		int bigIndex = (int)Mathf.Floor (index / 8);
		int smallIndex = index % 8;

		if (bigIndex < 0 || bigIndex >= mineBytes.Count
			|| smallIndex < 0 || smallIndex >= 8
		) {
			Debug.LogError ("bigIndex:" + bigIndex + " smallIndex:" + smallIndex);
			return;
		}
		uint one = 1u << smallIndex;
		uint flip = ~one;
		mineBytes [bigIndex] = mineBytes [bigIndex] & flip;

	}
		
	public void RemoveMineById(List<uint> mineBytes, int mineId){

		if (!m_mineById.ContainsKey (mineId)) {
			Debug.LogError ("no such mineId:" + mineId);
			return;
		}

		RemoveMine (mineBytes, m_mineById [mineId].index);

	}

	public List<int> getAllMineIndex(List<uint> mineBytes){

		List<int> res = new List<int> ();
		int index = 0;
		for (int ii = 0; ii < mineBytes.Count; ii++) {
			uint bits = mineBytes [ii];
			for (int jj = 0; jj < 8 && index < m_mineNum; jj++, index++) {
				uint one = 1u << jj;
				uint check = one & bits;
				if (check != 0) {
					res.Add (index);
					
				}			
			}
		
		}
		return res;
	}

	public void Awake(){
		m_mineById = new Dictionary<int, MineInfo> ();
		m_mineByIndex = new List<MineInfo> ();
		InitDetectRadius ();
	
	}


	public void InitFromInitSync(List<CSBasicMinePos> mines){
		m_mineNum = mines.Count;

		foreach (var mine in mines) {
		
			MineInfo mineInfo = new MineInfo ();
			mineInfo.mid = mine.mid;
			mineInfo.colorId = mine.color;
			mineInfo.pos = new Vector2 (mine.pos.x, mine.pos.y);
			mineInfo.type = mine.type;
			mineInfo.index = m_mineByIndex.Count;
			m_mineByIndex.Add (mineInfo);
			m_mineById [mineInfo.mid] = mineInfo;

		
		}
	
	
	}

	public List<int> FilterByDetectRadius(List<int> indexes, Vector3 pos){
		
		List<int> res = new List<int> ();

		foreach(int index in indexes){
			MineInfo info = GetMineInfoByIndex (index);
			if (info == null) {
				Debug.LogError ("mine info null");
				continue;
			}
			Vector3 offset = new Vector3(info.pos.x, info.pos.y) - pos;
			if (offset.magnitude < m_detectRadius) {
			
				res.Add (index);
			}

		
		}
		return res;
	
	}

	public MineInfo GetMineInfoById(int mineId){
		if (!m_mineById.ContainsKey (mineId)) {
			Debug.LogError ("no such mineid:" + mineId);
			return null;		
		}

		return m_mineById [mineId];
	
	}

	public MineInfo GetMineInfoByIndex(int index){
		if (!ContainIndex (index)) {
			Debug.LogError ("index exceeds:" + index);
			return null;
		
		}
		return m_mineByIndex [index];

	}

	public bool ContainIndex(int index){
		if (index < 0 || index >= m_mineByIndex.Count) {
			return false;
		}
		return true;
	}

	
}