﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg.login;
using pbmsg.room;

public class RemeberLoseManager : MonoBehaviour {

	public MoveSync m_move;
	private const int REM_OFFSET_SEQ = 10;

	public enum LOSE_TYPE{
		LOSE_TYPE_INVALID,
		LOSE_TYPE_UNIT,
		LOSE_TYPE_MINE	
	}

	public class RemeberLoseItem{
		public LOSE_TYPE type;

		public int uid;
		public int unitId;
		public int mineId;
		public int remFrameSeq;
		public RemeberLoseItem(int uid, int unitId, int seq){		
			this.uid = uid;
			this.unitId = unitId;
			this.remFrameSeq = seq;		
			type = LOSE_TYPE.LOSE_TYPE_UNIT;
		}

		public RemeberLoseItem(int mineId, int seq){
			this.mineId = mineId;
			this.remFrameSeq = seq;
			type = LOSE_TYPE.LOSE_TYPE_MINE;
		}

	}

	public List<RemeberLoseItem> m_remLose;

	public void LoseUnit(int uid, int unitId){

		m_remLose.Add(new RemeberLoseItem(uid, unitId, m_move.m_frameSeq + REM_OFFSET_SEQ));

	}

	public void LoseMine(int mineId){
	
		m_remLose.Add (new RemeberLoseItem (mineId, m_move.m_frameSeq + REM_OFFSET_SEQ));
	
	}

	public bool isUnitInRememberLose(int uid, int unitId){
	
		foreach(var unit in m_remLose){
			if(unit.type == LOSE_TYPE.LOSE_TYPE_UNIT && unit.uid == uid && unit.unitId == unitId){
				return true;
			}		
		}
		return false;	
	}

	public bool isMineInRememberLose(int mineId){

		foreach (var item in m_remLose) {
			if (item.type == LOSE_TYPE.LOSE_TYPE_MINE && item.mineId == mineId) {
				return true;			
			}
		
		}
		return false;
	}


	public bool isPlayerInRemeberLose(CSBasicMovement move){

		int uid = move.uid;
		foreach(var unit in move.units){
			int unitId = unit.id;
			if(!isUnitInRememberLose(uid, unitId)){
				return false;			
			}	
		}	

		return true;
	}

	void Update(){
		List<RemeberLoseItem> rms = new List<RemeberLoseItem>();

		foreach(var unit in m_remLose){
		
			if(unit.remFrameSeq < m_move.m_frameSeq){

				rms.Add(unit);			
			}		
		}

		foreach(var rm in rms){
		
			m_remLose.Remove(rm);
		
		}
	
	
	}


	void Awake(){

		m_remLose = new List<RemeberLoseItem>();
	
	}
}
