﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class MineByter : MonoBehaviour {

	public static List<int> GetAliveMineIndex(List<uint> bytes){

		int index = 0;
		List<int> res = new List<int>();

		for (int i = 0; i < bytes.Count; i++) {

			uint bits = bytes [i];

			for (int j = 0; j < 8; j++, index++) {
				
				uint testByte = 1u << j;
				if ((bits & testByte) != 0) {
					res.Add (index);					
				}			
			}
		}
		return res;
	}

	public static void RemoveMineByte(List<uint> bytes, int index){
		int bigIndex = (int)Mathf.Floor(index / 8);
		int smallIndex = index % 8;

		if (bigIndex < 0 || bigIndex >= bytes.Count
		   || smallIndex < 0 || smallIndex >= 8) {
			Debug.LogError ("index failed, bigIndex:" + bigIndex + " smallIndex:" + smallIndex);
			return;
		}

		uint oneByte = 1u << smallIndex;
		uint flipByte = ~oneByte;
		bytes [bigIndex] = bytes [bigIndex] & flipByte;

	
	}


}
