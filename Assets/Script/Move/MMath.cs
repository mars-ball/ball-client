﻿using UnityEngine;
using System.Collections;

public class MMath  {

	public static Vector2 accPos(Vector2 initPos, Vector2 speed, Vector2 accSpeed, float timeS){
		Vector2 pos = initPos + speed * timeS + 0.5f * accSpeed * timeS * timeS;
		return pos;
	
	}

	public static Vector2 accV(Vector2 initSpeed, Vector2 acc, float timeS){
	
		Vector2 newSpeed = initSpeed + acc * timeS;
		return newSpeed;
	
	}

}
