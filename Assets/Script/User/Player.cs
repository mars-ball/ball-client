﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Fighter{

	public class ResData{
		public string m_headPic;
		public string m_bodyPic;
		public Dictionary<int, string> m_skillPics;
	}

	public int m_id;
	public int m_listPos;
	public string m_name;
	public ResData m_res;

	public Vector2 m_mapPos;

	private Image m_currSkill;
	private Transform m_arrow;


	public Fighter(int pos, FighterParam par){
		m_listPos = pos;
		m_id = par.m_id;
		m_name = par.m_name;	
		m_mapPos = par.m_mapPos;
	}

}

public class FighterParam{
	public int m_id;
	public string m_name;
	public Vector2 m_mapPos;

}

public class PlayerFighterData{

	List<Fighter> m_battleFighters;

	public Fighter GetFighterByPos(int pos){
		if(pos < 0 || pos >= m_battleFighters.Count){
			Debug.Log("failed to get fighter");
			return null;
		}

		return m_battleFighters [pos];
	}

	public bool AddBattleFighter(FighterParam par){
		int idx = m_battleFighters.Count;

		Fighter fighter = new Fighter (idx, par);
		m_battleFighters.Add (fighter);
		return true;
	}

}



public class Player{

	public int m_userId;

	PlayerFighterData m_fighterData;

	public Player(int userId){
		m_userId = userId;
	}

	public PlayerFighterData GetPlayerFighterData(){
		return m_fighterData;	
	}



}
























