﻿using UnityEngine;
using System.Collections;
using localSave;

public class UserInfo{

	public const int WAIT_FOR_AD_CNT = 5;

	public enum AppStatus{
		INVALID,
		START_UP,
		LOGIN_IN,
		IN_BATTLE,
		WAIT_FOR_NEXT_ROUND,
		WAIT_FOR_WHOLE_NEW_GAME,
		FOLLOW_FRIEND_BATTLE,
		OFFLINE,	
	}

	public AppStatus mAppStatus;
	public bool mHasNotif;
	public int mWaitForAd = WAIT_FOR_AD_CNT;

	public UserBasicData mBasicInfo;
	public UserLifeInfo mLifeInfo;
	public UserGamingInfo mGamingInfo;

	private void Init(){
		mLifeInfo = new UserLifeInfo ();
		mGamingInfo = new UserGamingInfo ();
	}

	public bool isUserFirstInGame(){
		if(mAppStatus == AppStatus.START_UP){
			return true;
		}
		return false;
	
	}


	public UserInfo(UserBasicData basicInfo){
		Init ();
		mBasicInfo = basicInfo;	
		mAppStatus = AppStatus.START_UP;
	}

	public UserInfo(int uId){
		Init ();
		mBasicInfo = new UserBasicData();
		mBasicInfo.uid = uId;		
	}

	public UserGamingInfo GetUserGamingInfo(){
		return mGamingInfo;
	}

	public UserLifeInfo GetUserLifeInfo(){
		return mLifeInfo;	
	}

	public UserBasicData GetUserBasicInfo(){
		return mBasicInfo;	
	}


}
