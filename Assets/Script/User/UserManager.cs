using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using localSave;

public class UserManager{

	private static UserManager mInstance;
	private Dictionary<int, UserInfo> mUserDict;
	private UserInfo mMainUser;

	private UserManager(){
		mUserDict = new Dictionary<int, UserInfo> ();	
	}

	static UserManager(){
		mInstance = new UserManager ();
	}

	public static UserManager GetInstance(){	
		return mInstance;
	}

	public void RemoveUser(int uid){
		if (!mUserDict.ContainsKey (uid)) {
			Debug.Log ("failed to remove user " + uid);
			return;
		}
		mUserDict.Remove (uid);	
	}


	public void ResetUser(){
	}

	public UserInfo AddUserSimple(int uId){

		UserInfo newUserInfo = new UserInfo (uId);

		if (mUserDict.ContainsKey (uId)) {
			mUserDict.Remove(uId);
			mUserDict.Add (uId, newUserInfo);
		} else {
			mUserDict.Add (uId, newUserInfo);
		}

		if(mUserDict.Count <=1){
			mMainUser = newUserInfo;
		}

		return mUserDict[uId];

	}

	public void AddUser(UserBasicData userBasic){

		int uId = userBasic.uid;

		UserInfo newUser = new UserInfo (userBasic);

		if (mUserDict.ContainsKey (uId)) {
			mUserDict.Remove (uId);
		}

		mUserDict.Add (uId, newUser);	

		if(mUserDict.Count <=1){
			mMainUser = newUser;
		}
	}

	public UserInfo GetMainUser(){
		return mMainUser;
	}

	public UserInfo GetUserById(int uid = 0){
		if(mUserDict.ContainsKey(uid)){
			return mUserDict[uid];
		}else{
			return null;
		}	
	}

	public UserBasicData GetUseBasicInfoById(int uid = 0){
		UserInfo info = GetUserById (uid);
		if (info == null) {
			Debug.Log ("error get user by id " + uid);
			return null;
		}
		return info.GetUserBasicInfo ();
	}

	public UserGamingInfo GetUserGamingInfoById(int uid = 0){
	
		UserInfo info = GetUserById (uid);
		if (info == null) {
			Debug.Log ("error get user by id " + uid);
			return null;
		}
		return info.GetUserGamingInfo();
	}


	public UserLifeInfo GetUserLifeInfoById(int uId = 0){
	
		UserInfo info = GetUserById (uId);
		if (info == null) {
			Debug.Log ("error get user by id " + uId);
			return null;
		}
		return info.GetUserLifeInfo();
	
	}


}




















