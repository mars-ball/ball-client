﻿using UnityEngine;
using System.Collections;

public class UserGamingInfo{
	public uint mPreLv;
	public uint mLv;
	public int mScore;
	public int mRoomTypeId;
	public int mRoomId;
	public int mCampId;

	public UserGamingInfo(){
		mLv = 0;
		mPreLv = 0;
		mScore = 0;
	}

}
