﻿using UnityEngine;
using System.Collections;
using System;

public class UserBasicInfo{
	public string mUserName;
	public int mUserId;
	public Int64 mUUid;
	public string mToken;
	public DateTime mLastLogin;
	public bool mIsLogin;
	public uint mServerId;
	public uint mServerName;
	public uint mWorldId;
	public uint mBigAreaId;
	public uint mSmallAreaId;
	public uint mRoomId;
}
