﻿using UnityEngine;
using System.Collections;
public class JoyStick : MonoBehaviour {
	private Vector3 m_origin;

	private Transform m_mainTrans;
	private Transform m_joyTrans;
	private Vector3 m_DeltaPos;
	private bool m_Drag;

	private Vector3 m_mousePrev;

	public bool m_isMouseDrag = false;

	private Vector3 m_deltaPosition;

	[SerializeField]
	private float m_moveMaxDistance;

	[HideInInspector]
	public Vector3 m_fixedMovePosiNorm;

	[HideInInspector]
	public Vector3 m_moveNorm;

	[SerializeField]
	private float m_activeMoveDistance = 1;

	void Awake(){
		EventTriggerListener.Get (gameObject).onDrag = OnDrag;
		EventTriggerListener.Get (gameObject).onDragOut = OnDragOut;
		EventTriggerListener.Get (gameObject).onDown = OnMoveStart;

		//EventTriggerListener.Get (gameObject).onUp = OnMouseDown;
		//EventTriggerListener.Get (gameObject).onDown = OnMouseUp;

		m_joyTrans = transform;
	}

	void Start(){
		m_origin = transform.localPosition;
	}

	void Update(){

		//if(m_isMouseDrag){
			//Vector3 currPos = Input.mousePosition;
			//Vector3 delta = currPos - m_mousePrev;
			//m_joyTrans.localPosition += new Vector3(delta.x, delta.y, 0);

			float dis = Vector3.Distance(m_joyTrans.localPosition, m_origin);
			if(dis >= m_moveMaxDistance){
				Vector3 vec = m_origin + (m_joyTrans.localPosition - m_origin)*m_moveMaxDistance/dis;
				m_joyTrans.localPosition = vec;		
			}
		
		//}
	}
	/*
	void OnMouseDown(GameObject obj){
		m_isMouseDrag = true;
		m_mousePrev = Input.mousePosition;
	}

	void OnMouseUp(GameObject obj){
		m_isMouseDrag = false;
		m_joyTrans.localPosition = m_origin;
	}*/


	void TouchDown(){
	
		if(!(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)){
		}else{		
			m_joyTrans.localPosition = m_origin;
			m_isMouseDrag = true;
		}
	
	}

	public void OnDrag(GameObject go, Vector2 delta){
		m_joyTrans.localPosition += new Vector3(delta.x, delta.y, 0);
	}

	public void OnDragOut(GameObject go, Vector2 delta){
		m_joyTrans.localPosition = m_origin;
		m_isMouseDrag = false;
	}

	public void OnMoveStart(GameObject go){
	}


	public int GetDirDis(out Vector3 dir, out float dis ){
		dis = Vector3.Distance(m_joyTrans.localPosition, m_origin)/m_moveMaxDistance;
		if(dis > 1f){
			dis = 1f;
		}

		if(dis < 0.1){

			dir = new Vector3(0, 0, 0);
		
		}else{
			dir = (m_joyTrans.localPosition - m_origin).normalized;
		}
		return 0;
	}

}












