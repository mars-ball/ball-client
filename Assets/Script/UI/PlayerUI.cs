﻿using UnityEngine;
using System.Collections;
using Unit=MoveSync.Player.Unit;


public class PlayerUI : MonoBehaviour {

	public JoyController m_joy;

	private static readonly float ARROW_PLAYER_GAP = 0.1f;

	private const float R = 1.59f;

	public void UpdateArrow(Unit unit){
	
		Vector3 dir;
		float dis;
		int ret = m_joy.GetDirDis(out dir, out dis);
		//float r = (unit.GetScale()/MoveSync.UNIT_INIT_SCALE) + ARROW_PLAYER_GAP;
		float r = unit.m_arrow.transform.localScale.x;
		Transform arrowTrans = unit.m_arrow;
		unit.m_arrow.gameObject.SetActive(true);
		arrowTrans.localPosition = dir.normalized * R;

		float angle = Vector2.Angle(new Vector2(0, 1), dir);

		if(dir.x > 0){
			angle = -angle;
		}

		arrowTrans.eulerAngles = new Vector3(0, 0, angle);
	
	}





}
