﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoginUI : MonoBehaviour {

	public Text m_userText;

	public void OnClick(){
		SceneManager.LoadScene("FriendSearch");
	}

	public void OnClickNotif(){
		SceneManager.LoadScene("Notification");
	}

	public void OnClickFriendList(){
		SceneManager.LoadScene("FriendList");
	}

	// Use this for initialization
	void Start () {
		m_userText.text = UserManager.GetInstance().GetMainUser().mBasicInfo.uid.ToString();
	}

}
