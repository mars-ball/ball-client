﻿using UnityEngine;
using System.Collections;

public static class SpritePivotAlignment {
	public static SpriteAlignment GetSpriteAlignment(GameObject SpriteObject){
		BoxCollider2D MyBoxCollider= SpriteObject.AddComponent<BoxCollider2D> ();
		float colX = MyBoxCollider.offset.x;
		float colY = MyBoxCollider.offset.y;
		if (colX > 0f && colY < 0f)
			return (SpriteAlignment.TopLeft);
		else if (colX < 0 && colY < 0)
			return (SpriteAlignment.TopRight);
		else if (colX == 0 && colY < 0)
			return (SpriteAlignment.TopCenter);
		else if (colX > 0 && colY == 0)
			return (SpriteAlignment.LeftCenter);
		else if (colX < 0 && colY == 0)
			return (SpriteAlignment.RightCenter);
		else if (colX > 0 && colY > 0)
			return (SpriteAlignment.BottomLeft);
		else if (colX < 0 && colY > 0)
			return (SpriteAlignment.BottomRight);
		else if (colX == 0 && colY > 0)
			return (SpriteAlignment.BottomCenter);
		else if (colX == 0 && colY == 0)
			return (SpriteAlignment.Center);
		else
			return (SpriteAlignment.Custom);
	}
}


[RequireComponent (typeof (SpriteRenderer))]
public class RepeatSpriteBoundary : MonoBehaviour {
	public SpriteRenderer sprite;
	void Awake () {

		sprite = GetComponent<SpriteRenderer>();
		if(!SpritePivotAlignment.GetSpriteAlignment(gameObject).Equals(SpriteAlignment.TopRight)){
			Debug.LogError("You forgot change the sprite pivot to Top Right.");
		}
		Vector2 spriteSize = new Vector2(sprite.bounds.size.x / transform.localScale.x, sprite.bounds.size.y / transform.localScale.y);

		GameObject childPrefab = new GameObject();

		SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
		childPrefab.transform.position = transform.position;
		childSprite.sprite = sprite.sprite;

		GameObject child;
		for (int i = 0, h = (int)Mathf.Round(sprite.bounds.size.y); i*spriteSize.y < h; i++) {
			for (int j = 0, w = (int)Mathf.Round(sprite.bounds.size.x); j*spriteSize.x < w; j++) {
				child = Instantiate(childPrefab) as GameObject;
				child.transform.position = transform.position - (new Vector3(spriteSize.x*j, spriteSize.y*i, 0));
				child.transform.parent = transform;
			}
		}

		Destroy(childPrefab);
		sprite.enabled = false;

	}
}




