﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using tnt_deploy;

public class FriendScrollCell : IScrollCell {

	public enum Type {
	
		INVALID,
		LIST,
		SEARCH,
		MAX	
	}

	public int index;	
	public int uid;
	public string uname;
	public int photoId;
	public UserSrcType srcType;
	public int lv;
	public int exp;

	public Button eButton;
	public Text eButtonText;

	public Text eNameText;
	public Image ePhoto;
	public Image eSrcPhoto;
	public Type eType;


	public void show(){
	}
	public IDataListCanGetNum m_dataSource;


	public override void Show(int typeIndex, int _index){
		CSFriendInfo friend = m_dataSource.GetDataByIndex(typeIndex, index) as CSFriendInfo;
		if(friend == null){
			Debug.Log("notif is null");
			return;		
		}
		index = _index;
		uid = friend.uid;
		uname = friend.name;
		lv = friend.lv;
		exp = friend.exp;
		srcType = friend.srcType;
		photoId = friend.photo;

		eNameText.text = uname;

		Texture2D photoTex = PhotoGen.GetInstance().getPhotoTexture(photoId);

		Sprite photoSp = Sprite.Create(photoTex, new Rect(0, 0, photoTex.width, photoTex.height), new Vector2(0.5f, 0.5f));

		ePhoto.sprite = photoSp;

	}

	public override void SetDataSource(IDataListCanGetNum dataSource){
		m_dataSource = dataSource;
	}

	void Awake(){	

	}

	public void OnClickDecline(){
	}

	public void OnClickBattle(){

		new CSFollowInstantBattleMessage(uid).Send(
			delegate(IExtensible msg){
				HMessage message = (HMessage)msg;

				CSFollowFriendInstantBattleResponse response = message.response.followFriendInstantBattle;
				if(response == null){
					Debug.LogError("response null , recv id :"+ (int)message.msgType);
					return false;			
				}

				int roomId = response.roomId;
				int campId = response.campid;
				UserManager.GetInstance ().GetMainUser ().mGamingInfo.mRoomId = roomId;
				UserManager.GetInstance ().GetMainUser ().mGamingInfo.mCampId = campId;
				UserManager.GetInstance ().GetMainUser ().mAppStatus = UserInfo.AppStatus.FOLLOW_FRIEND_BATTLE;
				SceneManager.LoadScene("Login");
				//eNameText.text = roomId + " , " + campId;

				return true;
			}
		);

	}




	public void OnClickAdd(){

		new CSAddFriendMessage(uid).Send(
			delegate(IExtensible msg){
				HMessage message = (HMessage)msg;

				CSAddFriendResponse response = message.response.addFriend;
				if(response == null){
					Debug.LogError("response null , recv id :"+ (int)message.msgType);
					return false;			
				}

				if(response.ret == Ret.Success){
					eButtonText.text = "Sent";
				}

				return true;
			}
		);
	}
	/*
	public void OnClickAccept(){
		Debug.LogWarning("accept uid:" + uid);
		new CSHandleNotifMessage(index, pbmsg.login.HandleNotifType.HANDLE_NOTIF_TYPE_ACCEPT).Send(
			delegate(IExtensible msg){
				HMessage message = (HMessage)msg;

				CSHandleNotifResponse response = message.response.handleNotif;
				if(response == null){
					Debug.LogError("response null , recv id :"+ (int)message.msgType);
					return false;			
				}
				if(response.ret == Ret.Success){
					nameText.text = "success";
				}

				return true;
			}
		);


	}
*/	
}
