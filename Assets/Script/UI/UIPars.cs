﻿using UnityEngine;
using System.Collections;

public class UIPars{

	public static readonly float m_screenAspect = 9/(16*(1-0.2f));
	public static readonly float m_topPercent = 0.1f;
	public static readonly float m_bodyPercent = 0.7f;
	public static readonly float m_bottomPercent = 0.2f;

}
