﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIWarningWindow : MonoBehaviour {

	public delegate void OnButtonClick();

	public OnButtonClick m_onLeftClick;
	public OnButtonClick m_onRightClick;

	public Text eMsgText;
	public Button eLeftBtn;
	public Button eRightBtn;

	public Text eLeftBtnText;
	public Text eRightBtnText;


	public void Show(string msg, OnButtonClick leftCallback, OnButtonClick rightCallback, string leftText = "", string rightText = ""){
	
		m_onLeftClick = leftCallback;
		m_onRightClick = rightCallback;

		eMsgText.text = msg;

		if (leftText.Length > 0) {
			eLeftBtnText.text = leftText;
		}
		if (rightText.Length > 0) {
			eRightBtnText.text = rightText;
		}

		this.gameObject.SetActive (true);
	
	}

	public void OnLeftButtonCallback(){
		if (m_onLeftClick != null) {
			m_onLeftClick ();
		}
		this.gameObject.SetActive (false);


	}


	public void OnRightButtonCallback(){
		if (m_onRightClick != null) {
			m_onRightClick ();
		}
		this.gameObject.SetActive (false);
	
	}


}
