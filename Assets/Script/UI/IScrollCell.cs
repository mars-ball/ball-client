﻿using UnityEngine;
public abstract class IScrollCell:MonoBehaviour {
	public abstract void Show(int typeIndex, int index);
	public abstract void SetDataSource(IDataListCanGetNum dataSource);
}
