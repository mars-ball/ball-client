﻿using UnityEngine;
public abstract class ITabPage:MonoBehaviour{

	public virtual void RefreshAfterSearch(int typeIndex){}
	public abstract void Refresh(int typeIndex);
	public abstract void SetDataSource(IDataListCanGetNum dataSource);
	public virtual void RemoveItem(int index){}
}
