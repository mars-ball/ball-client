﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using tnt_deploy;
using System.Text;
using ItemType=StoreDataManager.ITEM_TYPE;
using PriceType=StoreDataManager.PRICE_TYPE;
using SimpleJSON;

public class StoreScrollCell : MonoBehaviour {

	public int m_index;
	public int m_id;	
	public string m_desc;
	public string m_fileName;
	public ItemType m_itemType;
	public PriceType m_priceType;
	public int m_price;
	public string m_localPriceTxt;

	public int m_addGold;
	public int m_photoId;

	public Image eItemPhoto;
	public Image eCoinImg;
	public Text eDescText;
	public Text ePriceText;
	public Button ePriceButton;

	public StoreScrollManager m_scrollMgr;


	public void show(STORE item, int index){

		StartCoroutine (showCoroutine (item, index));

	}

	public IEnumerator showCoroutine(STORE item, int index){

		if (item.type == (int)PriceType.PRICE_TYPE_CASH && m_scrollMgr.m_IAP.m_Controller != null && m_scrollMgr.m_IAP.m_Controller.products != null) {
			yield return null;
		}

		m_index = index;

		m_id = item.id;

		m_price = item.price;

		m_addGold = item.add_gold_num;
		m_photoId = item.photo_id;

		m_itemType = (ItemType)item.type;
		m_priceType = (PriceType)item.price_type;

		UnityEngine.Purchasing.Product productInfo = null;
		if (m_itemType == ItemType.ITEM_TYPE_GOLD) {
			productInfo = m_scrollMgr.m_IAP.m_Controller.products.all [index];
			Debug.Log ("productinfo :" + productInfo);
		}

		if (m_itemType == ItemType.ITEM_TYPE_GOLD) {
			m_desc = productInfo.metadata.localizedDescription;  //Encoding.Default.GetString (item.msg);
			Debug.Log ("metadata :" + productInfo.metadata);
		}


		string fileName = "";
		Sprite itemSprite = null;

		if(m_itemType == ItemType.ITEM_TYPE_GOLD){

			fileName = "Common/Image/gold";
			itemSprite = Resources.Load<Sprite>(fileName);
			if (itemSprite == null) {
				Debug.LogError ("load sprite gold failed");
				yield break;
			}

		}else if(m_itemType == ItemType.ITEM_TYPE_PHOTO){
		
			/*
			int photoId = item.photo_id;

			PHOTO photo = PhotoDataManager.GetInstance().GetDataById(photoId);

			fileName = "Photos/" + Encoding.Default.GetString(photo.photo_name);		*/
			Texture2D itemPhoto =PhotoGen.GetInstance().getPhotoTexture(m_photoId);    //Resources.Load<Sprite>(fileName);
			if(itemPhoto == null){
				Debug.LogError("load photo is failed:" + fileName);
				yield break;
			}

			itemSprite = Sprite.Create(itemPhoto, new Rect(0, 0, itemPhoto.width, itemPhoto.height), new Vector2(0.5f, 0.5f));
			if (itemSprite == null) {
				Debug.LogError ("load sprite id." + m_photoId + " failed");
				yield break;
			}

		}




		eItemPhoto.sprite = itemSprite;
		eDescText.text =m_desc;

		int usePhoto = UserManager.GetInstance().GetMainUser().mBasicInfo.photo;

		if(usePhoto == m_photoId){
			ePriceText.text = "In Use";		
			m_scrollMgr.m_prevIdx = m_index;
		}else if(m_scrollMgr.m_shop.m_userPack.Contains(m_id) && m_itemType == ItemType.ITEM_TYPE_PHOTO){
			ePriceText.text = "Choose";
			ePriceButton.onClick.AddListener(OnChoose);
		}else{
			ePriceButton.onClick.AddListener(OnBuy);
			if(m_priceType == PriceType.PRICE_TYPE_CASH){
				Debug.Log ("local str :" + productInfo.metadata.localizedPriceString);
				ePriceText.text = productInfo.metadata.localizedPriceString;   //"¥" + m_price.ToString();
			}else{
				ePriceText.text = "  " + m_price.ToString();
				eCoinImg.gameObject.SetActive(true);
			}
		}
	}




	public void OnBuy(){

		if (m_priceType == PriceType.PRICE_TYPE_CASH) {

			//TODO: Add a loading scene to wait unit system finish IAP's initialization.

			int index = 0;

			foreach (var itemPair in StoreDataManager.GetInstance().m_allData) {
				if (m_id == itemPair.Key) {
					break;
				}
				index++;
			}

			m_scrollMgr.m_IAP.m_onBuyVerify = HandleOnBuyVerify;
			m_scrollMgr.m_IAP.m_onBuyComplete = HandleOnBuyComplete;
			m_scrollMgr.m_IAP.Buy (m_id, index);
		} 
		// Buy avatar
		else if (m_priceType == PriceType.PRICE_TYPE_GOLD) {

			GameObject canvas = GameObject.Find ("StoreUI");

			Popup.show_popup_confirm (canvas, delegate () {
				Popup.closePopup ();
			}, delegate () {
				Popup.closePopup ();

				int gold = UserManager.GetInstance ().GetMainUser ().mBasicInfo.gold;
				if (gold < m_price) {
					Debug.LogError ("no enough gold");
					return;
				}

				PLATFORM_TYPE platform = PLATFORM_TYPE.PLATFORM_TYPE_INVALID;

				Debug.LogError ("item id:" + m_id + " platform:" + platform + " this:" + this);

				HandleOnBuyVerify (m_id, "receipt", platform);
			}
			);
				
			Popup.changeTitle (LanguageText.getStringByID ("STRINGS_SHOP_POPUP_TITLE"));
			Popup.changeContent (LanguageText.getStringByID ("STRINGS_SHOP_POPUP_CONTENT"));
			Popup.changeLeftButtonLabel (LanguageText.getStringByID ("STRINGS_CANCEL"));
			Popup.changeRightButtonLabel (LanguageText.getStringByID ("STRINGS_OK"));
		}
	}

	public void SetPriceText(string str){
		ePriceText.text = str;
		if(str == "Choose"){
			ePriceButton.onClick.RemoveAllListeners();
			ePriceButton.onClick.AddListener(OnChoose);
		}
	}

	public void OnChoose(){
		Debug.LogError("choose:" + m_id);	
		if(m_itemType == ItemType.ITEM_TYPE_PHOTO){

			CSChangeInfoMessage.ChangeParam param = new CSChangeInfoMessage.ChangeParam();
			param.photoId = m_photoId;
			CSChangeInfoMessage.ChangeInfoType type = CSChangeInfoMessage.ChangeInfoType.CHANGE_INFO_TYPE_PHOTO;

			new CSChangeInfoMessage(type, param).Send(
				delegate(IExtensible message){
					HMessage msg = (HMessage)message;
					if(msg.msgType != MSGID.CSChangeInfo_Response){
						Debug.LogError ("msg id error " + msg.msgType);
						return false;
					}

					CSChangeInfoResponse rsp = msg.response.changeInfo;
					if(rsp == null){
						Debug.LogError ("create user rsp null ");
						return false;			
					}
					if(rsp.ret == Ret.Success){
						ePriceText.text = "In Use";
						ePriceButton.onClick.RemoveAllListeners();
						m_scrollMgr.SetItemPriceTextByIndex(m_scrollMgr.m_prevIdx, "Choose");
						m_scrollMgr.m_prevIdx = m_index;
						UserManager.GetInstance().GetMainUser().mBasicInfo.photo = m_photoId;
					}else{
						Debug.LogError("not in user");					
					}

					NetworkManager.GetInstance().m_battleServer.DisConnect();

					return true;
				}
			);
		}

	}

	void HandleOnBuyComplete(bool res){

		if(!res){
			Debug.LogError("buy failed");
			return;
		}
		Debug.LogError (" complete , res:" + res);

		if(m_priceType == PriceType.PRICE_TYPE_GOLD){
			UserManager.GetInstance().GetMainUser().mBasicInfo.gold -= m_price;	
			m_scrollMgr.m_shop.m_CoinText.text = UserManager.GetInstance ().GetMainUser ().mBasicInfo.gold.ToString ();
		}

		if(m_itemType == ItemType.ITEM_TYPE_GOLD){
			UserManager.GetInstance().GetMainUser().mBasicInfo.gold += m_addGold;	
			m_scrollMgr.m_shop.m_CoinText.text = UserManager.GetInstance().GetMainUser().mBasicInfo.gold.ToString();
			
		}else if(m_itemType == ItemType.ITEM_TYPE_PHOTO){
			eCoinImg.gameObject.SetActive(false);
			ePriceText.text = "Choose";	
			ePriceButton.onClick.RemoveAllListeners();
			ePriceButton.onClick.AddListener(OnChoose);
		}	
	}




	bool HandleOnBuyVerify (int itemId, string receipt, PLATFORM_TYPE platform)
	{

		Debug.LogError ("item id:" + itemId + " platform:" + receipt + " platform:" + platform);

		CSBasicBuyMessage.BuyParam param = new CSBasicBuyMessage.BuyParam ();
		param.m_itemId = itemId;
		param.m_platform = platform;
		param.m_receipt = receipt;

		if (platform == PLATFORM_TYPE.PLATFORM_TYPE_ANDROID) {
			var receiptJson = JSON.Parse (receipt);

			Debug.LogError ("receipt json:" + receiptJson);

			var payLoadString = receiptJson ["Payload"].Value;

			var payLoadJson = JSON.Parse (payLoadString);

			var payLoad = payLoadJson ["json"].Value;

			Debug.LogError ("payload:" + payLoad);

			var sig = payLoadJson ["signature"].Value;

			Debug.LogError ("sig:" + sig);

			param.m_payLoad = payLoad;

			param.m_signature = sig;
		}

		new CSBasicBuyMessage(param).Send(
			delegate(IExtensible message){
				Debug.LogError("buy complete");
				HMessage msg = (HMessage)message;
				if(msg.msgType != MSGID.CSBasicBuy_Response){
					Debug.LogError ("msg id error " + msg.msgType);
					return false;
				}

				CSBasicBuyResponse rsp = msg.response.basicBuy;
				if(rsp == null){
					Debug.LogError ("create user rsp null ");
					return false;			
				}

				if(rsp.ret == Ret.Success){
					HandleOnBuyComplete(true);
				}else{
					HandleOnBuyComplete(false);
				}

				return true;

			}
		);
		return true;
	}


}
