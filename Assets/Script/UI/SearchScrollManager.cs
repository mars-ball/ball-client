﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SearchScrollManager : ITabPage {

	public int m_typeIndex;

	public RectTransform eContent;
	public GridLayoutGroup eContentGrid;

	public enum SCROLL_DIRECTION{
		SCROLL_DIRECTION_HOZ,
		SCROLL_DIRECTION_VERT	
	}

	public SCROLL_DIRECTION m_scrollDirection;
	public readonly static float HOZ_ITEM_HV_ASPECT = 0.6f;
	public readonly static float HOZ_SPACING_ASPECT = 0.05f;

	public readonly static float VERT_ITEM_VH_ASPECT = 0.15f;
	public readonly static float VERT_SPACING_ASPECT = 0.1f;

	private IDataListCanGetNum m_dataSource;
	public GameObject eCell;
	private List<GameObject> m_cells; 

	public InputField eSearchText;

	public override void SetDataSource(IDataListCanGetNum dataSource){
		m_dataSource = dataSource;	
	}

	public void OnClickSearch(){
		if(eSearchText.text.Length > 0){
			m_dataSource.Search(m_typeIndex, eSearchText.text);
		}
	
	}


	public override void RefreshAfterSearch(int typeIndex){
	
		if(m_cells.Count > 0){

			foreach(var cell in m_cells){
				GameObject.Destroy(cell.gameObject);			
			}
			m_cells.Clear();
		}

		int num = m_dataSource.GetDataListNum(typeIndex);

		if(m_scrollDirection == SCROLL_DIRECTION.SCROLL_DIRECTION_HOZ){
			float itemHeight = eContent.rect.height;
			float itemWidth = itemHeight * HOZ_ITEM_HV_ASPECT;
			float itemSpacing = itemWidth * HOZ_SPACING_ASPECT;

			eContentGrid.cellSize = new Vector2(itemWidth, itemHeight);
			eContentGrid.spacing = new Vector2(itemSpacing, 0);

			float contentWidth = itemWidth * num;

			eContent.sizeDelta = new Vector2(contentWidth, 0);
		}else{
			float itemWidth = eContent.rect.width;
			float itemHeight = itemWidth * VERT_ITEM_VH_ASPECT;
			float itemSpacing = itemHeight * VERT_SPACING_ASPECT;

			eContentGrid.cellSize = new Vector2(itemWidth, itemHeight);
			eContentGrid.spacing = new Vector2(itemSpacing, 0);

			float contentHeight = itemHeight * num;

			eContent.sizeDelta = new Vector2(0, contentHeight);
		}



		for(int ii = 0; ii < num; ii++){

			AddItem(typeIndex, ii);

		}
	
	}

	public override void Refresh(int index){
		m_typeIndex = index;

		if(m_cells.Count > 0){

			foreach(var cell in m_cells){
				GameObject.Destroy(cell.gameObject);			
			}
			m_cells.Clear();
		}


	}

	public void AddItem(int typeIndex, int index){

		GameObject obj = GameObject.Instantiate(eCell, eCell.transform.position, eCell.transform.rotation) as GameObject;
		obj.SetActive(true);

		IScrollCell cell = obj.GetComponent<IScrollCell>();
		cell.SetDataSource(m_dataSource);

		cell.Show(typeIndex, index);

		obj.transform.parent = eContent;
		obj.transform.localScale = Vector3.one;

		m_cells.Add(obj);

	}

	private void Awake(){

		m_cells = new List<GameObject>();


	}




}
