﻿using UnityEngine;
public abstract class IDataListCanGetNum:MonoBehaviour{

	public bool m_isDataReady;

	public abstract int GetDataListNum(int typeIndex);

	public abstract void Search(int typeIndex, string str);

	public abstract System.Object GetDataByIndex(int typeIndex, int index);

	public virtual void RemoveItem(int typeIndex, int index){}
}
