﻿using UnityEngine;
using System.Collections;

public interface UIScrollInterface{

	void OnClickAdd();

	void OnClickAccept();

	void OnClickDecline();

	void OnClickBattle();


}
