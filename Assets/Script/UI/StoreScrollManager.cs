﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using tnt_deploy;

using ItemType=StoreDataManager.ITEM_TYPE;
using PriceType=StoreDataManager.PRICE_TYPE;

using ProtoBuf;
using pbmsg.login;

public class StoreScrollManager : MonoBehaviour {
	
	public Transform eContent;

	public GameObject eStoreCell;


	public IAP m_IAP;

	private List<StoreScrollCell> m_cells; 

	public ShopScene m_shop;

	public int m_prevIdx;

	public void MakeList(ItemType type){

		m_prevIdx = -1;

		List<STORE> list = StoreDataManager.GetInstance().m_itemsByType[type];

		if(m_cells.Count > 0){
		
			foreach(var cell in m_cells){
				GameObject.Destroy(cell.gameObject);			
			}
			m_cells.Clear();
		}

		foreach(var item in list){
		
			AddItem(item);
		
		}
	}

	public void SetItemPriceTextByIndex(int index, string str){
		if(index < 0 || index >= m_cells.Count){
			return;
		}
		m_cells[index].SetPriceText(str);	
	}

	public void AddItem(STORE store){

		GameObject obj = GameObject.Instantiate(eStoreCell, eStoreCell.transform.position, eStoreCell.transform.rotation) as GameObject;
		obj.SetActive(true);

		StoreScrollCell cell = obj.GetComponent<StoreScrollCell>();

		cell.show(store, m_cells.Count);

		obj.transform.parent = eContent;
		obj.transform.localScale = Vector3.one;

		m_cells.Add(cell);

	}

	private void Awake(){
	
		m_cells = new List<StoreScrollCell>();
		m_IAP = IAP.getInstance ();
	
	}


}














