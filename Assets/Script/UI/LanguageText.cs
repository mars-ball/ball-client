﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using localSave;
using tnt_deploy;

public class LanguageText : MonoBehaviour {

	public string eLanguageID;
	private Text m_Text;

	void Awake(){
		m_Text = this.gameObject.GetComponent<Text> ();
	}

	// Use this for initialization
	void Start () {
		StartCoroutine (showText());
	}

	public IEnumerator showText(){

		while (!LanguageDataManager.GetInstance ().IsLoad()) {
			Debug.LogWarning ("language is not yet loaded");
			yield return null;
		}

		while (!UserBasicDataManager.GetInstance ().IsLoad()) {
			Debug.LogWarning ("user basic is not yet loaded");
			yield return null;
		}

		if (!LanguageDataManager.GetInstance ().IsContainsKey (eLanguageID)) {
			Debug.LogError ("no such lan id:" + eLanguageID);
			yield break;
		
		}

		LANGUAGE lan = LanguageDataManager.GetInstance ().GetDataById (eLanguageID);
		if (lan == null) {
			Debug.LogError ("no such lan id:" + eLanguageID);
			yield break;
		}

		m_Text.text = getStringByID(eLanguageID);
	}
		
	public static string getStringByID (string eLanguageID) {

		LANGUAGE lan = LanguageDataManager.GetInstance ().GetDataById (eLanguageID);
		if (lan == null) {
			Debug.LogError ("no such lan id:" + eLanguageID);
		}

		byte[] str;
		switch (UserBasicDataManager.GetInstance().GetData().language) {
		case UserLanguage.USER_LANGUAGE_ENGLISH:
			str = lan.english;
			break;
		case UserLanguage.USER_LANGUAGE_JAPANESE:
			str = lan.japanese;
			break;
		case UserLanguage.USER_LANGUAGE_CHINESE:
			str = lan.chinese_simple;
			break;
		case UserLanguage.USER_LANGUAGE_TRADITIONAL_CHINESE:
			str = lan.chinese_traditional;
			break;
		default:
			str = lan.english;
			break;
		}

		return System.Text.Encoding.UTF8.GetString (str);
	}
}
