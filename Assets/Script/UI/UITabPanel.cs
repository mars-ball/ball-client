﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UITabPanel : MonoBehaviour {

	public class TabCell:MonoBehaviour{
		public int m_index;
	}

	public bool m_isMultiPanel;
	public List<ITabPage> ePanels;

	public List<Image> m_tabBtns;

	public int m_currSel;
	public static readonly Color TAB_ACTIVE_COLOR = new Color(241f/255f, 211f/255f, 172f/255f, 1);
	public static readonly Color TAB_DEACTIVE_COLOR = new Color(222f/255f, 222f/255f, 222f/255f, 1);
	public IDataListCanGetNum m_dataSource;
	public ITabPage eTabPage;

	void Awake(){
		int index = 0;

		foreach(var tab in m_tabBtns){
			TabCell tabCell = tab.gameObject.AddComponent<TabCell>();
			if(tabCell == null){
				Debug.Log("add tab cell component failed");
				return;
			
			}		
			tabCell.m_index = index++;
			EventTriggerListener.Get(tab.gameObject).onDown = OnClickTab;
		}	
	
		if(ePanels.Count > 0){
			m_isMultiPanel = true;		
		}

	}

	void Start(){
		StartCoroutine(WaitForDataBaseReady());
	}


	IEnumerator WaitForDataBaseReady(){

		while(!m_dataSource || !m_dataSource.m_isDataReady){
			yield return null;
		}
		if(!m_isMultiPanel){
			eTabPage.SetDataSource(m_dataSource);
		}else{
		
			foreach(var page in ePanels){
			
				page.SetDataSource(m_dataSource);
			}
		
		}
		RefreshTab(0);

	}

	void OnClickTab(GameObject tabCell){
	
		if(!m_dataSource.m_isDataReady){
			return;
		}

		TabCell cell = tabCell.GetComponent<TabCell>();
		if(cell == null){
			Debug.LogError("tab cell is null");
			return;		
		}

		int idx = cell.m_index;

		RefreshTab(idx);
	
	
	}

	public void RefreshTab(int index){

		m_currSel = index;

		for(int ii = 0; ii < m_tabBtns.Count; ii++){

			if(ii == index){
				m_tabBtns[ii].color = TAB_ACTIVE_COLOR;			
			}else{
				m_tabBtns[ii].color = TAB_DEACTIVE_COLOR;
			}
		}


		if(!m_isMultiPanel){
			eTabPage.Refresh(index);
		}else{
			for(int ii = 0; ii < ePanels.Count; ii++){
				ePanels[ii].gameObject.SetActive(false);
			}
			ePanels[index].gameObject.SetActive(true);
			ePanels[index].Refresh(index);
		}

	}

}
