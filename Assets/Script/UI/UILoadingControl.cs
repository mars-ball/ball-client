﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UILoadingControl : MonoBehaviour {

	public enum Step
	{
		INVALID,
		INIT, 
		MOVING,
		WAITEND,
		END

	}

	public Step m_step;

	public List<Transform> eEggs;

	public Transform eLastEgg;
	public Transform eEater;

	private int m_count;

	public Vector3 m_targetPos;

	public static readonly float EAT_MAX_TIME = 0.5f;
	public static readonly float EAT_MIN_TIME = 0.1f;

	private Vector3 m_vel;

	void Awake(){
		eEggs.Sort ((item1, item2) => item1.localPosition.x.CompareTo (item2.localPosition.x));
		m_count = eEggs.Count;
		eLastEgg = eEggs [eEggs.Count - 1];
		m_targetPos = eEater.localPosition;
		m_step = Step.INIT;
	}


	public void StartLoading(){
		foreach (var egg in eEggs) {
			egg.gameObject.SetActive (true);
		}
		eEater.gameObject.SetActive (true);
		this.gameObject.SetActive (true);
		m_step = Step.MOVING;

	}

	public void SetLoadingPercent(int per){
		if (per >= 100) {
			m_step = Step.WAITEND;
			m_targetPos = eLastEgg.localPosition;

		} else {

			int index = (int)Mathf.Abs(m_count * (1.0f * per / 100.0f)) - 1;
			if (index < 0) {
				index = 0;
			}
			m_targetPos = eEggs [index].localPosition;

		}

	}


	void Update(){
		switch (m_step) {
		case Step.MOVING:
			eEater.localPosition = Vector3.SmoothDamp (eEater.localPosition, m_targetPos, ref m_vel, EAT_MAX_TIME);
			break;
		case Step.WAITEND:
			eEater.localPosition = Vector3.SmoothDamp (eEater.localPosition, m_targetPos, ref m_vel, EAT_MIN_TIME);
			break;	

		}

		if (Mathf.Abs (eEater.localPosition.x - eLastEgg.localPosition.x) < 10 || eEater.localPosition.x > eLastEgg.localPosition.x) {
			m_step = Step.END;
			this.gameObject.SetActive (false);
		}

		foreach (var egg in eEggs) {
			if (Mathf.Abs (eEater.localPosition.x - egg.localPosition.x) < 10 || eEater.localPosition.x > egg.localPosition.x) {
				egg.gameObject.SetActive (false);			
			}
		}


	}




}
