﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;

public class NotifScrollCell : IScrollCell {

	public int m_id;	
	public int m_index;
	public string m_msg;
	//public Image ePhoto;

	public Text eMsgText;

	public Button AcceptButton;
	public Button DeclineButton;

	public const int ACCEPT_BUTTON = 1;
	public const int DECLINE_BUTTON = 2;

	public IDataListCanGetNum m_dataSource;

	public ITabPage m_scroll;


	public override void Show(int typeIndex, int index){
		m_index = index;
		CSNotifItem notif = m_dataSource.GetDataByIndex(typeIndex, index) as CSNotifItem;
		if(notif == null){
			Debug.Log("notif is null");
			return;		
		}

		m_id = notif.seqNo;

		if(notif.addFriend != null){
			int photoId = notif.addFriend.info.photo;

			/*Texture2D photoTex = PhotoGen.GetInstance().getPhotoTexture(photoId);

			Sprite photoSp = Sprite.Create(photoTex, new Rect(0, 0, photoTex.width, photoTex.height), new Vector2(0.5f, 0.5f));

			ePhoto.sprite = photoSp;*/

			m_msg = notif.addFriend.info.name + " (lv." + notif.addFriend.info.lv + ") wants to add you as friend.";	
			eMsgText.text = m_msg;

		}

	}

	public override void SetDataSource(IDataListCanGetNum dataSource){
		m_dataSource = dataSource;
	}

	public void OnClickAccept(){
		//Debug.LogWarning("accept uid:" + uid);
		new CSHandleNotifMessage(m_id, pbmsg.login.HandleNotifType.HANDLE_NOTIF_TYPE_ACCEPT).Send(
			delegate(IExtensible msg){
				HMessage message = (HMessage)msg;

				CSHandleNotifResponse response = message.response.handleNotif;
				if(response == null){
					Debug.LogError("response null , recv id :"+ (int)message.msgType);
					return false;			
				}
				if(response.ret == Ret.Success){
					//nameText.text = "success";
					Debug.LogError("handle accept success");
				}
				m_scroll.RemoveItem(m_index);

				return true;
			}
		);
	}

	public void OnClickReject(){
	
		new CSHandleNotifMessage(m_id, pbmsg.login.HandleNotifType.HANDLE_NOTIF_TYPE_IGNORE).Send(
			delegate(IExtensible msg){
				HMessage message = (HMessage)msg;

				m_scroll.RemoveItem(m_index);


				return true;
			}
		);
	
	}

	void Awake(){	
	
	}


	public void OnClickDecline(){
	}

}
