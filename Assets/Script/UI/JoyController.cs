﻿using UnityEngine;
using System.Collections;
public class JoyController : MonoBehaviour {
	private Vector3 m_origin;

	public Camera m_mainCam;
	public RectTransform m_padCanvas;

	public GameObject m_joyBase;
	public GameObject m_joyStick;

	private Transform m_baseTrans;
	private Transform m_joyTrans;

	public bool m_isDrag = false;

	private Vector3 m_deltaPosition;

	private float STICK_MOVE_MAX_DISTANCE;

	private static readonly float JOY_BASE_OFFSET_PERCENT = 0.4f;
	private static readonly float JOY_STICK_OFFSET_PERCENT = 0.1f;
	private static readonly float JOY_STICK_R_PERCENT = 0.5f;
	private float JOY_DIR;
	private Vector2 SCREEN_CENTER;


	private Vector3 m_joyDir;
	private float m_joyMag;

	void Awake(){
		EventTriggerListener.Get (gameObject).onDrag = OnDrag;
		//EventTriggerListener.Get (gameObject).onDragOut = OnDragOut;
		EventTriggerListener.Get (gameObject).onDown = OnTouchDown;
		//EventTriggerListener.Get (gameObject).onClick = OnClick;
		EventTriggerListener.Get (gameObject).onUp = OnPointerUp;

		m_joyBase.SetActive(false);
		m_joyStick.SetActive(false);
		m_baseTrans = m_joyBase.GetComponent<Transform>();
		m_joyTrans = m_joyStick.GetComponent<Transform>();
	}

	void Start(){
		//m_origin = transform.localPosition;
		float halfHeight = Screen.height;//m_mainCam.orthographicSize;
		float halfWidth = Screen.width;//halfHeight * m_mainCam.aspect;

		float shortDir = (halfHeight > halfWidth? halfWidth: halfHeight);
		JOY_DIR = shortDir * JOY_STICK_R_PERCENT;
		SCREEN_CENTER = new Vector2(Screen.width/2.0f, Screen.height/2.0f);

		STICK_MOVE_MAX_DISTANCE = shortDir * JOY_STICK_OFFSET_PERCENT;

	}

	void Update(){
		float dis = Vector3.Distance(m_joyTrans.localPosition, m_origin);
		if(dis >= STICK_MOVE_MAX_DISTANCE){
			Vector3 vec = m_origin + (m_joyTrans.localPosition - m_origin)*STICK_MOVE_MAX_DISTANCE/dis;
			m_joyTrans.localPosition = vec;		
		}
	}

	/*
	void OnClick(GameObject go){

		Debug.LogError ("click");
		
		Vector2 pPos0 = Input.mousePosition;
		Vector2 dir = pPos0 - SCREEN_CENTER;		

		new DirectMoveSyncUpMessage (dir.normalized, 1.0f).Send ();

	}*/


	void OnPointerUp(GameObject go){
	
		OnDragOut (go, new Vector2(0, 0));
	}

	void OnTouchDown(GameObject go){
		m_isDrag = false;

		Debug.LogError ("touch down");
		/*if(!(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)){
			Debug.LogError("touch count:" + Input.touchCount);
		}else{		*/
		Vector2 pPos0 = Input.mousePosition;//Input.touches[0].position;
		Vector2 pPos = SCREEN_CENTER + (pPos0 - SCREEN_CENTER)*JOY_BASE_OFFSET_PERCENT;

		Vector2 wPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(m_padCanvas, pPos, null, out wPos);//(new Vector3(pPos));

		m_baseTrans.localPosition = new Vector3(wPos.x, wPos.y, 0);

		Vector2 touchOffset = pPos - SCREEN_CENTER;

		float joyMoveDis = 0f;

		if(touchOffset.magnitude >= JOY_DIR){
			joyMoveDis = 1f;
		}else{			
			joyMoveDis = touchOffset.magnitude / JOY_DIR;			
		}

		Vector2 joyScreenPos = SCREEN_CENTER + touchOffset.normalized * STICK_MOVE_MAX_DISTANCE * joyMoveDis;
		Vector2 joyCanvasPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(m_padCanvas, joyScreenPos, null, out joyCanvasPos);

		m_joyTrans.localPosition = new Vector3(joyCanvasPos.x, joyCanvasPos.y, 0);

		m_joyBase.SetActive(true);
		m_joyStick.SetActive(true);

		//m_isMouseDrag = true;
	//	}
	
	}

	public void OnDrag(GameObject go, Vector2 delta){
		m_isDrag = true;
		m_joyTrans.localPosition += new Vector3(delta.x, delta.y, 0);
	}

	public void OnDragOut(GameObject go, Vector2 delta){
		Debug.LogError ("drag out");
		//m_joyTrans.localPosition = m_origin;
		m_joyBase.SetActive(false);
		m_joyStick.SetActive(false);

		if (!m_isDrag) {
			Vector2 pPos0 = Input.mousePosition;
			Vector2 dir = pPos0 - SCREEN_CENTER;		

			float per = dir.magnitude / (Screen.width / 2.0f);

			if (per > 0.2) {
				new DirectMoveSyncUpMessage (dir.normalized, 1.0f).Send ();
			} else {
				new DirectMoveSyncUpMessage (dir.normalized, 0.0f).Send ();

			}


		}

		//m_isMouseDrag = false;
	}

	public void OnMoveStart(GameObject go){
	}


	public int GetDirDis(out Vector3 dir, out float dis ){
		dis = Vector3.Distance(m_joyTrans.localPosition, m_origin)/STICK_MOVE_MAX_DISTANCE;
		if(dis > 1f){
			dis = 1f;
		}

		if(dis < 0.1){

			dir = new Vector3(0, 0, 0);
		
		}else{
			dir = (m_joyTrans.localPosition - m_origin).normalized;
		}
		return 0;
	}

}












