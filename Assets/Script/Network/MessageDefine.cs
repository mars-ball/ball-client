﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pbmsg;
using System;
using System.Reflection;
using ProtoBuf;


public class MessageDefine : MonoBehaviour {

	private static MessageDefine m_instance;

	private Dictionary<MSGID, Type> m_messageApi;

	public Dictionary<MSGID, Type> m_messageType;

	private MessageDefine(){}

	public static MessageDefine GetInstance(){
		if(m_instance == null){
			GameObject obj = new GameObject();
			obj.name = "MessageDefine";
			m_instance = obj.AddComponent<MessageDefine>();
			if(m_instance == null){
				Debug.LogError("instance null");
			}
		}
		return m_instance;
	}

	void Awake(){
		m_messageApi = new Dictionary<MSGID, Type>();
		m_messageApi.Add(MSGID.CreateUser_Response, typeof(CreateUserMessage));
		m_messageApi.Add(MSGID.Login_Response, typeof(LoginMessage));	
		m_messageApi.Add(MSGID.CSDirectMove_SyncDown, typeof(DirectMoveSyncDownMessage));
		m_messageApi.Add (MSGID.CSSplit_Response, typeof(CSSplitMessage));
		m_messageApi.Add(MSGID.CSRoundOver_SyncDown, typeof(CSGameRoundOverSyncDownMessage));
		//m_messageApi.Add(MSGID.CSGetUserPack_Response, typeof(CSGetU
		m_messageApi.Add(MSGID.CSChangeInfo_Response, typeof(CSChangeInfoMessage));
		m_messageApi.Add(MSGID.CSFollowFriendInstantBattle_Response, typeof(CSFollowInstantBattleMessage));
		InitMessageTypes ();
	}

	void InitMessageTypes (){
		m_messageType = new Dictionary<MSGID, Type> ();
		m_messageType.Add (MSGID.H_Message, typeof(pbmsg.login.HMessage));
		m_messageType.Add (MSGID.Room_Message, typeof(pbmsg.room.RoomMessage));
	}

	public bool HasHandler(MSGID msgId){
		return m_messageApi.ContainsKey(msgId);
	}

	public Type GetHandlerTypeByID(MSGID msgId){
		if(!HasHandler(msgId)){
			Debug.LogError("get handler no msgid:" + msgId);
			return null;
		}
		return m_messageApi[msgId];
	}

	public IExtensible getMessageFromId(MSGID msgId){
		if (!m_messageType.ContainsKey (msgId)) {
			Debug.LogError ("get message from id failed:" + msgId);
			return default(IExtensible);
		}

		IExtensible instance = (IExtensible)Activator.CreateInstance(m_messageType[msgId]);
		return instance;
	}



}
