﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSEatMineSyncUpMessage: MessageHandler {

	int m_unitId;
	int m_mineId;
	Int64 m_serverTime;

	public CSEatMineSyncUpMessage(int unitId, int mineId, Int64 servertime):base(ServerType.BattleServer, MSGID.CSEatMine_SyncUp, MSGID.INVALID){
		m_unitId = unitId;
		m_mineId = mineId;
		m_serverTime = servertime;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSEatMine_SyncUp;
		msg.syncUp = new SyncUp();
		msg.syncUp.eatMine = new CSEatMineSyncUp();
		CSEatMineSyncUp eat = msg.syncUp.eatMine;
		eat.serverTime = m_serverTime;
		CSEatMineUnit unit = new CSEatMineUnit();
		unit.mineId = m_mineId;
		unit.unitId = m_unitId;

		eat.mines.Add(unit);


		m_sendMessage = msg;

	}

}