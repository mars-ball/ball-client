﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSAddFriendMessage: MessageHandler {

	int m_friendId;

	public CSAddFriendMessage(int uid):base(ServerType.LoginServer, MSGID.CSAddFriend_Request, MSGID.CSAddFriend_Response){
		m_friendId = uid;			
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSAddFriend_Request;
		msg.request = new Request();
		msg.request.addFriend = new CSAddFriendRequest();
		CSAddFriendRequest req = msg.request.addFriend;
		req.uid = m_friendId;
		m_sendMessage = msg;

	}

}
