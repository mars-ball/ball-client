﻿using UnityEngine;
using System.Collections;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using System;

public class CSRegMailMessage : MessageHandler {

	public string m_mail;
	public string m_pwd;

	public CSRegMailMessage (string mail, string pwd):base(ServerType.LoginServer, MSGID.CSRegMail_Request, MSGID.CSRegMail_Response){
		m_mail = mail;
		m_pwd = pwd;
	}


	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSRegMail_Request;
		msg.request = new Request();
		CSRegMailRequest req = msg.request.regMail = new CSRegMailRequest();

		req.mail = m_mail;
		req.pwd = m_pwd;

		m_sendMessage = msg;
	}

}
