﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using localSave;
using System;

public class CSChangeInfoMessage: MessageHandler {

	public enum ChangeInfoType{
		CHANGE_INFO_TYPE_INVALID,
		CHANGE_INFO_TYPE_NAME,
		CHANGE_INFO_TYPE_PHOTO,
		CHANGE_INFO_TYPE_LANGUAGE,
	}

	public class ChangeParam{
		public int photoId;
		public string name;
		public UserLanguage lan;
	}
	public ChangeInfoType m_type;
	public ChangeParam m_param;

	public CSChangeInfoMessage(ChangeInfoType infoType, ChangeParam param):base(ServerType.LoginServer, MSGID.CSChangeInfo_Request, MSGID.CSChangeInfo_Response){	
		m_param = param;
		m_type = infoType;	
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSChangeInfo_Request;
		msg.request = new Request();
		msg.request.changeInfo = new CSChangeInfoRequest();
		CSChangeInfoRequest req = msg.request.changeInfo;
		switch (m_type) {
		case ChangeInfoType.CHANGE_INFO_TYPE_NAME:
			req.name = m_param.name;
			break;
		case ChangeInfoType.CHANGE_INFO_TYPE_PHOTO:
			req.photoId = m_param.photoId;
			break;
		case ChangeInfoType.CHANGE_INFO_TYPE_LANGUAGE:
			req.language = m_param.lan;
			break;
		default:
			break;
		}


		m_sendMessage = msg;

	}

}
