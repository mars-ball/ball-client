﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class DirectMoveSyncDownMessage: MessageHandler {

	public DirectMoveSyncDownMessage():base(ServerType.BattleServer, MSGID.INVALID, MSGID.CSDirectMove_SyncDown){
	}

	public override void NormalCallback(IExtensible message){
		pbmsg.room.RoomMessage msg = (pbmsg.room.RoomMessage)message;

		if (msg.msgType != MSGID.CSDirectMove_SyncDown) {
			Debug.LogError ("msg id error " + msg.msgType);
			return;
		}
		
		pbmsg.room.CSDirectMoveSycnDown rsp = msg.syncDown.move;
		if (rsp == null) {
			Debug.LogError ("create user rsp null ");
			return;			
		}

		MoveSync.GetInstance().ReceiveMovePacket(rsp);
	}

	public override void MakeMessageMeta(){
	}

}
