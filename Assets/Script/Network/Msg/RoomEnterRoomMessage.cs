﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class RoomEnterRoomMessage: MessageHandler {

	int m_roomId;

	public RoomEnterRoomMessage(int roomId):base(ServerType.BattleServer, MSGID.RoomEnterRoom_Request, MSGID.RoomEnterRoom_Response){
		m_roomId = roomId;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.RoomEnterRoom_Request;
		msg.request = new Request();
		msg.request.enterRoomRequest = new RoomEnterRoomRequest();
		RoomEnterRoomRequest req = msg.request.enterRoomRequest;
		req.uid = UserBasicDataManager.GetUserId ();
		req.roomid = m_roomId;
		req.token = UserBasicDataManager.GetToken ();

		m_sendMessage = msg;

	}

}
