﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class BornMessage: MessageHandler {

	public BornMessage():base(ServerType.BattleServer, MSGID.Born_Request, MSGID.Born_Response){
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.Born_Request;
		msg.request = new Request();
		msg.request.born = new BornRequest();
		BornRequest req = msg.request.born;
		req.uid = UserBasicDataManager.GetUserId ();

		m_sendMessage = msg;

	}

}
