﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSGetInitSyncMessage: MessageHandler {

	public CSGetInitSyncMessage():base(ServerType.BattleServer, MSGID.CSGetInitSync_Request, MSGID.CSGetInitSync_Response){	
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSGetInitSync_Request;
		msg.request = new Request();
		msg.request.initSync = new CSGetInitSyncRequest();
		m_sendMessage = msg;

	}

}
