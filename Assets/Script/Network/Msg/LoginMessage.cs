﻿using UnityEngine;
using System.Collections;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using System;

public class LoginMessage : MessageHandler {

	public LoginMessage ():base(ServerType.LoginServer, MSGID.Login_Request, MSGID.Login_Response){
	}


	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		Int32 uid = UserBasicDataManager.GetUserId();
		string token = UserBasicDataManager.GetToken();

		HMessage msg = new HMessage();
		msg.msgType = MSGID.Login_Request;
		msg.request = new Request();
		LoginRequest req = msg.request.login = new LoginRequest();

		req.id = UserBasicDataManager.GetUserId();
		req.token = UserBasicDataManager.GetToken();
		req.deviceId = SystemInfo.deviceUniqueIdentifier;

		m_sendMessage = msg;
	}

}
