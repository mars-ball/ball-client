﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSHandleNotifMessage: MessageHandler {

	int m_notifSeq;
	HandleNotifType m_handleType;

	public CSHandleNotifMessage(int notifSeq, HandleNotifType notifType):base(ServerType.LoginServer, MSGID.CSHandleNotif_Request, MSGID.CSHandleNotif_Response){
		m_notifSeq = notifSeq;
		m_handleType = notifType;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSHandleNotif_Request;
		msg.request = new Request();
		msg.request.handleNotif = new CSHandleNotifRequest();
		CSHandleNotifRequest req = msg.request.handleNotif;
		req.handleType = m_handleType;
		req.notifSeq = m_notifSeq;

		m_sendMessage = msg;

	}

}
