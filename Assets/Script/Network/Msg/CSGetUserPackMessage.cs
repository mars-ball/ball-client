﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSGetUserPackMessage: MessageHandler {

	public CSGetUserPackMessage():base(ServerType.LoginServer, MSGID.CSGetUserPack_Request, MSGID.CSGetUserPack_Response){	
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSGetUserPack_Request;
		msg.request = new Request();
		msg.request.userPack = new CSGetUserPackRequest();
		m_sendMessage = msg;

	}

}
