﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSBasicBuyMessage: MessageHandler {

	public class BuyParam{
		public int m_itemId;
		public string m_receipt;
		public PLATFORM_TYPE m_platform;
		public string m_payLoad;
		public string m_signature;
	}

	public BuyParam m_param;

	public CSBasicBuyMessage(BuyParam param):base(ServerType.LoginServer, MSGID.CSBasicBuy_Request, MSGID.CSBasicBuy_Response){	
		m_param = param;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSBasicBuy_Request;
		msg.request = new Request();
		msg.request.basicBuy = new CSBasicBuyRequest();
		CSBasicBuyRequest req = msg.request.basicBuy;
		req.itemId = m_param.m_itemId;
		req.receipt = m_param.m_receipt;
		req.platformType = m_param.m_platform;
		req.payLoad = m_param.m_payLoad;
		req.signature = m_param.m_signature;


		m_sendMessage = msg;

	}

}
