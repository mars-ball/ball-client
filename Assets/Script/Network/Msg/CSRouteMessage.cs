﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSRouteMessage: MessageHandler {

	bool m_hasId;
	int m_uid;

	public CSRouteMessage(bool hasId, int uid = -1):base(ServerType.RouterServer, MSGID.CSRoute_Request, MSGID.CSRoute_Response){
		m_hasId = hasId;
		m_uid = uid;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSRoute_Request;
		msg.request = new Request();
		msg.request.route = new CSRouteRequest();
		CSRouteRequest req = msg.request.route;
		req.hasUid = m_hasId;
		req.uid = m_uid;
		m_sendMessage = msg;

	}

}
