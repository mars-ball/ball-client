﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSGameRoundOverSyncDownMessage: MessageHandler {


	public CSGameRoundOverSyncDownMessage():base(ServerType.BattleServer, MSGID.INVALID, MSGID.CSRoundOver_SyncDown){
	}

	public override void NormalCallback(IExtensible message){
		pbmsg.room.RoomMessage msg = (pbmsg.room.RoomMessage)message;

		if (msg.msgType != MSGID.CSRoundOver_SyncDown) {
			Debug.LogError ("msg id error " + msg.msgType);
			return;
		}

		pbmsg.room.CSRoundOverSyncDown rsp = msg.syncDown.battleOver;
		if (rsp == null) {
			Debug.LogError ("create user rsp null ");
			return;			
		}

		UserManager.GetInstance().GetMainUser().mGamingInfo.mScore = rsp.score;


		MoveSync.GetInstance().m_camera.StopFollow ();

		//SceneManager.LoadScene("BattleEnd");

		BattleScene.GetInstance().GameRoundOver();


	}

	public override void MakeMessageMeta(){


	}

}