﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSExitGameMessage: MessageHandler {


	public CSExitGameMessage():base(ServerType.BattleServer, MSGID.CSExitRoom_Request, MSGID.CSExitRoom_Response){
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSExitRoom_Request;
		msg.request = new Request ();
		msg.request.exitRoom = new CSExitRoomRequest ();

		m_sendMessage = msg;

	}

}