﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSEatEnemyMessage: MessageHandler {

	int m_uid0;
	int m_uid1;
	int m_unitId0;
	int m_unitId1;
	long m_time;

	public CSEatEnemyMessage(int uid0, int unitId0, int uid1, int unitId1, long timestamp):base(ServerType.BattleServer, MSGID.CSEatEnemy_Request, MSGID.CSEatEnemy_Response){
		m_uid0 = uid0;
		m_uid1 = uid1;
		m_unitId0 = unitId0;
		m_unitId1 = unitId1;
		m_time = timestamp;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSEatEnemy_Request;
		msg.request = new Request();
		msg.request.eatEnemy = new CSEatEnemyRequest();
		CSEatEnemyRequest req = msg.request.eatEnemy;

		req.uid = m_uid0;
		req.unitId = m_unitId0;

		req.oppUid = m_uid1;
		req.oppUnitId = m_unitId1;

		req.serverTime = m_time;

		m_sendMessage = msg;

	}

}
