﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSSeachFriendMessage: MessageHandler {

	int m_friendId;
	string m_friendName;

	public CSSeachFriendMessage(int uid, string name):base(ServerType.LoginServer, MSGID.CSSearchForPlayer_Request, MSGID.CSSearchForPlayer_Response){
		m_friendId = uid;
		if(name != null){
			m_friendName = name;
		}else{
			m_friendName = "";
		}			
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSSearchForPlayer_Request;
		msg.request = new Request();
		msg.request.searchPlayer = new CSSearchForPlayerRequest();
		CSSearchForPlayerRequest req = msg.request.searchPlayer;
		req.uid = m_friendId;
		req.uname = m_friendName;
		m_sendMessage = msg;

	}

}
