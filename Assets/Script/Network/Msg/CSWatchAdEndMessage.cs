﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSWatchAdEndMessage: MessageHandler {

	private AdType m_type;

	public CSWatchAdEndMessage(AdType type):base(ServerType.LoginServer, MSGID.CSBasicBuy_Request, MSGID.CSBasicBuy_Response){	
		m_type = type;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSWatchAdEnd_Request;
		msg.request = new Request();
		msg.request.adEnd = new CSWatchAdEndRequest();
		CSWatchAdEndRequest req = msg.request.adEnd;
		req.type = m_type;
		m_sendMessage = msg;

	}

}
