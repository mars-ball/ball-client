﻿using UnityEngine;
using System.Collections;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using System;

public class CSLoginMailMessage : MessageHandler {

	public string m_uname;
	public string m_upwd;

	public CSLoginMailMessage (string name, string pwd):base(ServerType.LoginServer, MSGID.CSLoginMail_Request, MSGID.CSLoginMail_Response){
	
		m_uname = name;
		m_upwd = pwd;
	}


	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSLoginMail_Request;
		msg.request = new Request();
		CSLoginMailRequest req = msg.request.loginMail = new CSLoginMailRequest();

		req.mail = m_uname;
		req.pwd = m_upwd;

		m_sendMessage = msg;
	}

}
