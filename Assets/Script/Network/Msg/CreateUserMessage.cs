﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CreateUserMessage: MessageHandler {

	public string m_userName;

	public CreateUserMessage(string userName):base(ServerType.LoginServer, MSGID.CreateUser_Request, MSGID.CreateUser_Response){
		m_userName = userName;	
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		Int64 uuid = GenerateUUID();
		HMessage msg = new HMessage();
		msg.msgType = MSGID.CreateUser_Request;
		msg.request = new Request();
		msg.request.createUser = new CreateUserRequest();
		CreateUserRequest req = msg.request.createUser;
		req.uuid = (Int32)uuid;	
		req.name = m_userName;
		req.deviceId = SystemInfo.deviceUniqueIdentifier;

		m_sendMessage = msg;

	}


	private Int64 GenerateUUID(){

		Int64 uuid = HTime.NowTimeMs();
		return uuid;

	}
}
