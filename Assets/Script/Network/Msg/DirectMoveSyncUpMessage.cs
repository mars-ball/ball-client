﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class DirectMoveSyncUpMessage: MessageHandler {

	Vector3 m_dir;
	float m_dis;

	public DirectMoveSyncUpMessage(Vector3 dir, float dis):base(ServerType.BattleServer, MSGID.CSDirectMove_SyncUp, MSGID.INVALID){
		m_dir = dir;
		m_dis = dis;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSDirectMove_SyncUp;
		msg.syncUp = new SyncUp();
		msg.syncUp.move = new CSDirectMoveSyncUp();
		CSDirectMoveSyncUp req = msg.syncUp.move;
		req.uid = UserBasicDataManager.GetUserId ();
		req.joy = new CSJoyStickOperation();
		CSJoyStickOperation joy = req.joy;
		joy.dir = new pbmsg.room.Vector();
		pbmsg.room.Vector dir = joy.dir;

		dir.x = m_dir.x;
		dir.y = m_dir.y;

		joy.dis = m_dis;

		m_sendMessage = msg;

	}

}
