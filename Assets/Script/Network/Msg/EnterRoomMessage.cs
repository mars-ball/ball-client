﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class EnterRoomMessage: MessageHandler {

	int m_roomType;
	int m_roomId;
	int m_campId;

	public EnterRoomMessage(int roomType, int roomId = -1, int campId = -1):base(ServerType.LoginServer, MSGID.RoomEnter_Request, MSGID.RoomEnter_Response){
		m_roomType = roomType;
		m_roomId = roomId;
		m_campId = campId;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.RoomEnter_Request;
		msg.request = new Request();
		msg.request.roomEnter = new RoomEnterRequest();
		RoomEnterRequest req = msg.request.roomEnter;
		req.roomTypeId = m_roomType;
		req.roomId = m_roomId;
		req.campId = m_campId;
		m_sendMessage = msg;

	}

}
