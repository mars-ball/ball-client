﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.room;
using ProtoBuf;
using System;

public class CSSplitMessage: MessageHandler {

	Vector3 m_dir;
	float m_dis;
	Int64 m_serverTime;

	public CSSplitMessage(Vector3 dir, float dis, Int64 servertime):base(ServerType.BattleServer, MSGID.CSSplit_Request, MSGID.CSSplit_Response){
		m_dir = dir;
		m_dis = dis;
		m_serverTime = servertime;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		RoomMessage msg = new RoomMessage();
		msg.msgType = MSGID.CSSplit_Request;
		msg.request = new pbmsg.room.Request();
		msg.request.split = new CSSplitRequest();
		CSSplitRequest split = msg.request.split;
		split.serverTime = m_serverTime;
		split.dir = new Vector();
		Vector dir = split.dir;
		dir.x = m_dir.x;
		dir.y = m_dir.y;

		m_sendMessage = msg;

	}

}