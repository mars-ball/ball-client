﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSGetFriendListMessage: MessageHandler {

	public CSGetFriendListMessage():base(ServerType.LoginServer, MSGID.CSGetFriendList_Request, MSGID.CSGetFriendList_Response){		
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSGetFriendList_Request;
		msg.request = new Request();
		msg.request.getFriendList = new CSGetFriendListRequest();
		CSGetFriendListRequest req = msg.request.getFriendList;
		m_sendMessage = msg;

	}

}
