﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSFollowInstantBattleMessage: MessageHandler {

	int m_followId;

	public CSFollowInstantBattleMessage(int followId):base(ServerType.LoginServer, MSGID.CSFollowFriendInstantBattle_Request, MSGID.CSFollowFriendInstantBattle_Response){		
		m_followId = followId;
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSFollowFriendInstantBattle_Request;
		msg.request = new Request();
		msg.request.followFriendInstantBattle = new CSFollowFriendInstantBattleRequest();
		CSFollowFriendInstantBattleRequest req = msg.request.followFriendInstantBattle;
		req.friendId = m_followId;
		m_sendMessage = msg;

	}

}
