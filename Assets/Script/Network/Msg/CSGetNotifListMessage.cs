﻿using UnityEngine;
using System.Collections;
using pbmsg;
using pbmsg.login;
using ProtoBuf;
using System;

public class CSGetNotifListMessage: MessageHandler {

	public CSGetNotifListMessage():base(ServerType.LoginServer, MSGID.CSGetNotifList_Request, MSGID.CSGetNotifList_Response){		
	}

	public override void NormalCallback(IExtensible message){
	}

	public override void MakeMessageMeta(){

		HMessage msg = new HMessage();
		msg.msgType = MSGID.CSGetNotifList_Request;
		msg.request = new Request();
		msg.request.notifList = new CSGetNotifListRequest();
		CSGetNotifListRequest req = msg.request.notifList;
		m_sendMessage = msg;

	}

}
