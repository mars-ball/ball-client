﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using System;
using System.Reflection;
using pbmsg;
using pbmsg.room;

public class MessageSheduler : MonoBehaviour{

	public Text m_downSpeedTxt;
	public Int64 m_testCounter = 0;

	public class QueueUnit{
		public IExtensible msg;
		public MSGID topKind;
		public int seq;
		public QueueUnit(IExtensible msg_, MSGID topKind_, int seq_){
			msg = msg_;
			seq = seq_;
			topKind = topKind_;
		}
	}

	private static MessageSheduler m_instance;
	private System.Object locker = new System.Object();
	private System.Object m_lockObj = new System.Object();
	public static Queue<QueueUnit> m_queue;

	private MessageDefine m_messageDefine;

	static int m_waiterId = 0;
	public Dictionary<int, NetWaiter> m_waiters;
	
	public class NetWaiter{
		public Int64 timestamp;
		public MessageHandler handler;	
		public NetWaiter(Int64 _timestamp, MessageHandler _waiter){
			timestamp = _timestamp;
			handler = _waiter;
		}
	}


	public static MessageSheduler GetInstance(){

		if (m_instance == null) {
			GameObject obj = new GameObject ();
			obj.name = "Message Shedular";
			m_instance = obj.AddComponent <MessageSheduler> ();		
			m_queue = new Queue<QueueUnit>();
		}
		return m_instance;
	}

	void Awake(){
		m_messageDefine = MessageDefine.GetInstance();	
		m_waiters = new Dictionary<int, NetWaiter>();
		m_queue = new Queue<QueueUnit>() ;


	}

	public void Enqueue(IExtensible ex, MSGID topKind){	
		int seq;
		if (!MessageCoder.GetMessageSeq (topKind, ex, out seq)) {
			Debug.LogError ("get seq failed");
			return;
		}

		lock (locker) {
			m_queue.Enqueue(new QueueUnit(ex, topKind, seq));	
		}
	}

	public Int32 Dequeue(){
		//Debug.LogError ("dequeue 0");
		m_testCounter += 1;
		QueueUnit unit = null;

		lock (locker) {
			//Debug.LogError ("dequeue 1");
			unit = m_queue.Dequeue();		
		}

		if(unit.msg == null){
			Debug.Log ("dequeued message null");
			return -1;
		}

		if(unit.seq > 0){
			if(!m_waiters.ContainsKey(unit.seq)){
				Debug.Log ("no such sessionId");									
			}

			NetWaiter waiter = m_waiters[unit.seq];

			if(waiter == null){
				Debug.LogError("waiter null " + unit.seq);
			}

			MessageHandler handler = waiter.handler;

			handler.WaitCallback(unit.msg);
			
		}else{
			
			MSGID msgId;
			if (!MessageCoder.GetMessageMsgId (unit.topKind, unit.msg, out msgId)) {
				Debug.LogError ("get message id failed topKind:" + unit.topKind);
				return 0;
			}
			/*
			if (unit.topKind == MSGID.Room_Message) {
				RoomMessage room = (RoomMessage)unit.msg;
				if (room != null && msgId == MSGID.CSDirectMove_SyncDown && m_downSpeedTxt != null) {

					foreach (var move in room.syncDown.move.move) {
						if (UserBasicDataManager.GetInstance ().GetData ().id == move.uid) {
							m_downSpeedTxt.text = move.units [0].v.x + ", " + move.units [0].v.y + " :" + m_testCounter;
							Debug.LogWarning ("dequeue sertime:" + room.syncDown.move.timestamp + " queue count:"+ m_queue.Count);
								
						}
					}
				}
			}*/

			Type handlerType = m_messageDefine.GetHandlerTypeByID(msgId);

			if(handlerType == null){
				Debug.LogError("failed get handler type: " + msgId);
				return -1;				
			}

			System.Object inst = (System.Object)Activator.CreateInstance(handlerType);
			MethodInfo method = handlerType.GetMethod("NormalCallback");
			method.Invoke(inst, new object[]{unit.msg});		
		}

		return 0;
	}


	public int RestMessageNum(){
		lock (locker) {
			return m_queue.Count;
		}
	}


	void Update(){
		while (RestMessageNum() > 0) {
			Int32 ret = Dequeue();			
		}	
	}


	private int GetWaitCard(){
		
		lock(m_lockObj){

			do{

				if(m_waiterId >= uint.MaxValue-1){
					m_waiterId = 0;
				}

				m_waiterId++;

				if(!m_waiters.ContainsKey(m_waiterId)){
					return m_waiterId;
				}
			}while(true);
		}
		return 0;
	}
	/*
	public uint AddWaiter(MessageHandler msgHandler){
		uint waiterId = GetWaitCard();
		if(waiterId == 0){
			Debug.LogError("error waiterId" + waiterId);
		}

		Int64 timestamp = HTime.NowTimeMs();		
		NetWaiter waiter = new NetWaiter(timestamp, msgHandler);
		m_waiters.Add (waiterId, waiter);
		return waiterId;
	}*/



	public void SendNetMessage(ServerType serverType, IExtensible messgae){	
		NetworkManager.GetInstance().SendMessage(serverType, messgae);	
	}

	public void SendNetMessage(ServerType serverType, MessageHandler handler, IExtensible message){

		int waiterId = GetWaitCard();

		if(waiterId == 0){
			Debug.LogError("error waiterId" + waiterId);
		}

		if (!MessageCoder.SetMessageSeq (serverType, message, waiterId)) {
			Debug.LogError ("set seq failed, server:" + serverType + " wid:" + waiterId);
			return;
		}

		NetWaiter waiter = new NetWaiter(HTime.NowTimeMs(), handler);

		m_waiters.Add(waiterId, waiter);

		NetworkManager.GetInstance().SendMessage(serverType, message);

	}

}







