﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System;
using System.IO;
using System.Reflection;
using ProtoBuf;
using pbmsg;


public class MessageCoder{
	public static bool DecodeMessage(byte[] data, out IExtensible message, out MSGID topKind){
		
		message = null;
		topKind = MSGID.INVALID;

		uint checkSum_tmp = BitConverter.ToUInt32 (data, data.Length - 4);

		int checkSum = IPAddress.NetworkToHostOrder((int)checkSum_tmp);

		Adler32 adler = new Adler32 ();
		adler.Update (data, 0, data.Length - 4);

		long calcSum = adler.Checksum;

		int calcSum32 = (int)(calcSum & 0xFFFFFFFF);

		if((long)checkSum != calcSum32){
			Debug.LogError("check sum is not matched, check_sum:" + checkSum + " calcSum:" + calcSum32);
			return false;
		}

		MemoryStream mem = new MemoryStream(data, 4, data.Length - 8);

		int topKind_tmp = BitConverter.ToInt32 (data, 0);
		int topKind_net = IPAddress.NetworkToHostOrder((int)topKind_tmp);
		topKind = (MSGID)topKind_net;

		try{
			switch (topKind) {
			case MSGID.H_Message:
				message = Serializer.Deserialize<pbmsg.login.HMessage> (mem);
				break;
			case MSGID.Room_Message:
				message = Serializer.Deserialize<pbmsg.room.RoomMessage> (mem);
				break;
			default:
				Debug.LogError ("no such topKind:" + topKind);
				return false;
			}

			if(message == null){
				Debug.LogError("deserialize failed:" + topKind);
			}

			mem.Close();
		}catch(Exception e){
			Debug.LogError("decode failed, topKind_net:" + topKind_net + " topKind:" + topKind + " dataLen:" + data.Length + " checkSum:" + checkSum + " calcSum" + calcSum + " error msg:" + e.Message + " trace:" + e.StackTrace);
			return false;
		}

		return true;
	
	}

	public static byte[] CodeMessage(IExtensible message, MSGID topKind){

		MemoryStream mem = new MemoryStream ();
		Serializer.Serialize<IExtensible> (mem, message);
		byte[] msg = mem.ToArray();

		byte[] data = new byte[msg.Length + 12];

		int topKind_net = IPAddress.HostToNetworkOrder ((int)topKind);
		byte[] topKind_byte = BitConverter.GetBytes (topKind_net);

		Array.Copy (topKind_byte, 0, data, 4, 4);

		Array.Copy (msg, 0, data, 8, msg.Length);

		Adler32 adler = new Adler32 ();
		adler.Update (data, 4, data.Length - 8);

		int sum = (int)adler.Checksum;

		int sum_net = IPAddress.HostToNetworkOrder (sum);
		byte[] sum_byte = BitConverter.GetBytes (sum_net);

		Array.Copy (sum_byte, 0, data, msg.Length + 8, 4);

		int length = data.Length - 4;
		int length_net = IPAddress.HostToNetworkOrder (length);
		byte[] length_byte = BitConverter.GetBytes (length_net);

		Array.Copy (length_byte, 0, data, 0, 4);

		mem.Close();
		return data;

	}

	public static bool GetTopKindFromServerType(ServerType serverType, out MSGID msgId){
		msgId = MSGID.INVALID;
		switch (serverType) {
		case ServerType.LoginServer:
			msgId = MSGID.H_Message;
			break;
		case ServerType.BattleServer:
			msgId = MSGID.Room_Message;
			break;
		case ServerType.RouterServer:
			msgId = MSGID.H_Message;
			break;
		default:
			Debug.LogError ("get msg id failed,serverType:" + serverType);
			return false;
		}
		return true;

	}



	public static bool SetMessageSeq(ServerType serverType, IExtensible msg, int seq){

		switch (serverType) {
		case ServerType.LoginServer:
			{
				pbmsg.login.HMessage message = (pbmsg.login.HMessage)msg;
				message.seq = seq;
				break;
			}
		case ServerType.BattleServer:
			{
				pbmsg.room.RoomMessage message = (pbmsg.room.RoomMessage)msg;
				message.seq = seq;
				break;
			}
		case ServerType.RouterServer:
			{
				pbmsg.login.HMessage message = (pbmsg.login.HMessage)msg;
				message.seq = seq;
				break;
			}

		default:
			Debug.LogError ("set seq failed, no servertype:" + serverType);
			return false;
		}
	
		return true;
	}


	public static bool GetMessageSeq(MSGID topKind, IExtensible msg, out int seq){
		seq = 0;
		switch (topKind) {
		case MSGID.H_Message:
			{
				pbmsg.login.HMessage message = (pbmsg.login.HMessage)msg;
				seq = message.seq;
				break;
			}
		case MSGID.Room_Message:
			{
				pbmsg.room.RoomMessage message = (pbmsg.room.RoomMessage)msg;
				seq = message.seq;
				break;
			}
		default:
			Debug.LogError ("get seq failed, no topKind:" + topKind);
			return false;
		}

		return true;
	}

	public static bool GetMessageMsgId(MSGID topKind, IExtensible msg, out MSGID msgId){
		
		msgId = MSGID.INVALID;
		switch (topKind) {
		case MSGID.H_Message:
			{
				pbmsg.login.HMessage message = (pbmsg.login.HMessage)msg;
				msgId = message.msgType;
				break;
			}
		case MSGID.Room_Message:
			{
				pbmsg.room.RoomMessage message = (pbmsg.room.RoomMessage)msg;
				msgId = message.msgType;
				break;
			}
		default:
			Debug.LogError ("get msgid failed, no topKind:" + topKind);
			return false;
		}

		return true;
	}

	public static T GetHRequestMessage<T>(pbmsg.login.HMessage message, MSGID id) where T:IExtensible{

		if(message.request == null){
			message.request = new pbmsg.login.Request();
		}
		pbmsg.login.Request req = message.request;

		T rreq = (T)ReflectionHelper.GetFieldValue(req, typeof(T).Name);

		if(rreq == null){
			ReflectionHelper.SetFieldValue(req, typeof(T).Name, (T)Activator.CreateInstance(typeof(T), new object[]{}));
			rreq = (T)ReflectionHelper.GetFieldValue(req, typeof(T).Name);

			if(rreq == null){
				Debug.LogError("failed to make request message :" + typeof(T).Name);
				return default(T);
			}

			return rreq;
		}

		return rreq;
	}

	public static T GetHResponseMessage<T>(pbmsg.login.HMessage message, MSGID id) where T:IExtensible{
		if(message.response == null){
			Debug.LogError("response null");
			return default(T);
		}

		pbmsg.login.Response rsp = message.response;
		
		T rrsp = (T)ReflectionHelper.GetFieldValue(rsp, typeof(T).Name);
		
		if(rrsp == null){
			Debug.LogError("failed to make request message :" + typeof(T).Name);
			return default(T);	
		}
		
		return rrsp;
	}


	public static T GetHMessageById<T>(pbmsg.login.HMessage message, MSGID id) where T:IExtensible{

		switch(id){
			case MSGID.CreateUser_Request:
			case MSGID.Login_Request:
				return GetHRequestMessage<T>(message, id); 
			break;
			case MSGID.CreateUser_Response:
			case MSGID.Login_Response:
				return GetHResponseMessage<T>(message, id);	
			break;		
		}
		return default(T);
	}



}









