﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System;
using System.Text;
using ProtoBuf;
using pbmsg;

public class StateObject {
	
	public readonly byte[] head = new byte[4];
	public int headNum = 0;
	public const int HEAD_LEN = 4;
	public byte[] body = null;
	public int bodyNum = 0;
	public int BODY_LEN;

	public readonly Socket listener;
	public readonly int id;

	public StateObject(Socket listener, int id = -1)
	{
		this.listener = listener;
	}

}

public class TcpNetworkAsync : MonoBehaviour {



	private bool close;

	public delegate void ConnectedHandler(TcpNetworkAsync a);
	public delegate void ClientMessageReceivedHandler(TcpNetworkAsync a, string msg);
	public delegate void ClientMessageSubmittedHandler(TcpNetworkAsync a, bool close);

	private readonly ManualResetEvent connected = new ManualResetEvent(false);
	private readonly ManualResetEvent sent = new ManualResetEvent(false);
	private readonly ManualResetEvent received = new ManualResetEvent(false);

	public event ConnectedHandler Connected;
	public event ClientMessageReceivedHandler MessageReceived;
	public event ClientMessageSubmittedHandler MessageSubmitted;


	public GameObject m_netObj;

	NetworkStream m_stream = null;
	private Socket m_client;

	private readonly int waitorTimeOut = 30000;

	private string m_ip;
	private int m_port;

	public string IP{
		get{
			return m_ip;
		}
		set{
			m_ip = value;
		}
	}

	public int Port{
		get{
			return m_port;
		}
		set{
			m_port = value;
		}	
	}


	// The response from the remote device.
	private static String response = String.Empty;
	private MessageSheduler sheduler;

	void Awake(){
		sheduler = MessageSheduler.GetInstance ();	
	}

	public static TcpNetworkAsync NewNetWork(string ip, int port, string name){
		GameObject obj = new GameObject ("TcpNetWork_" + name);
		TcpNetworkAsync tcp = obj.AddComponent<TcpNetworkAsync> ();	
		tcp.IP = ip;
		tcp.Port = port;
		tcp.m_netObj = obj;
		DontDestroyOnLoad(obj);
		return tcp;
	}

	public void Connect(){

		//IPHostEntry ipHostInfo = Dns.GetHostEntry(m_ip);
		//ipHostInfo.AddressList[0];

		AddressFamily ipFamily = AddressFamily.InterNetwork;

		try{
			
			if(Application.platform == RuntimePlatform.IPhonePlayer){
				string newIp;
				AddressFamily newFamily;
				Ipv6.getIPType(m_ip, m_port.ToString(), out newIp, out newFamily);

				m_ip = newIp;
				ipFamily = newFamily;
				Debug.Log("iphone ip:" + newIp + " family:" + ipFamily);
			}

			IPAddress ipAddress = IPAddress.Parse(m_ip);
			IPEndPoint remoteEP = new IPEndPoint(ipAddress, m_port);

			m_client = new Socket(ipFamily,
				SocketType.Stream, ProtocolType.Tcp);

			// Connect to the remote endpoint.
			m_client.BeginConnect( remoteEP, 
				new AsyncCallback(ConnectCallback), m_client);
			
			connected.WaitOne();
			if(Connected != null){
				Connected(this);
			}

		}catch(Exception e){
			Debug.LogError("connect failed:" + e.StackTrace);
		}
	}


	public bool IsConnected()
	{
		return !(this.m_client.Poll(1000, SelectMode.SelectRead) && this.m_client.Available == 0);
	}

	private void ConnectCallback(IAsyncResult ar) {
		try {
			// Retrieve the socket from the state object.
			m_client = (Socket) ar.AsyncState;

			// Complete the connection.
			m_client.EndConnect(ar);

			connected.Set();
			Debug.LogError("connection complete");

			ReceiveHead(this.m_client);

		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	private void ReceiveHead(Socket client) {
		try {
			// Create the state object.
			StateObject state = new StateObject(this.m_client);

			// Begin receiving the data from the remote device.
			client.BeginReceive( state.head, 0, StateObject.HEAD_LEN, SocketFlags.None,
				new AsyncCallback(ReceiveHeadCallback), state);
		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	private void ReceiveHeadCallback(IAsyncResult result) {
		try {
			var state = (StateObject)result.AsyncState;
			var receive = state.listener.EndReceive(result);


			if (receive > 0) {
				state.headNum += receive;
				if(state.headNum < state.head.Length){
					state.listener.BeginReceive( state.head, state.headNum, StateObject.HEAD_LEN - state.headNum, SocketFlags.None,
						new AsyncCallback(ReceiveHeadCallback), state);				
				}else{			
		
					int length_tmp = BitConverter.ToInt32(state.head, 0);
					
					int body_len = IPAddress.NetworkToHostOrder(length_tmp);
					state.body = new byte[body_len];
					state.BODY_LEN = body_len;

					state.listener.BeginReceive( state.body, 0, body_len, SocketFlags.None,
						new AsyncCallback(ReceiveBodyCallback), state);	
				}
			} else {
				Debug.LogError("receive length 0");
			}
		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	private void ReceiveBodyCallback(IAsyncResult result) {
		try {
			var state = (StateObject)result.AsyncState;
			var receive = state.listener.EndReceive(result);


			if (receive > 0) {
				state.bodyNum += receive;
				if(state.bodyNum < state.body.Length){
					state.listener.BeginReceive( state.body, state.bodyNum, state.body.Length - state.bodyNum, SocketFlags.None,
						new AsyncCallback(ReceiveBodyCallback), state);				
				}else{			
			
					OnGetData(state.body);

					ReceiveHead(state.listener);
				}
			} else {
				Debug.LogError("receive length 0");
			}
		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	public void SendData(byte[] data){
		this.m_client.BeginSend(data, 0, data.Length, 0,
			new AsyncCallback(SendCallback), this.m_client);
	}

	private void SendCallback(IAsyncResult ar) {
		try {
			// Retrieve the socket from the state object.
			Socket client = (Socket) ar.AsyncState;

			// Complete sending the data to the remote device.
			int bytesSent = client.EndSend(ar);
			Console.WriteLine("Sent {0} bytes to server.", bytesSent);

		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	public bool CanSendData(){
	
		return true;
	}

	private void OnGetData(byte[] data){

		IExtensible message;
		MSGID topKind;
		if (!MessageCoder.DecodeMessage (data, out message, out topKind)) {
			Debug.LogError ("decode failed");
			return;
		}
		/*
		if (topKind == MSGID.Room_Message) {
			pbmsg.room.RoomMessage room = (pbmsg.room.RoomMessage)message;
			if (room != null && room.msgType == MSGID.CSDirectMove_SyncDown) {
				//Debug.LogWarning ("get data, sertime:" + room.syncDown.move.timestamp);
			}
		}*/

		sheduler.Enqueue (message, topKind);

	}


	public void DisConnect()
	{
		try
		{
			if (this.IsConnected())
			{
				this.m_client.Shutdown(SocketShutdown.Both);
				this.m_client.Close();
			}

			GameObject.Destroy(m_netObj);
		}
		catch (SocketException)
		{
			// TODO:
		}
	}


}


//
//public class TcpNetwork : MonoBehaviour {
//
//
//	public bool m_connected = false;
//	public bool m_connectionFailed = false;
//	public GameObject m_netObj;
//
//	private byte[] m_head = new byte[4];
//	private byte[] m_body = null;
//
//	private bool is_sending;
//	public static List<byte[]> wait_for_send = new List<byte[]>();
//
//	TcpClient m_tcp  = null;
//	NetworkStream m_stream = null;
//
//	private readonly int waitorTimeOut = 30000;
//
//	private string m_ip;
//	private int m_port;
//
//	public string IP{
//		get{
//			return m_ip;
//		}
//		set{
//			m_ip = value;
//		}
//	}
//
//	public int Port{
//		get{
//			return m_port;
//		}
//		set{
//			m_port = value;
//		}	
//	}
//	public ManualResetEvent connectDone = new ManualResetEvent(false);
//
//	private MessageSheduler sheduler;
//
//
//
//	void Awake(){
//		sheduler = MessageSheduler.GetInstance ();	
//	}
//
//	public static TcpNetwork NewNetWork(string ip, int port, string name){
//		GameObject obj = new GameObject ("TcpNetWork_" + name);
//		TcpNetwork tcp = obj.AddComponent<TcpNetwork> ();	
//		tcp.IP = ip;
//		tcp.Port = port;
//		tcp.m_netObj = obj;
//		DontDestroyOnLoad(obj);
//		return tcp;
//	}
//
//	public bool CanSendData(){
//		if(m_connected){
//			return true;
//		}
//		return false;	
//	}
//
//	public void OnApplicationQuit(){
//		m_tcp.Close();
//		m_tcp = null;
//	}
//
//	private void ConnectCallback(IAsyncResult ar){
//		
//		connectDone.Set ();
//		TcpClient tcp = (TcpClient)ar.AsyncState;
//		try{
//			if(tcp.Connected){
//				Debug.Log("connected");
//				tcp.EndConnect(ar);
//				Debug.Log ("connect success");
//				m_connected = true;
//			}else{
//				Debug.Log ("connect failed");
//				tcp.EndConnect(ar);
//				m_connectionFailed = true;
//			}
//		}catch(Exception e){
//			Debug.Log ("connection failed: " + e.Message);
//			m_connectionFailed = true;
//		}	
//	}
//
//
//	private void OnGetData(byte[] data){
//
//		IExtensible message;
//		MSGID topKind;
//		if (!MessageCoder.DecodeMessage (data, out message, out topKind)) {
//			Debug.LogError ("decode failed");
//			return;
//		}
//
//		if (topKind == MSGID.Room_Message) {
//			pbmsg.room.RoomMessage room = (pbmsg.room.RoomMessage)message;
//			if (room != null && room.msgType == MSGID.CSDirectMove_SyncDown) {
//				//Debug.LogWarning ("get data, sertime:" + room.syncDown.move.timestamp);
//			}
//		}
//
//		sheduler.Enqueue (message, topKind);
//
//	}
//
//	private void TcpReadCallback(IAsyncResult ar){
//		StateObject state = (StateObject)ar.AsyncState;
//
//		if ((state.client == null) || (!state.client.Connected)) {
//			return;
//		}
//
//		int numToRead;
//		NetworkStream mas = state.client.GetStream ();
//
//		numToRead = mas.EndRead (ar);
//		state.totalBytesRead += numToRead;
//
//		if (numToRead > 0) {
//			byte[] dd = new byte[numToRead];
//			Array.Copy(state.buffer, 0, dd, 0, numToRead);
//			OnGetData(dd);
//			mas.BeginRead(state.buffer, 0, StateObject.BufferSize, new AsyncCallback(TcpReadCallback), state);
//		
//		} else {
//
//			mas.Close();
//			state.client.Close();
//			mas = null;
//			state = null;
//		}
//	
//	}
//
//	void StartReadHead(){
//		StateObject state = new StateObject ();
//		state.client = m_tcp;
//		m_tcp.Client.BeginReceive(m_head, 0, m_head.Length, 0, new AsyncCallback(OnReadHead), state);
//		Debug.LogError("Read Head");
//	}
//
//	void OnReadHead(IAsyncResult ar){
//
//		//Debug.Log("read new head");
//		StateObject state = (StateObject)ar.AsyncState;
//		
//		if ((state.client == null) || (!state.client.Connected)) {
//			return;
//		}
//
//		try{
//
//			int len = state.client.Client.EndReceive(ar);
//
//			if(len != m_head.Length){
//				Debug.LogError("length not matched " + len );
//			}
//
//			int length_tmp = BitConverter.ToInt32(m_head, 0);
//			
//			int body_len = IPAddress.NetworkToHostOrder(length_tmp);
//			m_body = new byte[body_len];
//			StateObject stateBody = new StateObject ();
//			stateBody.client = m_tcp;
//			stateBody.bodyLength = body_len;
//			/*
//			string ss = "";
//			for(int ii = 0; ii < m_head.Length; ii++){
//				ss = ss + " " + ((sbyte)m_head[ii]).ToString(); 
//			}
//
//			Debug.LogError("head:" + ss);*/
//
//
//			state.client.Client.BeginReceive(m_body, 0, body_len, 0, new AsyncCallback(OnReadBody), stateBody);
//
//
//		}catch(Exception e){
//			Debug.Log ("read head .. exception:" + e.Message);
//		}
//		
//	}
//
//	public static Int64 m_testRecvPackNum = 0;
//
//	void OnReadBody(IAsyncResult ar){
//		StateObject state = (StateObject)ar.AsyncState;
//
//		try{
//
//			int len = state.client.Client.EndReceive(ar);
//			state.bodyReadLength += len;
//			if(state.bodyReadLength < state.bodyLength){
//				Debug.LogError("length less, bodyReadLen:" + state.bodyReadLength + " bodyLen:" + state.bodyLength);
//				state.client.Client.BeginReceive(m_body, state.bodyReadLength, state.bodyLength - state.bodyReadLength, 0, new AsyncCallback(OnReadBody), state);
//				return;		
//			}
//			Debug.LogError("xxxxx bodyReadLen:" + state.bodyReadLength + " bodyLen:" + state.bodyLength);
//			/*
//			string ss = "";
//			for(int ii = 0; ii < m_body.Length; ii++){
//				ss = ss + " " + Convert.ToString( m_body[ii], 16);
//			}
//
//			Debug.LogError("bodyLen:" + state.bodyLength + " readLen:" + state.bodyReadLength + " body:" + ss);*/
//
//
//			StartReadHead();
//		}catch(Exception e){
//			Debug.LogError("read body failed" + e.Message);
//		}
//
//		if(state.bodyLength != state.bodyReadLength){
//			Debug.LogError("len not matched, len:" + state.bodyLength + " bodyReadLen:" + state.bodyReadLength);
//			return;
//		}
//
//		m_testRecvPackNum += 1;
//		IExtensible message;
//		MSGID topKind;
//		if (!MessageCoder.DecodeMessage (m_body, out message, out topKind)) {
//			Debug.LogError ("decode failed");
//			return;
//		}
//		/*
//		if (topKind == MSGID.Room_Message) {
//			pbmsg.room.RoomMessage room = (pbmsg.room.RoomMessage)message;
//			if (room != null && room.msgType == MSGID.CSDirectMove_SyncDown) {
//
//				foreach (var move in room.syncDown.move.move) {
//					if (UserBasicDataManager.GetInstance ().GetData ().id == move.uid) {
//						Debug.LogWarning("earliest down:" + move.units [0].v.x + ", " + move.units [0].v.y + " sertime:" + room.syncDown.move.timestamp);
//
//					}
//				}
//			}
//		}*/
//
//		OnGetData(m_body);
//	}
//
//
//
//	private void AsyncRead(TcpClient sock){
//
//		StateObject state = new StateObject ();
//		state.client = sock;
//		NetworkStream stream = sock.GetStream ();
//
//		if (stream.CanRead) {
//			try{
//				IAsyncResult ar = stream.BeginRead(state.buffer, 0, StateObject.BufferSize, new AsyncCallback(TcpReadCallback), state);				
//			}catch(Exception e){			
//				Debug.Log ("IO failed " + e.ToString());			
//			}		
//		}
//	}
//
//	/*
//	public void SendData(byte[] data){
//	
//		if (m_stream != null) {
//			//byte[] data;
//
//			//data = Encoding.ASCII.GetBytes(
//			m_stream.Write(data, 0, data.Length);
//		
//		}	
//	}*/
//
//	void OnSendComplete(IAsyncResult ar){
//		StateObject state = (StateObject)ar.AsyncState;
//		
//		if ((state.client == null) || (!state.client.Connected)) {
//			return;
//		}
//		
//		int numToRead;
//		NetworkStream mas = state.client.GetStream ();
//
//		int len = state.client.Client.EndSend(ar);
//		if(len < state.sendBuffer.Length){
//		
//			byte[] rest_data = new byte[state.sendBuffer.Length - len];
//			Array.Copy(state.sendBuffer, len, rest_data, 0, state.sendBuffer.Length - len);
//			SendData(rest_data);
//			return;
//		}else{
//			is_sending = false;
//			lock(wait_for_send){
//				if(wait_for_send.Count > 0){
//					SendData(wait_for_send[0]);
//					wait_for_send.RemoveAt(0);
//				}else{
//
//				}
//			
//			}
//		
//		}
//	}
//
//
//	public void AsyncSend(byte[] data){
//		StateObject state = new StateObject ();
//		state.client = m_tcp;
//		state.sendBuffer = data;
//
//		if(m_tcp != null && m_tcp.Connected){
//			try{
//				IAsyncResult ar = m_stream.BeginWrite(data, 0, data.Length, new AsyncCallback(OnSendComplete), state);
//			}catch(Exception e){
//				Debug.Log ("IO write failed " + e.ToString());
//			}
//		}
//
//	}
//
//	public static Int64 m_testCounter = 0;
//	public static Int64 m_prevTestTime = 0;
//
//	public void SendData(byte[] data){
//		lock(wait_for_send){
//			if(is_sending){
//				wait_for_send.Add (data);
//			}else{
//				//Debug.LogWarning ("send data: " + m_testCounter + " time:" + HTime.NowTimeMs() + " delta:" + (HTime.NowTimeMs() - m_prevTestTime) + " wait:" + wait_for_send.Count);
//				m_testCounter++;
//				m_prevTestTime = HTime.NowTimeMs ();
//				is_sending = true;
//				AsyncSend(data);			
//			}
//		}
//	}
//
//
//	public bool IsConnected(){
//		if ((m_tcp != null) && (m_tcp.Connected)){
//			return true;	
//		}
//		return false;	
//	}
//
//
//	public void DisConnect(){
//	
//		if ((m_tcp != null) && (m_tcp.Connected)) {
//			m_stream.Close ();
//			m_tcp.Close ();
//			m_connected = false;
//		}	
//
//		if(!m_netObj){
//			GameObject.Destroy(m_netObj);
//		}
//
//	}
//
//	public void Connect(){
//
//		if ((m_tcp == null) || (!m_tcp.Connected)) {
//			try{
//				m_tcp = new TcpClient();
//				m_tcp.ReceiveTimeout = 10;
//
//				connectDone.Reset();
//
//				m_tcp.BeginConnect(m_ip,  m_port, new AsyncCallback(ConnectCallback), m_tcp);
//			
//				connectDone.WaitOne();
//
//				if((m_tcp != null) && (m_tcp.Connected)){
//					m_stream = m_tcp.GetStream();
//					Debug.Log ("connection created");		
//					//AsyncRead(m_tcp);
//					
//					StartReadHead();
//				}
//			
//			}catch(Exception e){
//				Debug.Log (e.Message + " conn ... " + Environment.NewLine);			
//			}	
//		}	
//	}
//
//
//
//
//	internal class StateObject{
//	
//		public TcpClient client = null;
//		public int totalBytesRead = 0;
//		public const int BufferSize = 1024;
//		public string readType = null;
//		public int bodyLength = 0;
//		public int bodyReadLength = 0;
//		public byte[] buffer = new byte[BufferSize];
//		public byte[] sendBuffer = null;
//		public StringBuilder messageBuffer = new StringBuilder();
//	
//	
//	}
//



//}










