﻿using UnityEngine;
using System.Collections;
using ProtoBuf;
using pbmsg;

public class MessageHandler{

	public delegate bool MessageCallback(IExtensible msg);

	protected MSGID mSendApi;
	protected MSGID mRecvApi;
	protected ServerType m_serverType;

	protected IExtensible m_sendMessage;

	protected MessageCallback m_callback;

	public MSGID SendApi{
		get{
			return mSendApi;
		}
	}

	public MSGID RecvApi{
		get{
			return mRecvApi;
		}
	}



	public MessageHandler(ServerType serverType, MSGID sendApi, MSGID recvApi){
		this.mSendApi = sendApi;
		this.mRecvApi = recvApi;
		m_serverType = serverType;
	}


	protected void MessageCallbackBase(){	
	}

	public virtual void MakeMessageMeta(){
	}

	public void Send(){
		MakeMessageMeta();
		MessageSheduler.GetInstance().SendNetMessage(m_serverType, m_sendMessage);
	}
	public bool Send(MessageCallback callback){
		MakeMessageMeta();
		m_callback = callback;
		MessageSheduler.GetInstance().SendNetMessage (m_serverType, this, m_sendMessage);	
		return true;
	}

	public void WaitCallback(IExtensible message){
		if(m_callback == null){
			Debug.LogError("no m_callback");
			return;
		}
		m_callback(message);
	}

	public virtual void NormalCallback(IExtensible message){}


}






