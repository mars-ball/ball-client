﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System;
using System.Text;
using ProtoBuf;
using TcpNetwork=TcpNetworkAsync;
//using TcpNetwork=TcpNetworkSync;

public enum ServerType{
	Invalid = 0,
	LoginServer = 1,
	BattleServer = 2,
	RouterServer = 3,
}



public class NetworkManager : MonoBehaviour {
	public TcpNetwork m_routerServer;
	public  TcpNetwork m_loginServer;
	public TcpNetwork m_battleServer;
	private  static NetworkManager m_instance;
	private readonly float TIME_OUT_MS = 100000;
	//static string ip = "";
	//int port = 0;


	private MessageSheduler m_shedular;


	void Awake(){
	}


	public void SetupLoginServer(string ip_, int port_){
		if(m_loginServer != null){
			if(m_loginServer.IP == ip_ && m_loginServer.Port == port_){
				return;
			}else{
				m_loginServer.DisConnect();
			}
		}

		m_loginServer = TcpNetwork.NewNetWork(ip_, port_, "login");
		m_loginServer.Connect();
	}

	public void SetupBattleServer(string ip_, int port_){
		m_battleServer = TcpNetwork.NewNetWork (ip_, port_, "battle");
		m_battleServer.Connect ();
	}

	public void SetupRouterServer(string ip_, int port_){
		m_routerServer = TcpNetwork.NewNetWork (ip_, port_, "router");
		m_routerServer.Connect ();
	}


	public static NetworkManager GetInstance(){
		if (m_instance == null) {
			
			GameObject obj = new GameObject();
			obj.name = "NetworkManager";
			m_instance = obj.AddComponent<NetworkManager>();
			DontDestroyOnLoad(m_instance);
		}
		return m_instance;	
	}

	public TcpNetwork GetServerByType(ServerType serverType){
	
		switch (serverType) {
		case ServerType.LoginServer:
			return m_loginServer;

		case ServerType.RouterServer:
			return m_routerServer;

		case ServerType.BattleServer:
			return m_battleServer;
		
		default:
			return null;
		}
	
	}

	private IEnumerator SendMessageCoroutine(ServerType serverType, IExtensible msg){
		pbmsg.MSGID msgId;
		if (!MessageCoder.GetTopKindFromServerType (serverType, out msgId)) {
			Debug.LogError ("message coder");
			yield break;
		}

		byte[] coded = MessageCoder.CodeMessage (msg, msgId);
		TcpNetwork server = GetServerByType(serverType);

		if(server == null){
			Debug.Log("server not yet ready:"+ serverType);
			yield break;
		}

		float timeout = TIME_OUT_MS;

		while(!server.CanSendData()){
			timeout -= Time.deltaTime;
			if(timeout < 0){
				
				yield break;
			}
			yield return null;
		}
		server.SendData (coded);
	
	}

	public int SendMessage(ServerType serverType, IExtensible msg){
		
		StartCoroutine(SendMessageCoroutine(serverType, msg));

		return 0;
	
	}




}










