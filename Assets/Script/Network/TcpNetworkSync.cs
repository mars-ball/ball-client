﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;
using ProtoBuf;
using pbmsg;

public class TcpNetworkSync : MonoBehaviour {

	public enum Step{
		INVALID, 
		INIT,
		CONNECTED,
		READ_HEAD,
		READ_BODY,
		DISCONNECTED,
		DESTORYED,
		ERROR
	}


	GameObject m_netObj;

	public Step m_step;

	private byte[] m_head = new byte[4];
	private int m_bodyLen;
	private byte[] m_body = null;

	public static List<byte> m_wait_for_read = new List<byte>();

	TcpClient m_tcp  = null;
	NetworkStream m_stream = null;

	private string m_ip;
	private int m_port;

	public string IP{
		get{
			return m_ip;
		}
		set{
			m_ip = value;
		}
	}

	public int Port{
		get{
			return m_port;
		}
		set{
			m_port = value;
		}	
	}

	private MessageSheduler sheduler;



	void Awake(){
		sheduler = MessageSheduler.GetInstance ();	
		m_step = Step.INIT;
	}

	public static TcpNetworkSync NewNetWork(string ip, int port, string name){
		GameObject obj = new GameObject ("TcpNetWork_" + name);
		TcpNetworkSync tcp = obj.AddComponent<TcpNetworkSync> ();	
		tcp.m_ip = ip;
		tcp.m_port = port;
		tcp.m_netObj = obj;
		DontDestroyOnLoad(obj);



		return tcp;
	}

	public bool CanSendData(){
		if(IsConnected()){
			return true;
		}
		return false;	
	}

	public void OnApplicationQuit(){
		m_tcp.Close();
		m_tcp = null;
	}


	private void OnGetData(byte[] data){

		IExtensible message;
		MSGID topKind;
		if (!MessageCoder.DecodeMessage (data, out message, out topKind)) {
			Debug.LogError ("decode failed");
			ErrorHandler();
			return;
		}

		if (topKind == MSGID.Room_Message) {
			pbmsg.room.RoomMessage room = (pbmsg.room.RoomMessage)message;
			if (room != null && room.msgType == MSGID.CSDirectMove_SyncDown) {
				//Debug.LogWarning ("get data, sertime:" + room.syncDown.move.timestamp);
			}
		}

		sheduler.Enqueue (message, topKind);

	}



	void StartReadHead(){
		m_step = Step.READ_HEAD;

		if(!IsConnected() || !m_stream.CanRead || !m_stream.DataAvailable){
			return;
		}

		if(m_wait_for_read.Count < m_head.Length){
			
			byte[] head = new byte[m_head.Length - m_wait_for_read.Count];
			int headLen = m_stream.Read(head, 0, head.Length);
			m_wait_for_read.AddRange(head);

			if(headLen < head.Length){
				return;
			}
		}

		OnReadHead();

			
	}

	void OnReadHead(){

		if(!IsConnected() || !m_stream.CanRead || !m_stream.DataAvailable){
			return;
		}

		try{

			m_step = Step.READ_BODY;
			Array.Copy(m_wait_for_read.ToArray(), 0, m_head, 0, m_head.Length);

			m_wait_for_read.RemoveRange(0, m_head.Length);

			int length_tmp = BitConverter.ToInt32(m_head, 0);
			
			m_bodyLen = IPAddress.NetworkToHostOrder(length_tmp);

			m_body = new byte[m_bodyLen];

			StartReadBody();


		}catch(Exception e){
			Debug.Log ("read head .. exception:" + e.Message);
			ErrorHandler();
		}
		
	}

	public static Int64 m_testRecvPackNum = 0;
	/*
	public static T[] SubArray<T>(this T[] data, int index, int length)
	{
		T[] result = new T[length];
		Array.Copy(data, index, result, 0, length);
		return result;
	}*/

	void StartReadBody(){
		try{
			if(!IsConnected() || !m_stream.CanRead|| !m_stream.DataAvailable){
				return;
			}


			if(m_wait_for_read.Count < m_bodyLen){
			
				byte[] body = new byte[m_bodyLen - m_wait_for_read.Count];

				int bodyLen = m_stream.Read(body, 0, body.Length);


				if(bodyLen < body.Length){

					byte[] tmp = new byte[bodyLen];
					Array.Copy(body, 0, tmp, 0, bodyLen);

					m_wait_for_read.AddRange(tmp);

					return;
				}else{
					m_wait_for_read.AddRange(body);
				}
								
			}

			if(m_wait_for_read.Count < m_bodyLen){
				Debug.LogError("body len is not enough");
				return;
			}

			Array.Copy(m_wait_for_read.ToArray(), 0, m_body, 0, m_bodyLen);

			m_wait_for_read.RemoveRange(0, m_bodyLen);

			m_step = Step.READ_HEAD;


		}catch(Exception e){
			Debug.LogError("read body failed" + e.Message);
			ErrorHandler();
		}

		/*
		m_testRecvPackNum += 1;
		IExtensible message;
		MSGID topKind;
		if (!MessageCoder.DecodeMessage (m_body, out message, out topKind)) {
			Debug.LogError ("decode failed");
			return;
		}

		if (topKind == MSGID.Room_Message) {
			pbmsg.room.RoomMessage room = (pbmsg.room.RoomMessage)message;
			if (room != null && room.msgType == MSGID.CSDirectMove_SyncDown) {

				foreach (var move in room.syncDown.move.move) {
					if (UserBasicDataManager.GetInstance ().GetData ().id == move.uid) {
						Debug.LogWarning("earliest down:" + move.units [0].v.x + ", " + move.units [0].v.y + " sertime:" + room.syncDown.move.timestamp);

					}
				}
			}
		}*/

		OnGetData(m_body);


	}

	public void SendData(byte[] data){
	
		if (m_stream != null && m_stream.CanWrite && IsConnected()) {
			m_stream.Write(data, 0, data.Length);
		
		}	
	}
		

	public bool IsConnected(){
		if ((m_tcp != null) && (m_tcp.Connected)){
			return true;	
		}
		return false;	
	}


	public void DisConnect(){
	
		if ((m_tcp != null) && (m_tcp.Connected)) {
			m_stream.Close ();
			m_tcp.Close ();
		}	

		m_step = Step.DESTORYED;

		if(!m_netObj){
			GameObject.Destroy(m_netObj);
		}

	}

	public void Connect(){

		if ((m_tcp == null) || (!m_tcp.Connected)) {
			try{
				m_tcp = new TcpClient(m_ip, m_port);

				if(m_tcp == null || !m_tcp.Connected){
					Debug.LogError("tcp client null or not connected");
					return;				
				}

				m_stream = m_tcp.GetStream();

				m_step = Step.CONNECTED;

			}catch(Exception e){
				Debug.Log (e.Message + " conn ... " + Environment.NewLine);			
			}	
		}	
	}

	public void ErrorHandler(){
		m_step = Step.READ_HEAD;
		return;

		Debug.LogError("network error handler");
		byte[] rest = new byte[1024];

		while(IsConnected() && m_stream.CanRead && m_stream.DataAvailable){

			int readLen = m_stream.Read(rest, 0, rest.Length);
		}

	}

	void Update(){


		if(IsConnected()){	
			switch(m_step){
			case Step.CONNECTED:
			case Step.READ_HEAD:
				StartReadHead();
				break;
			case Step.READ_BODY:
				StartReadBody();
				break;

			case Step.DISCONNECTED:
			case Step.DESTORYED:
				break;
			default:
				Debug.LogError("network is error, step:" + m_step);
				break;
			}

		}
	
	
	}




}










