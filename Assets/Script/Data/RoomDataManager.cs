﻿using UnityEngine;
using System.Collections;
using tnt_deploy;

public class RoomDataManager  : DataManagerObject<RoomDataManager, ROOM_INFO, int> {


	public RoomDataManager():base("tnt_deploy_room_info", true){
	}

	public override bool Load(){
		bool ret = LoadTnt<ROOM_INFO_ARRAY>("_roomId");
		if(!ret){
			Debug.LogError("load tnt data failed");
			return false;
		}		
		return true;
	}

}
