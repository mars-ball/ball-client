﻿using UnityEngine;
using System.Collections;
using tnt_deploy;

public class LanguageDataManager  : DataManagerObject<LanguageDataManager, LANGUAGE, string> {


	public LanguageDataManager():base("tnt_deploy_language", true){
	}

	public override bool Load(){
		bool ret = LoadTnt<LANGUAGE_ARRAY>();
		if(!ret){
			Debug.LogError("load tnt data failed");
			return false;
		}		
		return true;
	}

}
