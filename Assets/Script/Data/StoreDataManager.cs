﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using tnt_deploy;


public class StoreDataManager  : DataManagerObject<StoreDataManager, STORE, int> {

	public enum ITEM_TYPE{
		ITEM_TYPE_INVALID = 0,
		ITEM_TYPE_GOLD = 110001,
		ITEM_TYPE_PHOTO = 110002	
	}

	public enum PRICE_TYPE{
		PRICE_TYPE_INVALID = 0,
		PRICE_TYPE_CASH = 101,
		PRICE_TYPE_GOLD = 102	
	}

	public Dictionary<ITEM_TYPE, List<STORE>> m_itemsByType;

	public StoreDataManager():base("tnt_deploy_store", true){
	}

	public override bool Load(){
		bool ret = LoadTnt<STORE_ARRAY>();
		if(!ret){
			Debug.LogError("load tnt data failed");
			return false;
		}		

		m_itemsByType = new Dictionary<ITEM_TYPE, List<STORE>>();
		m_itemsByType[ITEM_TYPE.ITEM_TYPE_GOLD] = new List<STORE>();
		m_itemsByType[ITEM_TYPE.ITEM_TYPE_PHOTO] = new List<STORE>();

		foreach(var itemPair in m_allData){
			STORE item = itemPair.Value;

			switch((ITEM_TYPE)item.type){
			case ITEM_TYPE.ITEM_TYPE_GOLD:
				m_itemsByType[ITEM_TYPE.ITEM_TYPE_GOLD].Add(item);
				break;
			case ITEM_TYPE.ITEM_TYPE_PHOTO:
				m_itemsByType[ITEM_TYPE.ITEM_TYPE_PHOTO].Add(item);
				break;
			default:
				Debug.LogError("type error:" + item.type);
				break;
			}
		}

		return true;



	}


}
