﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class SoldierBornInfo{
	public int m_bornPointNum;
	public List<Vector2> m_bornPoints;
}

public class EnemyBornInfo{
	public int m_bornPointNum;
	public List<Vector2> m_bornPoints;
	public List<int> m_bornIds;
}


public class Stage{
	SoldierBornInfo soldierBornInfo;
	EnemyBornInfo enemyBornInfo;

}


public class RoundData : DataObject {

	int roundId;
	string roundName;

	int m_stageNum;
	Stage[] m_stages;

	public Stage GetStageById (int _id){
		if (_id < 0 || _id >= m_stageNum) {
			Debug.Log("error stage id " + _id);
			return null;		
		}
		return m_stages[_id];
	
	}


}



public class RoundDataManager: ManagerBase<RoundDataManager, RoundData>{

	public RoundDataManager ()
		:base("round"){
	}


}






























