﻿using UnityEngine;
using System.Collections;
using localSave;
using System;

public class UserBasicDataManager : DataManagerObject<UserBasicDataManager, UserBasicData, int> {

	public UserBasicDataManager():base("UserBasicData"){
	}

	public static Int32 GetUserId(){
		return GetInstance().m_aloneData.uid;
	
	}

	public static string GetToken(){
		return GetInstance().m_aloneData.token;
	}

}
