﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BundleLoader: Singleton<PhotoGen>  {
	public enum BUNDLE_TYPE{
		PHOTO = 0,
	}

	public static readonly string[] BUNDLE_FILE_NAMES = {"photos_ios"};

	public const string BUNDLE_EXT_NAME = ".unit3d";

	public const string OUTPUT_ROOT_DIR = "LocalBundle/";

	private static Dictionary<BUNDLE_TYPE, AssetBundle> m_allBundles;

	public static T LoadFromLocalBundle<T>(BUNDLE_TYPE type, String assetName){
		string path = API.AssetRoot + LoginScene.localBundlePath + "/" + BUNDLE_FILE_NAMES[(int)type] + BUNDLE_EXT_NAME;


		if(m_allBundles == null){
			m_allBundles = new Dictionary<BUNDLE_TYPE, AssetBundle>();		
		}

		if (!m_allBundles.ContainsKey(type) || m_allBundles [type] == null) {
			AssetBundle bundle = AssetBundle.LoadFromFile (path);
			m_allBundles [type] = bundle;
		}

		T ass = (T)Convert.ChangeType(m_allBundles[type].LoadAsset(assetName, typeof(T)), typeof(T));

		return ass;

	}


}
