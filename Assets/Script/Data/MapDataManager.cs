﻿using UnityEngine;
using System.Collections;
using localSave;
using System;

public class MapDataManager : DataManagerObject<MapDataManager, MapData, int> {

	public MapDataManager():base("map"){
	}

	public bool LoadByName(string name){
		return LoadLocalDataByFileName(name);
	}

}
