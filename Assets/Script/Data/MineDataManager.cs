﻿using UnityEngine;
using System.Collections;
using tnt_deploy;

public class MineDataManager  : DataManagerObject<MineDataManager, MINE, int> {


	public MineDataManager():base("tnt_deploy_mine", true){
	}

	public override bool Load(){
		bool ret = LoadTnt<MINE_ARRAY>();
		if(!ret){
			Debug.LogError("load tnt data failed");
			return false;
		}		
		return true;
	}

}
