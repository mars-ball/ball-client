﻿using UnityEngine;
using System.Collections;
using tnt_deploy;

public class PhotoDataManager  : DataManagerObject<PhotoDataManager, PHOTO, int> {

	const int PHOTO_TYPE_COLOR = 0;
	const int PHOTO_TYPE_PHOTO = 1;

	public PhotoDataManager():base("tnt_deploy_photo", true){
	}

	public override bool Load(){
		if(m_isDataLoad){
			return true;;
		}
		bool ret = LoadTnt<PHOTO_ARRAY>();
		if(!ret){
			Debug.LogError("load tnt data failed");
			return false;
		}		
		m_isDataLoad = true;
		return true;
	}

	public static bool isColorPhoto(PHOTO photo){
		return photo.photo_type == PHOTO_TYPE_COLOR;			
	}

	public static bool isPhotoPhoto(PHOTO photo){
		return photo.photo_type == PHOTO_TYPE_PHOTO;
	}

}
