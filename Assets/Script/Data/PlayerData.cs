﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using FormationList = System.Collections.Generic.List<PlayerData.Formation>;
using Formation = PlayerData.Formation;

public class PlayerData:IDataObject{

	public Dictionary<int, FormationList> m_allForms;

	public class Formation{
		public int num;
		public List<int> ids;	
	}


	public Formation GetFormationByNum(int num){
		if (m_allForms.ContainsKey (num)) {
			return m_allForms [num] [0];
		} else {
			Debug.Log ("no formation for num " + num);
			return null;
		}
	}


}


public class PlayerDataManager: DataManagerObject<PlayerDataManager, PlayerData, int>{

	public PlayerDataManager():base("PlayerDataManager"){
	}

	public Formation GetFormationByNum(int playerId, int soldierNum){
		PlayerData data = GetDataById (playerId);
		if (data == null) {
			Debug.Log("no player data");
			return null;
		}

		Formation form = data.GetFormationByNum (soldierNum);

		if (form == null) {
				return null;
		}
		return form;
		
	}




}