//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: database/Map.proto
// Note: requires additional types generated from: MoType.proto
namespace localSave
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"HPoint")]
  public partial class HPoint : global::ProtoBuf.IExtensible
  {
    public HPoint() {}
    
    private float _x;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"x", DataFormat = global::ProtoBuf.DataFormat.FixedSize)]
    public float x
    {
      get { return _x; }
      set { _x = value; }
    }
    private float _y;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"y", DataFormat = global::ProtoBuf.DataFormat.FixedSize)]
    public float y
    {
      get { return _y; }
      set { _y = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"Mo")]
  public partial class Mo : global::ProtoBuf.IExtensible
  {
    public Mo() {}
    
    private localSave.HPoint _pos;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"pos", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public localSave.HPoint pos
    {
      get { return _pos; }
      set { _pos = value; }
    }
    private localSave.MoType _type;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"type", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public localSave.MoType type
    {
      get { return _type; }
      set { _type = value; }
    }
    private long _lifetime = default(long);
    [global::ProtoBuf.ProtoMember(3, IsRequired = false, Name=@"lifetime", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(long))]
    public long lifetime
    {
      get { return _lifetime; }
      set { _lifetime = value; }
    }
    private long _recovertime = default(long);
    [global::ProtoBuf.ProtoMember(4, IsRequired = false, Name=@"recovertime", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(long))]
    public long recovertime
    {
      get { return _recovertime; }
      set { _recovertime = value; }
    }
    private string _script = "";
    [global::ProtoBuf.ProtoMember(5, IsRequired = false, Name=@"script", DataFormat = global::ProtoBuf.DataFormat.Default)]
    [global::System.ComponentModel.DefaultValue("")]
    public string script
    {
      get { return _script; }
      set { _script = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"MapData")]
  public partial class MapData : global::ProtoBuf.IExtensible
  {
    public MapData() {}
    
    private int _roomType;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"roomType", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int roomType
    {
      get { return _roomType; }
      set { _roomType = value; }
    }
    private string _mapName;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"mapName", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string mapName
    {
      get { return _mapName; }
      set { _mapName = value; }
    }
    private int _hCellNum;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"hCellNum", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int hCellNum
    {
      get { return _hCellNum; }
      set { _hCellNum = value; }
    }
    private int _vCellNum;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"vCellNum", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int vCellNum
    {
      get { return _vCellNum; }
      set { _vCellNum = value; }
    }
    private int _hCellSize;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"hCellSize", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int hCellSize
    {
      get { return _hCellSize; }
      set { _hCellSize = value; }
    }
    private int _vCellSize;
    [global::ProtoBuf.ProtoMember(6, IsRequired = true, Name=@"vCellSize", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int vCellSize
    {
      get { return _vCellSize; }
      set { _vCellSize = value; }
    }
    private int _vLength;
    [global::ProtoBuf.ProtoMember(7, IsRequired = true, Name=@"vLength", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int vLength
    {
      get { return _vLength; }
      set { _vLength = value; }
    }
    private int _hLength;
    [global::ProtoBuf.ProtoMember(8, IsRequired = true, Name=@"hLength", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int hLength
    {
      get { return _hLength; }
      set { _hLength = value; }
    }
    private int _mineNum = default(int);
    [global::ProtoBuf.ProtoMember(9, IsRequired = false, Name=@"mineNum", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int mineNum
    {
      get { return _mineNum; }
      set { _mineNum = value; }
    }
    private localSave.MineGenerateType _mineGenerateType = localSave.MineGenerateType.MINE_GENERATE_TYPE_INVALID;
    [global::ProtoBuf.ProtoMember(10, IsRequired = false, Name=@"mineGenerateType", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(localSave.MineGenerateType.MINE_GENERATE_TYPE_INVALID)]
    public localSave.MineGenerateType mineGenerateType
    {
      get { return _mineGenerateType; }
      set { _mineGenerateType = value; }
    }
    private int _mineTimeDelta = default(int);
    [global::ProtoBuf.ProtoMember(11, IsRequired = false, Name=@"mineTimeDelta", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    [global::System.ComponentModel.DefaultValue(default(int))]
    public int mineTimeDelta
    {
      get { return _mineTimeDelta; }
      set { _mineTimeDelta = value; }
    }
    private readonly global::System.Collections.Generic.List<localSave.Mo> _mos = new global::System.Collections.Generic.List<localSave.Mo>();
    [global::ProtoBuf.ProtoMember(12, Name=@"mos", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public global::System.Collections.Generic.List<localSave.Mo> mos
    {
      get { return _mos; }
    }
  
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
    [global::ProtoBuf.ProtoContract(Name=@"MineGenerateType")]
    public enum MineGenerateType
    {
            
      [global::ProtoBuf.ProtoEnum(Name=@"MINE_GENERATE_TYPE_INVALID", Value=0)]
      MINE_GENERATE_TYPE_INVALID = 0,
            
      [global::ProtoBuf.ProtoEnum(Name=@"MINE_GENERATE_TYPE_RANDOM", Value=11001)]
      MINE_GENERATE_TYPE_RANDOM = 11001,
            
      [global::ProtoBuf.ProtoEnum(Name=@"MINE_GENERATE_TYPE_FIXED", Value=11002)]
      MINE_GENERATE_TYPE_FIXED = 11002
    }
  
}