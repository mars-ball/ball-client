//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: database/tnt_deploy_room_info.proto
namespace tnt_deploy
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ROOM_INFO")]
  public partial class ROOM_INFO : global::ProtoBuf.IExtensible
  {
    public ROOM_INFO() {}
    
    private int _roomId;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"roomId", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int roomId
    {
      get { return _roomId; }
      set { _roomId = value; }
    }
    private byte[] _name;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"name", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public byte[] name
    {
      get { return _name; }
      set { _name = value; }
    }
    private int _roomType;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"roomType", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int roomType
    {
      get { return _roomType; }
      set { _roomType = value; }
    }
    private int _campNum;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"campNum", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int campNum
    {
      get { return _campNum; }
      set { _campNum = value; }
    }
    private int _maxNum;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"maxNum", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int maxNum
    {
      get { return _maxNum; }
      set { _maxNum = value; }
    }
    private int _successType;
    [global::ProtoBuf.ProtoMember(6, IsRequired = true, Name=@"successType", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int successType
    {
      get { return _successType; }
      set { _successType = value; }
    }
    private uint _requiredLv;
    [global::ProtoBuf.ProtoMember(7, IsRequired = true, Name=@"requiredLv", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public uint requiredLv
    {
      get { return _requiredLv; }
      set { _requiredLv = value; }
    }
    private uint _requiredExp;
    [global::ProtoBuf.ProtoMember(8, IsRequired = true, Name=@"requiredExp", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public uint requiredExp
    {
      get { return _requiredExp; }
      set { _requiredExp = value; }
    }
    private uint _roundTime;
    [global::ProtoBuf.ProtoMember(9, IsRequired = true, Name=@"roundTime", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public uint roundTime
    {
      get { return _roundTime; }
      set { _roundTime = value; }
    }
    private byte[] _mapName;
    [global::ProtoBuf.ProtoMember(10, IsRequired = true, Name=@"mapName", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public byte[] mapName
    {
      get { return _mapName; }
      set { _mapName = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"ROOM_INFO_ARRAY")]
  public partial class ROOM_INFO_ARRAY : global::ProtoBuf.IExtensible
  {
    public ROOM_INFO_ARRAY() {}
    
    private readonly global::System.Collections.Generic.List<tnt_deploy.ROOM_INFO> _items = new global::System.Collections.Generic.List<tnt_deploy.ROOM_INFO>();
    [global::ProtoBuf.ProtoMember(1, Name=@"items", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public global::System.Collections.Generic.List<tnt_deploy.ROOM_INFO> items
    {
      get { return _items; }
    }
  
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
}