﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;

public interface IManagerObject{
}

public interface IDataObject{
}


public class DataManagerObject<TManager, TData, IDType>: IManagerObject
	where TManager: DataManagerObject<TManager, TData, IDType>, new()
{
	const string TNT_PATH = "LocalTntData";

	public string m_managerName;

	public bool m_isDataLoad;

	public Dictionary<IDType, TData> m_allData;

	public TData m_aloneData;

	public static TManager m_instance;

	public bool m_isArray;

	protected DataManagerObject(string name, bool isArray = false){
		this.m_managerName = name;
		m_isArray = isArray;
	}

	public static TManager GetInstance(){
	
		if (m_instance == null) {
			m_instance = new TManager();
		} else {
			return m_instance;
		}	
		return m_instance;
	}

	public bool IsContainsKey(IDType id){
		return m_allData.ContainsKey (id);

	}

	public bool IsLoad(){
	
		return (m_allData != null && m_allData.Count > 0) || (m_aloneData != null);

	}

	public TData GetDataById(IDType id){
		if (m_allData.ContainsKey (id)) {
			return m_allData [id];	
		} else {
			return default(TData);
		}
	}

	public TData GetData(){
		if(m_aloneData == null){
			Debug.Log("data is null");
			return default(TData);
		}
		return m_aloneData;
	}

	public bool SetData(TData data){
		if(data == null){
			Debug.LogError("data empty");
			return false;
		}
		m_aloneData = data;
		return true;
	}

	public void InsertData(IDType id, TData data){
		if (m_allData.ContainsKey (id)) {
			Debug.Log ("already has key");
			return;		
		} else {
			m_allData [id] = data;
		}
		return;
	}	

	public bool LoadTnt<TTData>(string idColumnName = "_id"){
		string path = API.AssetRoot + TNT_PATH + "/";
		string fullpath = path + m_managerName + ".data";

		FileStream fileStream;
		fileStream = new FileStream(fullpath, FileMode.Open);

		bool execRes = false;
		TTData arr;
		if (null != fileStream) {
			arr = Serializer.Deserialize<TTData>(fileStream);
			fileStream.Close();
			execRes = true;
		}else{
			Debug.LogError("load " + m_managerName + " failed");
			return false;
		}

		System.Object obj = arr.GetFieldValue("_items");
		List<TData> dataArr = arr.GetFieldValue("_items") as List<TData>;
		m_allData = new Dictionary<IDType, TData>();

		foreach(TData data in dataArr){

			IDType id;
			if (typeof(IDType) == typeof(string)) {
				byte[] idbytes = (byte[])data.GetFieldValue (idColumnName);
				id = (IDType)((object)(System.Text.Encoding.Default.GetString (idbytes)));
			} else {
				id = (IDType)(data.GetFieldValue(idColumnName));
			}

			InsertData(id, data);
		}

		return true;
	}
		
	public virtual bool LoadLocalDataByFileName(string fileName){
		string path = API.AssetRoot + TNT_PATH + "/";
		string fullpath = path + fileName + ".data";

		FileStream fileStream;
		fileStream = new FileStream(fullpath, FileMode.Open);

		m_aloneData = Serializer.Deserialize<TData>(fileStream);

		if(m_aloneData == null){
			Debug.LogError("read in local data failed");
			return false;		
		}

		fileStream.Close();
		return true;
	}

	public virtual bool Load(){
		string path = Utility.GetCachePath ();
		string fullpath = path + "/" + m_managerName;
		string filename = m_managerName + Utility.LOCAL_SAVE_EXT;

		bool ret = Utility.LoadLocalFile (path, filename, delegate(Stream stream) {
			TData data = Serializer.Deserialize<TData> (stream);
			m_aloneData = data;
			if(data == null){
				return false;
			}
			return true;
		});

		return ret;
	}

	public bool Save(){
		string path = Utility.GetCachePath();
		string fullpath = path + "/" + m_managerName;
		string fileName = m_managerName + Utility.LOCAL_SAVE_EXT;

		MemoryStream mem = new MemoryStream();
		TData data = GetData();

		Serializer.Serialize<TData>(mem, data);

		bool ret = Utility.SaveLocalFile(path, fileName, mem.ToArray(), delegate(){
			Debug.Log ("write " + fileName + "complete");			  
			return true;
		});	
		return true;
	}



}












