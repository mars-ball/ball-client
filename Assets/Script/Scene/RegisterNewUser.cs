﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RegisterNewUser : MonoBehaviour {

	public void OnTapLogInWithExistingAccount() {
		SceneManager.LoadScene("LoginExistingAccount");
	}


	public void OnTapBackToLogin() {
		SceneManager.LoadScene("RegisterAccount");
	}


	public void OnTapRegisterNewAccount() {
		Debug.Log ("======== OnTapRegisterNewAccount");
	}

	public void OnTapLogin() {
		Debug.Log ("======== OnTapLogin");
	}
}
