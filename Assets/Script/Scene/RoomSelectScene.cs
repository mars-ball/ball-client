﻿using UnityEngine;
using System.Collections;
using System;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;

public class RoomSelectScene :  LuaBehaviour  {

	protected string _name = "main_menu";

	private int m_roomId;
	private string m_roomIP;
	private int m_roomPort;

	UserBasicData m_userBasicInfo;

	enum Step{
		Invalid,
		WaitForRoomId,
		EnterRoom,
		EnterWait,
		GoToRoom
	}

	Step m_step;

	void SpEnterRoom(){
		m_step = Step.EnterWait;
		bool ret = new RoomEnterRoomMessage (m_roomId).Send (delegate(IExtensible message) {

			pbmsg.room.RoomMessage msg = (pbmsg.room.RoomMessage)message;
			if (msg.msgType != MSGID.RoomEnterRoom_Response) {
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}

			pbmsg.room.RoomEnterRoomResponse rsp = msg.response.enterRoomResponse;
			if (rsp == null) {
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			Ret roomRet = rsp.ret;
			if (roomRet == Ret.Success) {
				m_step = Step.GoToRoom;
			}
			return true;
		});
	}

	void Update(){
		bool ret = false;

		switch (m_step) {
		case Step.WaitForRoomId:
			break;
		case Step.EnterRoom:
			SpEnterRoom ();
			break;
		case Step.EnterWait:
			break;
		case Step.GoToRoom:
			Application.LoadLevel("Battle");
			break;
		default:
			Debug.LogError ("unkown step:" + m_step);		
			break;
		}
	}

	public void OnClickEnterRoom(){
		bool ret = new EnterRoomMessage(10111).Send(delegate(IExtensible message){
			HMessage msg = (HMessage)message;
			if(msg.msgType != MSGID.RoomEnter_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}
			RoomEnterResponse rsp = msg.response.roomEnter;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			m_roomId = rsp.roomId;
			m_roomIP = rsp.ip;
			m_roomPort = rsp.port;

			NetworkManager.GetInstance().SetupBattleServer(m_roomIP, m_roomPort);

			m_step = Step.EnterRoom;
			return true;

		});


	}

	void Awake(){
		UserInfo info = UserManager.GetInstance().GetMainUser();
		if(info == null){
			Debug.LogError("failed add main user");
			return;
		}

		m_userBasicInfo = info.GetUserBasicInfo();
		m_step = Step.WaitForRoomId;

		API.AddListener("main menu start battle", OnClickEnterRoom);


		DoFile(_name,null); 
	}



}








