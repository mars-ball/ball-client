﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using SLua;

using UnityEngine.Analytics;
using System.Collections.Generic;

public class ConnectionPara{

	public ConnectionPara(string ip_, int port_){
		m_ip = ip_;
		m_port = port_;
	}

	public string m_ip;
	public int m_port;

}



public class LoginScene : LuaBehaviour {

	public UnityADs m_adMgr;

	NetworkManager m_manager;
	public Text m_debug;

	public GameObject mCreateUserPanel;

	public InputField mNameInput;

	UserBasicData m_userBasicInfo;

	protected string _name = "login.lua";

	private bool m_loadFromSave;

	private bool m_routeInit;

	public static LoginScene mInstance;

	public UILoadingControl eLoadingControl;

	public static readonly int FREE_ROOM_TYPE_ID = 10111;

	private int m_roomId;
	private string m_roomIP;
	private int m_roomPort;

	enum Step{
		Invalid,
		CheckExist,
		GetRouteWithoutId,
		GetRouteWithId,
		WaitRoute,
		CreateUser,
		CreateUserReq,
		LoadFromSave,
		WaitLoadFromSave,
		Login,
		WaitCreateUser,
		WaitLogin,
		WaitLoad,
		WaitForRoomId,
		EnterRoom,
		EnterWait,
		GoToRoom,
		Wait,
		Error,
	}

	Step m_step;

	public void Test(){
		Debug.Log ("haha");
	}

	void SpGetRoute(bool hasId, int id = -1){
		m_step = Step.WaitRoute;
		bool ret = new CSRouteMessage(hasId, id).Send(delegate(IExtensible message){
			HMessage msg = (HMessage)message;
			if(msg.msgType != MSGID.CSRoute_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}

			CSRouteResponse rsp = msg.response.route;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			string ip = rsp.ip;
			int port = rsp.port;

			NetworkManager m_manager;
			m_manager = NetworkManager.GetInstance();
			m_manager.SetupLoginServer(ip, port);

			if(hasId){
				m_step = Step.Login;
			}else{
				m_step = Step.CreateUserReq;
			}
			return true;

		});
	}


	void SpCheckUserExist(){
		bool ret = Utility.HasLocalSaveAll();
		
		if(ret){
			m_step = Step.LoadFromSave;
		}else{
			m_step = Step.CreateUser;
		}

		eLoadingControl.SetLoadingPercent (20);

	}

	bool SpLoadFromSave(){
		bool ret = false;
		m_step = Step.WaitLoad;
		ret = UserBasicDataManager.GetInstance().Load();
		if(ret == false){
			Debug.LogError("load from local file failed");
			return false;		
		}

		UserBasicData userData = UserBasicDataManager.GetInstance().GetData();
		if(userData == null){
			Debug.LogError("userData null");
			return false;
		}
		/*
		m_userBasicInfo.mUUid = userData.uuid;
		m_userBasicInfo.mUserId = userData.id;
		m_userBasicInfo.mToken = userData.token;*/

		UserManager.GetInstance().AddUser(userData);
		m_userBasicInfo = UserManager.GetInstance().GetMainUser().mBasicInfo;

		return true;
	}
		

	public bool CreateUserReq(){
		string name = "New Warrior";
		m_step = Step.WaitCreateUser;
		bool ret = new CreateUserMessage(name).Send(delegate(IExtensible message){
			HMessage msg = (HMessage)message;
			if(msg.msgType != MSGID.CreateUser_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}
			CreateUserResponse rsp = msg.response.createUser;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}
			int uid = rsp.id;
			string token = rsp.token;
			UserBasicData userData = new UserBasicData(); 
			userData.uid = uid;
			userData.token = token;
			userData.photo = rsp.photo;
			userData.language = UserLanguage.USER_LANGUAGE_INVALID;
			UserBasicDataManager.GetInstance().SetData(userData);
			UserBasicDataManager.GetInstance().Save();

			mCreateUserPanel.SetActive(false);

			m_step = Step.LoadFromSave;
			eLoadingControl.SetLoadingPercent (60);
			return true;

		});
		return ret;
	}


	bool SpCreateUser(){
		m_step = Step.GetRouteWithoutId;
		//mCreateUserPanel.SetActive(true);

		return true;
	}

	bool SpLogin(){

		Analytics.CustomEvent("gameOver", new Dictionary<string, object>
			{
				{ "login-time",  DateTime.Now.ToString("MM/dd/yyyy h:mm tt")}
			});
		
		m_step = Step.WaitLogin;

		bool ret = new LoginMessage().Send (delegate(IExtensible msg) {
			HMessage message = (HMessage)msg;

			LoginResponse response = message.response.login;
			if(response == null){
				Debug.LogError("response null , recv id :"+ (int)message.msgType);
				return false;			
			}

			if(message.response.ret == 0){

				pbmsg.common.UserBasicInfo userInfo = response.info;
				UserInfo info = UserManager.GetInstance().GetMainUser();
				info.mBasicInfo.uid = UserBasicDataManager.GetUserId();
				info.mBasicInfo.name = userInfo.name;
				info.mBasicInfo.photo = userInfo.photo;
				info.mBasicInfo.lv = userInfo.lv;
				info.mBasicInfo.gold = userInfo.gold;
				info.mHasNotif = (response.hasNotif > 0);

				PhotoGen.GetInstance().LoadAllPhoto();
				Texture2D photoTex = PhotoGen.GetInstance().getPhotoTexture(userInfo.photo);

				SetEnv("user_photo", photoTex, true);
				SetEnv("user_name", userInfo.name, true);
				SetEnv("user_lv", userInfo.lv.ToString(), true);
				SetEnv("user_gold", userInfo.gold.ToString(), true);
				SetEnv("user_has_notif", info.mHasNotif, true);

				API.Broadcast("login draw user info");

			
			}else{
				Debug.LogError("login failed");			
			}

			UserManager.GetInstance().GetMainUser().mAppStatus = UserInfo.AppStatus.LOGIN_IN;
			m_step = Step.WaitForRoomId;
			eLoadingControl.SetLoadingPercent (100);

			if(UserManager.GetInstance().GetMainUser().GetUserBasicInfo().language == UserLanguage.USER_LANGUAGE_INVALID){
				SceneManager.LoadScene("LanguageSelect");
			
			}else{
				LanguageDataManager.GetInstance ().Load ();
			
			}



			return true;
		});
		return ret;
	
	}


	void SpEnterRoom(){
		m_step = Step.EnterWait;

		bool ret = new RoomEnterRoomMessage (m_roomId).Send (delegate(IExtensible message) {

			pbmsg.room.RoomMessage msg = (pbmsg.room.RoomMessage)message;
			if (msg.msgType != MSGID.RoomEnterRoom_Response) {
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}

			pbmsg.room.RoomEnterRoomResponse rsp = msg.response.enterRoomResponse;
			if (rsp == null) {
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			Ret roomRet = rsp.ret;
			if (roomRet == Ret.Success) {
				eLoadingControl.SetLoadingPercent(80);

				m_step = Step.GoToRoom;
			}

			return true;
		});
	}

	void Update(){
		if(m_step == Step.Invalid){
			return;
		}

		bool ret = false;

		switch (m_step) {
		case Step.CheckExist:
			SpCheckUserExist();
			break;		
		case Step.GetRouteWithoutId:
			SpGetRoute(false);
			break;
		case Step.GetRouteWithId:
			if (!m_routeInit) {
				return;
			}
			SpGetRoute(true, m_userBasicInfo.uid);
			break;
		case Step.WaitRoute:
			break;
		case Step.CreateUser:
			ret = SpCreateUser();
			if(ret != true){				//need more
				Debug.LogError("create user failed");
			}
			break;
		case Step.CreateUserReq:
			ret = CreateUserReq();
			if(!ret){
				Debug.LogError("create user req failed");
			}
			break;
		case Step.WaitCreateUser:
			break;
		case Step.LoadFromSave:
			ret = SpLoadFromSave();
			if(!ret){
				m_step = Step.Error;
			}else{
				m_step = Step.GetRouteWithId;
				eLoadingControl.SetLoadingPercent (70);
			}
			break;
		case Step.Login:
			SpLogin ();
			break;
		case Step.WaitLogin:
			break;
		case Step.Wait:
			break;
		case Step.WaitForRoomId:
			break;
		case Step.EnterRoom:
			SpEnterRoom ();
			break;
		case Step.EnterWait:
			break;
		case Step.GoToRoom:
			eLoadingControl.SetLoadingPercent (100);
			Application.LoadLevel("Battle");
			break;
		default:
			Debug.LogError ("unkown step:" + m_step);		
			break;
		}

	}

	void StartLoginConnection(object obj){
		eLoadingControl.StartLoading ();
		LuaTable luaT = (LuaTable)obj;
		string route_ip = (string)luaT["routeIp"];
		double route_port_double = (double)luaT["routePort"];
		int route_port = (int)route_port_double;
		Debug.Log("login ip:" + route_ip + " port:" + route_port);

		//ConnectionPara para = (ConnectionPara)obj;
		NetworkManager m_manager;
		m_manager = NetworkManager.GetInstance();
		m_manager.SetupRouterServer(route_ip, route_port);
		m_routeInit = true;

		m_step = Step.CheckExist;
		/*UserInfo info = UserManager.GetInstance().AddUserSimple(0);
		if(info == null){
			Debug.LogError("failed add user 0");
			//return false;
		}
		m_userBasicInfo = info.GetUserBasicInfo();*/
		//return true;
	}



	void Awake(){

		mInstance = this;

		Debug.LogError ("ver.2");

		bool ret = Utility.HasLocalSaveAll();
		if (!ret) {
			
		}

		StartCoroutine(InitLua());
	
		m_adMgr = new UnityADs ();

		IAP.getInstance ().Init ();

		SoundManager.GetInstance ().PlayMenuMusic();
	}


	void OnDestroy(){
		API.RemoveListener2("login start login server", StartLoginConnection);
		API.RemoveListener2 ("login change name", OnBindChangeName);
		API.RemoveListener("main menu start battle", OnClickEnterRoom);
		API.RemoveListener("play ad", OnClickPlayAd);
		API.RemoveListener("login mypage", OnClickMyPage);
		API.Broadcast ("login reset");
	}

	public IEnumerator InitLua(){
		yield return StartCoroutine(InitLuaEnvVariable());

		UserInfo mainUser = UserManager.GetInstance().GetMainUser();

		API.AddListener2("login start login server", StartLoginConnection);
		API.AddListener2 ("login change name", OnBindChangeName);
		API.AddListener("main menu start battle", OnClickEnterRoom);
		API.AddListener ("play ad", OnClickPlayAd);
		API.AddListener("login mypage", OnClickMyPage);

		if(mainUser != null && !mainUser.isUserFirstInGame()){
			SetEnv("first_execute", false, true);

			Texture2D photoTex = PhotoGen.GetInstance().getPhotoTexture(mainUser.mBasicInfo.photo);

			SetEnv("user_photo", photoTex, true);
			SetEnv("user_name", mainUser.mBasicInfo.name, true);
			SetEnv("user_lv", mainUser.mBasicInfo.lv.ToString(), true);
			SetEnv("user_gold", mainUser.mBasicInfo.gold.ToString(), true);
			SetEnv("user_has_notif", mainUser.mHasNotif, true);

			DoFile(_name,null); 
		}else{

			SetEnv("first_execute", true, true);
			InitAsstes(); 
		}
	
	}

	public IEnumerator InitLuaEnvVariable(){
		yield return new WaitForEndOfFrame();
		while(env==null || !env.isReady)
		{
			yield return new WaitForEndOfFrame();
		} 
	}

	void Start(){

		UserInfo mainUser = UserManager.GetInstance().GetMainUser();
		if (mainUser != null && !mainUser.isUserFirstInGame ()) {
			if (mainUser.mWaitForAd <= 0) {
				m_adMgr.ShowAd ();
				mainUser.mWaitForAd = UserInfo.WAIT_FOR_AD_CNT;
			}
		}

		if (UserManager.GetInstance ().GetMainUser () != null && UserManager.GetInstance ().GetMainUser ().mAppStatus == UserInfo.AppStatus.FOLLOW_FRIEND_BATTLE) {
			OnClickEnterRoom ();
		} else {
			m_step = Step.Wait;
			//eLoadingControl.gameObject.SetActive (true);
		}
	}


	public void LogLogin(){
		Debug.LogError("login function");
	
	}
		
	public void OnClickPlayAd(){
		m_adMgr.ShowRewardedAd ();
	
	}

	public void OnClickMyPage(){
	
		if (String.IsNullOrEmpty (UserManager.GetInstance ().GetMainUser ().mBasicInfo.mail)) {
			SceneManager.LoadScene ("RegisterAccount");
		} else {
			SceneManager.LoadScene ("MyPage");					
		}
	
	}

	public void OnBindChangeName(object obj){
		LuaTable luaT = (LuaTable)obj;
		InputField input = (InputField)luaT["input"];
		input.onEndEdit.AddListener (delegate {
			OnInputNameEnd (input);
		});
	}

	public void OnInputNameEnd(InputField input){
		string name = input.text;

		CSChangeInfoMessage.ChangeParam param = new CSChangeInfoMessage.ChangeParam ();
		param.name = name;

		UserManager.GetInstance ().GetMainUser ().mBasicInfo.name = name;

		new CSChangeInfoMessage (CSChangeInfoMessage.ChangeInfoType.CHANGE_INFO_TYPE_NAME, param).Send (
			delegate(IExtensible message){
				HMessage msg = (HMessage)message;
				if(msg.msgType != MSGID.CSChangeInfo_Response){
					Debug.LogError ("msg id error " + msg.msgType);
					return false;
				}

				CSChangeInfoResponse rsp = msg.response.changeInfo;
				if(rsp == null){
					Debug.LogError ("create user rsp null ");
					return false;			
				}
				if(rsp.ret == Ret.Success){
					Debug.LogError("change name success");
				}else{
					Debug.LogError("change name failed");				
				}

				return true;

			}
		);



	}


	public void OnClickEnterRoom(){
		//eLoadingControl.gameObject.SetActive (true);
		eLoadingControl.StartLoading ();

		if(UserManager.GetInstance().GetMainUser().mAppStatus == UserInfo.AppStatus.WAIT_FOR_NEXT_ROUND && NetworkManager.GetInstance().m_battleServer != null
			&& NetworkManager.GetInstance().m_battleServer.IsConnected()){
			SceneManager.LoadScene("Battle");
			return;
		}

		int roomTypeId = -1;
		int roomId = -1;
		int campId = -1;

		if (UserManager.GetInstance ().GetMainUser ().mAppStatus == UserInfo.AppStatus.FOLLOW_FRIEND_BATTLE) {
			roomId = UserManager.GetInstance ().GetMainUser ().mGamingInfo.mRoomId;
			campId = UserManager.GetInstance ().GetMainUser ().mGamingInfo.mCampId;
		} else {
			roomTypeId = FREE_ROOM_TYPE_ID;
		}


		bool ret = new EnterRoomMessage(roomTypeId, roomId, campId).Send(delegate(IExtensible message){
			HMessage msg = (HMessage)message;
			if(msg.msgType != MSGID.RoomEnter_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}
			RoomEnterResponse rsp = msg.response.roomEnter;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			m_roomId = rsp.roomId;
			m_roomIP = rsp.ip;
			m_roomPort = rsp.port;

			if (NetworkManager.GetInstance ().m_battleServer != null && (NetworkManager.GetInstance ().m_battleServer.IP != m_roomIP || NetworkManager.GetInstance ().m_battleServer.Port != m_roomPort)) {

				NetworkManager.GetInstance ().m_battleServer.DisConnect ();
				NetworkManager.GetInstance ().m_battleServer = null;

			}

			if(NetworkManager.GetInstance ().m_battleServer == null || !NetworkManager.GetInstance ().m_battleServer.IsConnected()){
				NetworkManager.GetInstance().SetupBattleServer(m_roomIP, m_roomPort);
			}

			UserManager.GetInstance().GetMainUser().mGamingInfo.mRoomTypeId = FREE_ROOM_TYPE_ID;
			UserManager.GetInstance().GetMainUser().mGamingInfo.mRoomId = m_roomId;


			eLoadingControl.SetLoadingPercent(50);
			m_step = Step.EnterRoom;
			return true;

		});
	}


	public void Restart(){
		StartCoroutine (onReStart ());
	}

	void InitAsstes()
	{       
		string mainfile =API.AssetRoot+ "lua/"+_name;
		string assetFolder = API.AssetRoot + "/asset/";
		//如果入口主main.lua未找到       
		if (!Directory.Exists(assetFolder) || !File.Exists(mainfile) || !IsLocalDataComplete())
		{
			//解压主资源文件
			StartCoroutine(loadStreamingAssets());    

		}
		else
		{
			this.gameObject.AddComponent<InitLua>();
			DoFile(_name,null); 
		}

	}

	public static void ReLogin(){

		UserManager.GetInstance ().GetMainUser ().mAppStatus = UserInfo.AppStatus.START_UP;
		if (NetworkManager.GetInstance ().m_routerServer) {
			NetworkManager.GetInstance ().m_routerServer.DisConnect ();
		}

		if (NetworkManager.GetInstance ().m_battleServer) {
			NetworkManager.GetInstance ().m_battleServer.DisConnect ();
		}

		if (NetworkManager.GetInstance ().m_loginServer) {
			NetworkManager.GetInstance ().m_loginServer.DisConnect ();
		}

		API.CleanLuaEnv();

		SceneManager.LoadScene ("Login");

	
	
	}


	IEnumerator onReStart()
	{

		yield return new WaitForEndOfFrame();
		if (table != null)
		{
			table.Dispose();
		}
		yield return new WaitForEndOfFrame();
		
		API.CleanLuaEnv();
		yield return new WaitForEndOfFrame();
		InitAsstes();
	}

	private static readonly string[] localDatas = new string[]{"tnt_deploy_drop.data", "tnt_deploy_mine.data", "tnt_deploy_photo.data", 
										"tnt_deploy_room_info.data", "normal_map.data", "tnt_deploy_store.data", "tnt_deploy_language.data"
										,"photos_ios.unit3d"};
	public static readonly string localDataPath = "LocalTntData";
	public static readonly string localBundlePath = "LocalBundle";

	string getLocalDataPath(string fileName){

		if (Path.GetExtension (fileName) == ".data") {
			return API.AssetRoot + localDataPath + "/" + fileName;		
		} else if (Path.GetExtension (fileName) == ".unit3d") {
			return API.AssetRoot + localBundlePath + "/" + fileName;		
		}

		return API.AssetRoot + fileName;
	}

	string getLocalDataDir(string fileName){
		Debug.LogError ("ext:" + Path.GetExtension (fileName));
		if (Path.GetExtension (fileName) == ".data") {
			return API.AssetRoot + localDataPath;		
		} else if (Path.GetExtension (fileName) == ".unit3d") {
			return API.AssetRoot + localBundlePath;		
		}
		return API.AssetRoot;
	}

	bool IsLocalDataComplete(){
		foreach(string file in localDatas){
			string filename = getLocalDataPath(file); 
			if(!File.Exists(filename)){
				return false;			
			}		
		}
		return true;
	}

	IEnumerator loadlocalDatabase(){


		foreach(string file in localDatas){
			Debug.Log("load file:"+ file);
			string sorucefilename = file;
			string filename = getLocalDataPath(file); 
			string path = getLocalDataDir (file);

			if(!Directory.Exists(path)){
				Directory.CreateDirectory(path);
			}
			string log = "";       

			byte[] bytes = null;
			#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX

			string sourcepath = "file:///" + Application.streamingAssetsPath + "/" + sorucefilename;
			log += "asset path is: " + sourcepath;
			WWW www = new WWW(sourcepath);
			yield return www;
			if (www.error != null)
			{
				Debug.Log("Warning errow: " + "loadStreamingAssets");
				yield break;
			}
			bytes = www.bytes;

			#elif UNITY_WEBPLAYER 
			string sourcepath = "StreamingAssets/" + sorucefilename;        log += "asset path is: " + sourcepath; 
			WWW www = new WWW(sourcepath); 			
			yield return www;
			if (www.error != null)
			{           
			Debug.Log("Warning errow: " + "loadStreamingAssets");
			yield break;
			}
			bytes = www.bytes; 
			#elif UNITY_IPHONE 
			string sourcepath = Application.dataPath + "/Raw/" + sorucefilename;     log += "asset path is: " + sourcepath;     
			try{ 
			using ( FileStream fs = new FileStream(sourcepath, FileMode.Open, FileAccess.Read, FileShare.Read) ){ 
			bytes = new byte[fs.Length]; 
			fs.Read(bytes,0,(int)fs.Length); 
			}   
			} catch (System.Exception e){ 
			log +=  "\nTest Fail with Exception " + e.ToString(); 
			log +=  "\n"; 
			yield break;
			} 
			#elif UNITY_ANDROID 
			string sourcepath = "jar:file://" + Application.dataPath + "!/assets/"+sorucefilename; 			
			//NGUIDebug.Log("文件路径为：" + sourcepath); 
			log += "asset path is: " + sourcepath; 
			WWW www = new WWW(sourcepath); 
			yield return www;
			if (www.error != null)
			{           
			Debug.Log("Warning errow: " + "loadStreamingAssets");
			yield break;
			}
			bytes = www.bytes; 
			//Debug.Log("字节长度为：" + bytes.Length); 
			#endif


			using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
			{
				fs.Write(bytes, 0, bytes.Length);
				log += "\nCopy res form streaminAssets to persistentDataPath: " + filename;
				fs.Close();
			}
			Debug.Log(log);
		}	
	}

	IEnumerator loadStreamingAssets()
	{
		yield return StartCoroutine(loadlocalDatabase());

		string sorucefilename = "data.zip";
		string filename = API.AssetRoot + sorucefilename; 
		string log = "";       

		byte[] bytes = null;
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX

		string sourcepath = "file:///" + Application.streamingAssetsPath + "/" + sorucefilename;
		log += "asset path is: " + sourcepath;
		WWW www = new WWW(sourcepath);
		yield return www;
		if (www.error != null)
		{
			Debug.Log("Warning errow: " + "loadStreamingAssets");
			yield break;
		}
		bytes = www.bytes;

		#elif UNITY_WEBPLAYER 
		string sourcepath = "StreamingAssets/" + sorucefilename;        log += "asset path is: " + sourcepath; 
		WWW www = new WWW(sourcepath); 			
		yield return www;
		if (www.error != null)
		{           
			Debug.Log("Warning errow: " + "loadStreamingAssets");
			yield break;
		}
		bytes = www.bytes; 
		#elif UNITY_IPHONE 
		string sourcepath = Application.dataPath + "/Raw/" + sorucefilename;     log += "asset path is: " + sourcepath;     
		try{ 
			using ( FileStream fs = new FileStream(sourcepath, FileMode.Open, FileAccess.Read, FileShare.Read) ){ 
				bytes = new byte[fs.Length]; 
				fs.Read(bytes,0,(int)fs.Length); 
			}   
		} catch (System.Exception e){ 
			log +=  "\nTest Fail with Exception " + e.ToString(); 
			log +=  "\n"; 
		} 
		#elif UNITY_ANDROID 
		string sourcepath = "jar:file://" + Application.dataPath + "!/assets/"+sorucefilename; 			
		//NGUIDebug.Log("文件路径为：" + sourcepath); 
		log += "asset path is: " + sourcepath; 
		WWW www = new WWW(sourcepath); 
		yield return www;
		if (www.error != null)
		{           
			Debug.Log("Warning errow: " + "loadStreamingAssets");
			yield break;
		}
		bytes = www.bytes; 
		//Debug.Log("字节长度为：" + bytes.Length); 
		#endif
		if (bytes != null)
		{
			// 
			// 
			// copy zip  file into cache folder 
			using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
			{
				fs.Write(bytes, 0, bytes.Length);
				log += "\nCopy res form streaminAssets to persistentDataPath: " + filename;
				fs.Close();
			}

			//解压缩
			API.UnpackFiles(filename, API.AssetRoot);

			yield return new WaitForEndOfFrame();

			//删除临时zip包
			File.Delete(@filename);

			yield return new WaitForEndOfFrame();

			log += string.Format("\n{0} created!  ", "res");
			Debug.Log(log);

			this.gameObject.AddComponent<InitLua>();
			//加载入口文件 main.lua
			DoFile(_name,null);   
		}
	}

}








