﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using pbmsg;
using pbmsg.login;
using localSave;
using ProtoBuf;

public class LoginAccountScene:MonoBehaviour{

	public InputField eEmailInput;
	public InputField ePasswordInput;
	public Text eWarningText;
	public Text eErrorTextToUser;

	public const int EMAIL_LEN = 50;
	public const int PWD_LEN = 15;

	public void OnClickLogin(){

		string email = eEmailInput.text;
		string password = ePasswordInput.text;
		if (email.Length < 0 || email.Length >= EMAIL_LEN) {
			eWarningText.text = "Input Length error";
		}

		if (password.Length < 0 || password.Length >= PWD_LEN) {
			eWarningText.text = "Password Length error";
		}

		bool ret = new CSLoginMailMessage(email, password).Send (delegate(IExtensible msg) {
			HMessage message = (HMessage)msg;

			CSLoginMailResponse response = message.response.loginMail;
			if(response == null){
				Debug.LogError("response null , recv id :"+ (int)message.msgType);
				return false;			
			}

			if(response.ret == pbmsg.Ret.Success){
				var userBasicData = UserBasicDataManager.GetInstance().GetData();
				userBasicData.uid = response.uid;
				userBasicData.token = response.token;
				UserBasicDataManager.GetInstance().SetData(userBasicData);
				UserBasicDataManager.GetInstance().Save();

				LoginScene.ReLogin();

			}else{
				eErrorTextToUser.text = LanguageText.getStringByID ("STRINGS_LOGIN_FAIL");
				Debug.LogError("login failed");			
			}


			return true;
		});
	}


	public void OnClickBack(){

		SceneManager.LoadScene ("Login");
	}
}
