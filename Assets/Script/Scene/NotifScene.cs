﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using SLua;

public class NotifScene : IDataListCanGetNum{

	public enum NOTIF_TYPE_TO_INDEX{
		NOTIF_TYPE_TO_INDEX_SYSTEM = 0,
		NOTIF_TYPE_TO_INDEX_FRIEND = 1,
		NOTIF_TYPE_TO_INDEX_OTHER = 2,
		NOTIF_TYPE_TO_INDEX_MAX = 3
	}

	public Dictionary<int, List<CSNotifItem>> m_notifByType;

	public UITabPanel m_tabPanel;

	void Awake(){
		m_isDataReady = false;
		m_notifByType = new Dictionary<int, List<CSNotifItem>>();

		for(int ii = 0; ii < (int)NOTIF_TYPE_TO_INDEX.NOTIF_TYPE_TO_INDEX_MAX; ii++){
			m_notifByType[ii] = new List<CSNotifItem>();
		}

		new CSGetNotifListMessage().Send(delegate(IExtensible msg) {

			HMessage message = (HMessage)msg;

			CSGetNotifListResponse response = message.response.notifList;

			if(response == null){
				Debug.LogError("response null , recv id :"+ (int)message.msgType);
				return false;			
			}

			List<CSNotifItem> items = response.items;

			foreach(var item in items){				
				if(item.addFriend != null || item.addFriendSuccess != null){
					m_notifByType[(int)NOTIF_TYPE_TO_INDEX.NOTIF_TYPE_TO_INDEX_FRIEND].Add(item);
				}			
			}

			m_isDataReady = true;

			return true;

		});

	}

	void Start(){
	
	}

	public override void Search(int typeIndex, string str){
	}

	public override int GetDataListNum(int typeIndex){

		return m_notifByType[typeIndex].Count;

	}

	public override void RemoveItem(int typeIndex, int index){
	
		if(m_notifByType.ContainsKey(typeIndex)){
		
			var list = m_notifByType[typeIndex];
			if(index >= 0 && index < list.Count){
				list.RemoveAt(index);			
			}
		
		}

		bool empty = true;

		foreach(var notifPair in m_notifByType){
			if(notifPair.Value.Count > 0){
				empty = false;
				break;
			}		
		}

		if(empty){

			UserManager.GetInstance().GetMainUser().mHasNotif = false;
		
		}

	
	}

	public override System.Object GetDataByIndex(int typeIndex, int index){
		if(typeIndex < 0 || typeIndex >= m_notifByType.Count){		
			Debug.Log("typeindex exceeds border:" + typeIndex);		
		}
		List<CSNotifItem> notifs = m_notifByType[typeIndex];

		if(index < 0 || index >= notifs.Count){
			Debug.LogError("index error:" + index);		
		}

		return notifs[index];

	}

	public void OnClickGoMainMenu(){
		SceneManager.LoadScene("Login");

	}

}
