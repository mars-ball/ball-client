﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using SLua;

public class BattleOverScene : LuaBehaviour {

	protected string _name = "round_over.lua";

	void Awake(){

	}

	void RestartGameRound(){
		SceneManager.LoadScene("Battle");
	}



	public IEnumerator Do(){

		yield return StartCoroutine(InitEnv());

		int score = UserManager.GetInstance().GetMainUser().mGamingInfo.mScore;

		SetEnv("userScore", score.ToString(), true);

		API.AddListener("restart game", RestartGameRound);

		DoFile(_name,null); 

	}

	public IEnumerator InitEnv(){
		yield return new WaitForEndOfFrame();
		while(env==null || !env.isReady)
		{
			yield return new WaitForEndOfFrame();
		} 
	}

}


