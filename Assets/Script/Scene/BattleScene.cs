﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;
using pbmsg;
using pbmsg.room;
using System;
using tnt_deploy;
using localSave;

public class BattleScene : MonoBehaviour {

	private static BattleScene m_instance;
	public Text m_upSpeedTxt;
	public Text m_downSpeedTxt;
	public Text m_scoreText;
	public Transform m_mapBackGround;
	public UIWarningWindow eWarningWindow;

	public RoomDataManager m_roomDataMgr;
	public MapDataManager m_mapDataMgr;

	public GameObject eRoundOverPanel;
	public Text eRoundOverScoreText;

	public static BattleScene GetInstance(){
		if (m_instance == null) {
			Debug.LogError("no awake yet");
			return null;
		}
		return m_instance;	
	}

	public JoyController m_joy;
	public MoveSync m_sync;
	public Step m_step;

	public enum Step{
		INVALID,
		BORN,
		WAIT_BORN,
		INIT_SCENE,
		WAIT_INIT_SCENE,
		PLAY,
		PLAYING,
		DEAD
	}

	public const Int64 JOY_STICK_DELTA_MS = 80;
	public Int64 m_prevJoyTime;
	public bool m_isStart;

	public const float BACKGOURND_GRID_CELL_WIDTH = 100;

	public List<Text> eRankText;

	public class GamePlayerInfo{
		public int uid;
		public string name;
		public int photoId;

		public GamePlayerInfo(int _uid, string _name, int _photoId){
			uid = _uid;
			name = _name;
			photoId = _photoId;
		}
	}

	public Dictionary<int, GamePlayerInfo> m_playerInfos;

	public class MineInfo{
		public int mid;
		public MineType type; 
		public Vector2 pos;
		public int colorId;
		public int index;
	}

	public List<MineInfo> m_mineInfoByIndex;
	public Dictionary<int, MineInfo> m_mineInfoById;
	public float m_mineDetectRadius;


	// Use this for initialization
	void Awake () {
		m_instance = this;
		m_sync = MoveSync.GetInstance ();
		m_sync.m_battleScene = this;

		MessageSheduler.GetInstance ().m_downSpeedTxt = m_downSpeedTxt;
		m_roomDataMgr = RoomDataManager.GetInstance();
		m_mapDataMgr = MapDataManager.GetInstance();
		m_playerInfos = new Dictionary<int, GamePlayerInfo>();

		m_mineInfoById = new Dictionary<int, MineInfo> ();
		m_mineInfoByIndex = new List<MineInfo> ();

		for(int ii = 1; ii < 10; ii++){
			string objPath = "Rank" + ii.ToString() + "Text";
			GameObject obj = GameObject.Find(objPath);
			if(obj == null){
				break;
			}
			Text objText = obj.GetComponent<Text>();
			if(objText == null){
				Debug.LogError("obj text null");
			}
			eRankText.Add(objText);

		}


		m_step = Step.BORN;
		UserManager.GetInstance().GetMainUser().mAppStatus = UserInfo.AppStatus.IN_BATTLE;
		m_isStart = false;

		// Load BGM
		SoundManager.GetInstance ().PlayBattleMusic();
	}

	void Start(){
		m_roomDataMgr.Load();
		if(!DrawBattleMap()){
			Debug.LogError("battle map failed");
		}

		GetMineDetectRadius ();
	}

	public void GameRoundOver(){
		eRoundOverScoreText.text = m_scoreText.text;
		eRoundOverPanel.SetActive(true);
		m_step = Step.DEAD;
		UserManager.GetInstance().GetMainUser().mAppStatus = UserInfo.AppStatus.WAIT_FOR_NEXT_ROUND;

	}

	public void OnClickRoundOver(){
		UserManager.GetInstance ().GetMainUser ().mWaitForAd -= 1;

		MoveSync.GetInstance().Reset();

		SceneManager.LoadScene("Login");	
		m_sync.m_battleScene = null;
		m_sync = null;

	}

	public void SetRankBoard(List<CSDirectMoveSycnDown.RankUnit> units){

		int len = eRankText.Count > units.Count ? units.Count: eRankText.Count;

		for(int ii = 0; ii < len; ii++){
			if(m_playerInfos.ContainsKey(units[ii].uid)){
				var info = m_playerInfos[units[ii].uid];
				eRankText[ii].text = (ii+1).ToString() + "." + info.name;

			}else{
				eRankText[ii].text = (ii+1).ToString() + "." + units[ii].uid.ToString();
			}
		}

		if(len < eRankText.Count){
			for(int ii = len; ii < eRankText.Count; ii++){
				eRankText[ii].text = "";			
			}		
		}

	}


	bool DrawBattleMap(){
		int roomType = UserManager.GetInstance().GetMainUser().mGamingInfo.mRoomTypeId;
		ROOM_INFO roomInfo = m_roomDataMgr.GetDataById(roomType);
		if(roomInfo == null){
			Debug.LogError("room info is null");
			return false;
		}

		string mapName =  System.Text.Encoding.Default.GetString(roomInfo.mapName);

		m_mapDataMgr.LoadByName(mapName);

		MapData mapData = m_mapDataMgr.GetData();

		if(mapData == null){
			Debug.LogError("Load map failed");
			return false;
		}
	
		float height = mapData.vLength;
		float width = mapData.hLength;

		GameObject back = GameObject.Find("BattleMapBackground");
		if(back == null){
			Debug.LogError("battle back is not found");
			return false;	
		}

		Transform backTrans = back.GetComponent<Transform>();
		if(backTrans == null){
			Debug.LogError("back trans is null");
			return false;
		}
		backTrans.localPosition = new Vector3(width/2f, height/2f, backTrans.localPosition.z);
		backTrans.localScale = new Vector3(width, height, 1);

		Renderer backRender = back.GetComponent<Renderer>();
		if(backRender == null){
			Debug.LogError("back render is null");
			return false;
		}

		float yNum = height/BACKGOURND_GRID_CELL_WIDTH;
		float xNum = width/BACKGOURND_GRID_CELL_WIDTH;

		backRender.material.mainTextureScale = new Vector2(xNum, yNum);

		return true;
	}

	void GetMineDetectRadius(){
	
		Vector3 screenTopRight = new Vector3 (Screen.width, Screen.height, 0);
		Vector3 screenBottomLeft = new Vector3 (0, 0, 0);

		Vector3 worldPoint1 = Camera.main.ScreenToWorldPoint (screenTopRight);
		Vector3 worldPoint2 = Camera.main.ScreenToWorldPoint (screenBottomLeft);

		Vector3 crossLen = worldPoint2 - worldPoint1;
		float radius = crossLen.magnitude * 1.5f;

		m_mineDetectRadius = radius;
	}


	bool SpBorn(){
		m_step = Step.WAIT_BORN;
		bool ret = new BornMessage().Send(delegate(IExtensible message){
			RoomMessage msg = (RoomMessage)message;
			if(msg.msgType != MSGID.Born_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}
			BornResponse rsp = msg.response.born;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			Ret csRet = rsp.ret;
			if(csRet == Ret.Success){
				m_step = Step.INIT_SCENE;
			}
			
			return true;
			
		});	
		return ret;	
	}


	bool SpInitScene(){
		m_step = Step.WAIT_INIT_SCENE;
		bool ret = new CSGetInitSyncMessage().Send(delegate(IExtensible message){
			RoomMessage msg = (RoomMessage)message;
			if(msg.msgType != MSGID.CSGetInitSync_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}

			CSGetInitSyncResponse rsp = msg.response.initSync;

			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			foreach(var user in rsp.users){
				int id = user.uid;
				m_playerInfos[id] = new GamePlayerInfo(id, user.name, user.photoId);
			}

			if(rsp.mineNum != rsp.mines.Count){
				Debug.LogError("mine num not matched");
				return false;
			}


			m_sync.m_mineMgr.InitFromInitSync(rsp.mines);

			m_step = Step.PLAY;

			return true;

		});
	
		return true;
	}

	void SpPlay(){

		m_prevJoyTime = HTime.NowTimeMs();
		m_isStart = true;
		m_sync.StartMove();
		m_step = Step.PLAYING;
	
	}

	
	private Vector3 m_preDir = new Vector3(0, 0, 0);
	private float m_preDis = 0;
	bool testOnce = false;


	// Update is called once per frame
	void Update () {
		switch(m_step){
		case Step.BORN:
			SpBorn();
			break;
		case Step.WAIT_BORN:
			break;
		case Step.INIT_SCENE:
			SpInitScene();
			break;
		case Step.WAIT_INIT_SCENE:
			break;
		case Step.PLAY:
			SpPlay();
			break;
		case Step.PLAYING:
			break;
		case Step.DEAD:
			return;		
		}
		if(!m_isStart){
			return;
		}

		if(!m_isStart){
			return;
		}

		Int64 currTime = HTime.NowTimeMs();
		if(currTime - m_prevJoyTime >= JOY_STICK_DELTA_MS && m_sync.m_isStart && m_joy.m_isDrag){
			Vector3 dir;
			float dis;
			int ret = m_joy.GetDirDis(out dir, out dis);
			if(ret != 0){
				Debug.LogError("get dir dis failed");
				return;			
			}		
			/*
			if (!testOnce) {
				new DirectMoveSyncUpMessage (new Vector3(1, 0, 0), 1.0f).Send ();
				testOnce = true;
			}

			if (m_sync.isSpeedZero) {
				m_sync.isSpeedZero = false;
				new DirectMoveSyncUpMessage (new Vector3(1, 1, 0), 1.0f).Send ();
			}*/

			//if ((dir - m_preDir).magnitude > 0.2 || Mathf.Abs (dis - m_preDis) > 0.3) {
			//if (m_joy.m_isMouseDrag) {
			if (dir.magnitude > 0.1 && dis > 0.1) {
				m_preDir = dir;
				m_preDis = dis;
			}
			new DirectMoveSyncUpMessage (dir, dis).Send ();
			m_upSpeedTxt.text = dir.x + ", " + dir.y;
			//Debug.LogWarning ("up speed:" + dir.x + ", " + dir.y);
			//}
			//}
			m_prevJoyTime = currTime;
		
		}
	}


	public void OnClickSplit(){
		Vector3 dir;
		float dis;
		int ret = m_joy.GetDirDis(out dir, out dis);
		if(ret != 0){
			Debug.LogError("get dir dis failed");
			return;			
		}	

		if (dir.magnitude < 0.1) {
			dir = m_preDir;
			if (dir.magnitude < 0.1) {
				dir = new Vector3 (1, 0, 0);			
			}

			dis = m_preDis;
		}

		Int64 now = HTime.GetNowServerTime();

		if(now < MoveSync.m_currFrameServerTime){
			now = MoveSync.m_currFrameServerTime;
		}else if(now > MoveSync.m_currFrameServerTime + MoveSync.SYNC_DELTA_MS){
			now = MoveSync.m_currFrameServerTime + MoveSync.SYNC_DELTA_MS;		
		}
	
		Debug.LogWarning ("split time, server start:" + MoveSync.m_currFrameServerTime + " curr server:" + now + " local time:" + HTime.GetTimeMs());


		Int64 prevFrameEndTime = MoveSync.m_currFrameServerTime + MoveSync.SYNC_DELTA_MS;


		new CSSplitMessage(dir, dis, now).Send(delegate(IExtensible msg) {

			RoomMessage message = (RoomMessage)msg;

			pbmsg.room.Response response = message.response;
			if(response == null){
				Debug.LogError("split response null");
				return false;			
			}

			CSSplitResponse split = response.split;

			MoveSync.GetInstance().ReceiveSplitPacket(split, dir, now, prevFrameEndTime);

			return true;

		});
	



	}

	public void OnClickExitRoom(){

		string msg = LanguageText.getStringByID ("STRINGS_EXIT_CONFIRM");

		eWarningWindow.Show (msg, 
			() =>{
				
				new CSExitGameMessage().Send (
					delegate(IExtensible message) {
						Debug.Log("exit succ");
						return true;
					});
			}

			, null);



	
	}





}


























