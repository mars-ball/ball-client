﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using SLua;

public class FriendScene : IDataListCanGetNum{

	enum FRIEND_TAB_INDEX{
		FRIEND_TAB_INDEX_LIST,
		FRIEND_TAB_INDEX_SEARCH,
		FRIEND_TAB_INDEX_TEAM,
		FRIEND_TAB_INDEX_MAX
	}

	public Dictionary<int, List<CSFriendInfo>> m_friendType;
	public List<CSFriendInfo> m_friends;
	public List<CSFriendInfo> m_searchFriendRes;


	public UITabPanel m_tabPanel;

	void Awake(){
		m_isDataReady = false;
		m_friendType = new Dictionary<int, List<CSFriendInfo>>();

		for(int ii = 0; ii < (int)UserSrcType.USER_SRC_TYPE_RENREN; ii++){
			m_friendType[ii] =  new List<CSFriendInfo>();
		}

		new CSGetFriendListMessage().Send(delegate(IExtensible msg) {

			HMessage message = (HMessage)msg;

			CSGetFriendListResponse response = message.response.getFriendList;

			if(response == null){
				Debug.LogError("response null , recv id :"+ (int)message.msgType);
				return false;
			}

			List<CSFriendInfo> friendInfos = response.friends;

			foreach(var friend in friendInfos){
				m_friendType[(int)friend.srcType].Add(friend);			
				m_friends.Add(friend);
			}

			m_isDataReady = true;
			return true;

		});

	}

	public override void Search(int typeIndex, string str){

		if((FRIEND_TAB_INDEX)typeIndex == FRIEND_TAB_INDEX.FRIEND_TAB_INDEX_SEARCH){

			int playerId;

			bool check = Int32.TryParse(str,out playerId);
			if(!check){
				Debug.LogError("check player id failed");
				return;
			}
			m_searchFriendRes.Clear ();


			new CSSeachFriendMessage(playerId, "").Send(delegate(IExtensible msg) {

				HMessage message = (HMessage)msg;

				CSSearchForPlayerResponse response = message.response.searchPlayer;

				if(response == null){
					Debug.LogError("response null , recv id :"+ (int)message.msgType);
					return false;
				}

				if(response.ret != Ret.Success || response.friend == null){
					return false;
				}

				m_searchFriendRes.Add(response.friend);

				m_tabPanel.ePanels[typeIndex].RefreshAfterSearch(typeIndex);
				return true;

			});
		
		}
	}


	void Start(){
	
	}


	public override int GetDataListNum(int typeIndex){
		
		switch((FRIEND_TAB_INDEX)typeIndex){
		case FRIEND_TAB_INDEX.FRIEND_TAB_INDEX_LIST:
			return m_friends.Count;
			break;
		case FRIEND_TAB_INDEX.FRIEND_TAB_INDEX_SEARCH:
			return m_searchFriendRes.Count;
			break;
		}
		
		return 0;

	}

	public override System.Object GetDataByIndex(int typeIndex, int index){

		switch((FRIEND_TAB_INDEX)typeIndex){
		case FRIEND_TAB_INDEX.FRIEND_TAB_INDEX_LIST:
			if(index < 0 || index >= m_friends.Count){
				Debug.Log("index exceeds, index:" + index);
				return m_friends[index];		
			}

			return m_friends[index];
			break;
		case FRIEND_TAB_INDEX.FRIEND_TAB_INDEX_SEARCH:
			if(index < 0 || index >= m_searchFriendRes.Count){
				Debug.Log("index exceeds, index:" + index);
				return m_friends[index];		
			}

			return m_searchFriendRes[index];
			break;
		}

		return null;

	}

	public void OnClickGoMainMenu(){
		SceneManager.LoadScene("Login");

	}

}
