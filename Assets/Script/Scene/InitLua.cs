﻿using UnityEngine;
using System.Collections;

public class InitLua : LuaBehaviour {

	protected string _name = "init_lua.lua";

	void Awake(){
		StartCoroutine(Do());
	}


	public IEnumerator Do(){
		DoFile(_name,null); 
		yield return null;
	}

}
