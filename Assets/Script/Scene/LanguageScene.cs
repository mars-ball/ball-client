﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using localSave;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;

public class LanguageScene : MonoBehaviour {

	void Awake(){
	}

	public void OnClickLanguage(int lan){
	
		UserBasicDataManager.GetInstance().GetData().language = (UserLanguage)lan;

		UserBasicDataManager.GetInstance ().Save ();

		LanguageDataManager.GetInstance ().Load ();


		CSChangeInfoMessage.ChangeParam param = new CSChangeInfoMessage.ChangeParam ();
		param.name = name;

		param.lan = (UserLanguage)lan;

		new CSChangeInfoMessage (CSChangeInfoMessage.ChangeInfoType.CHANGE_INFO_TYPE_LANGUAGE, param).Send (
			delegate(IExtensible message){
				HMessage msg = (HMessage)message;
				if(msg.msgType != MSGID.CSChangeInfo_Response){
					Debug.LogError ("msg id error " + msg.msgType);
					return false;
				}

				CSChangeInfoResponse rsp = msg.response.changeInfo;
				if(rsp == null){
					Debug.LogError ("create user rsp null ");
					return false;			
				}
				if(rsp.ret == Ret.Success){
					Debug.LogError("change name success");
				}else{
					Debug.LogError("change name failed");				
				}
				SceneManager.LoadScene ("Login");
				return true;

			}
		);



	}

}
