﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.IO;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;
using SLua;
using ItemType=StoreDataManager.ITEM_TYPE;
using PriceType=StoreDataManager.PRICE_TYPE;
using tnt_deploy;


public class ShopScene : MonoBehaviour {

	public RectTransform eContent;

	public GridLayoutGroup eContentGrid;

	public StoreScrollManager eScrollMgr;

	public GameObject eCoinBtn;
	public GameObject ePhotoBtn;

	private Dictionary<ItemType, Image> m_tabs;

	public HashSet<int> m_userPack;

	public Text m_CoinText;

	public ItemType m_currSel;

	private IAP m_IAP;

	public static readonly Color TAB_ACTIVE_COLOR = new Color(241f/255f, 211f/255f, 172f/255f, 1);
	public static readonly Color TAB_DEACTIVE_COLOR = new Color(222f/255f, 222f/255f, 222f/255f, 1);
	public readonly static float ITEM_HV_ASPECT = 0.6f;
	public readonly static float SPACING_ASPECT = 0.05f;

	public bool m_isPackLoad;

	void Awake(){

		StoreDataManager.GetInstance().Load();

		Image coinImage = eCoinBtn.GetComponent<Image>();
		Image photoImage = ePhotoBtn.GetComponent<Image>();

		m_tabs = new Dictionary<ItemType, Image>();

		m_tabs[ItemType.ITEM_TYPE_GOLD] = coinImage;
		m_tabs[ItemType.ITEM_TYPE_PHOTO] = photoImage;

		EventTriggerListener.Get(eCoinBtn).onDown =  OnClickCoinPanal;
		EventTriggerListener.Get(ePhotoBtn).onDown =  OnClickPhotoPanal;

		eScrollMgr.m_shop = this;

		m_userPack = new HashSet<int>();

		m_isPackLoad = false;

		StartCoroutine(IStartShow());

	}
		
	public IEnumerator IStartShow(){

		bool ret = new CSGetUserPackMessage().Send(delegate(IExtensible message){
			HMessage msg = (HMessage)message;
			if(msg.msgType != MSGID.CSGetUserPack_Response){
				Debug.LogError ("msg id error " + msg.msgType);
				return false;
			}

			CSGetUserPackResponse rsp = msg.response.userPack;
			if(rsp == null){
				Debug.LogError ("create user rsp null ");
				return false;			
			}

			int gold = rsp.gold;

			m_CoinText.text = gold.ToString();

			UserPackData pack = rsp.userPack;

			foreach(var item in pack.items){

				m_userPack.Add(item.storeId);

			}
			m_isPackLoad = true;
			return true;
		});

		while(!m_isPackLoad){
			yield return null;
		}

		while (!IAP.getInstance ().m_isInit) {
			yield return null;
		}

		RefreshScroll(ItemType.ITEM_TYPE_GOLD);
	}



	public void RefreshScroll(ItemType type){
		m_currSel = type;

		foreach(var tabPair in m_tabs){

			if(tabPair.Key == type){
				tabPair.Value.color = TAB_ACTIVE_COLOR;			
			}else{
				tabPair.Value.color = TAB_DEACTIVE_COLOR;
			}	
		}



		int num = StoreDataManager.GetInstance().m_itemsByType[type].Count;
		float itemHeight = eContent.rect.height;
		float itemWidth = itemHeight * ITEM_HV_ASPECT;
		float itemSpacing = itemWidth * SPACING_ASPECT;

		eContentGrid.cellSize = new Vector2(itemWidth, itemHeight);
		eContentGrid.spacing = new Vector2(itemSpacing, 0);

		float contentWidth = itemWidth * num;

		eContent.sizeDelta = new Vector2(contentWidth, 0);

		eScrollMgr.MakeList(type);
	
	}



	public void OnClickCoinPanal(GameObject obj){
		RefreshScroll(ItemType.ITEM_TYPE_GOLD);
	}

	public void OnClickPhotoPanal(GameObject obj){
		RefreshScroll(ItemType.ITEM_TYPE_PHOTO);
	}

	public void OnClickGoMainMenu(){
		SceneManager.LoadScene("Login");
	}

}
