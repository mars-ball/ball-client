﻿using UnityEngine;
using System.Collections;

public class TestTiling22 : MonoBehaviour {
	
	public Renderer rend;

	void Start() {
		rend = GetComponent<Renderer>();
	}

	void Update() {
		float scaleX = 10f;//Mathf.Cos(Time.time) * 0.5F + 1;
		float scaleY = 10f;//Mathf.Sin(Time.time) * 0.5F + 1;
		rend.material.mainTextureScale = new Vector2(scaleX, scaleY);
	}
}
