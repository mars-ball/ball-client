﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace ModelViewer{

	class SceneObject{	
	}


	class SceneData{

		public SceneObject m_sceneObj;

		public AsyncOperation Async{ get; set; }
		
		//public bool IsLoading{ get; set;}

		//public bool IsLoaded{ get; set;}
		
		public bool IsFinishLoading(){
			/*if (!IsLoading) {
					return false;
			}*/
			if (Async != null && Async.progress >= 100) {
					return true;
			}
			return false;
		}		
			
	}


	public class SceneManager:MonoBehaviour{

		private static SceneManager s_instance;

		private static GameObject s_instanceObject;

		private static readonly float LOAD_ACTIVATION_PERCENT = 0.9f;

		private static readonly int MAX_KEEP_SCENE_NUM = 3;

		private List<SceneId> m_sceneCacheId;

		private Dictionary<SceneId, SceneData> m_sceneCache;

		private SceneId m_prevSceneId;

		private SceneId m_currSceneId;

		private SceneId m_loadingSceneId;

		private SceneData m_loadingSceneData;

		private LoadState m_loadState;


		public static SceneManager Instance{
			get{
				if(s_instance == null){
				
					s_instanceObject = new GameObject("SceneManager");
					s_instance = s_instanceObject.AddComponent<SceneManager>();				
				}
				return s_instance;			
			}		
		}

		public void InitMembers(){
			m_sceneCacheId = new List<SceneId> ();
			m_sceneCache = new Dictionary<SceneId, SceneData> ();
			m_prevSceneId = SceneId.None;
			m_currSceneId = SceneId.None;
		}

		void Awake(){
			InitMembers ();			
		
		}

		void Start(){
		}

		public bool LoadScene(SceneId id){ 
			if (!((int)id > (int)SceneId.None && (int)id < (int)SceneId.Max)) {
				Debug.Log ("invalide scene");
				return false;				
			}

			if (m_loadState == LoadState.Loading) {
				Debug.Log ("is loading");
				return false;
			}

			if (id == m_currSceneId) {
				Debug.Log ("already in scene " + id);
				return false;
			}
			m_loadState = LoadState.Loading;
			StartCoroutine (LoadingProgress(id));		
			return true;
		}
		private IEnumerator LoadingProgress(SceneId id){
			
			SceneData obj =  m_loadingSceneData = new SceneData ();

			m_loadingSceneData.Async = Application.LoadLevelAdditiveAsync (id.GetLabel ());
			m_loadingSceneData.Async.allowSceneActivation = false;

			while(!m_loadingSceneData.Async.isDone){
				if(!obj.Async.isDone){
					if(obj.Async.progress >= LOAD_ACTIVATION_PERCENT){
						obj.Async.allowSceneActivation = true;					
					}
				}
				yield return null;
			}

			while (obj.m_sceneObj == null) {
				yield return null;			
			}

			m_loadState = LoadState.Loaded;
		}
	}






}





















