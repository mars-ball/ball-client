﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using pbmsg;
using pbmsg.login;
using UnityEngine.SceneManagement;

public class MyProfileController : MonoBehaviour {

	public Image ImgAvatar;
	public Text LblUserID;
	public Text LblUserName;
	public Text LblUserLevel;
	public Text LblUserCoins;

	// Use this for initialization
	void Awake () {

		UserInfo info = UserManager.GetInstance().GetMainUser();
//		info.mBasicInfo.name = userInfo.name;
//		info.mBasicInfo.photo = userInfo.photo;
//		info.mBasicInfo.lv = userInfo.lv;
//		info.mBasicInfo.gold = userInfo.gold;
//		info.mHasNotif = (response.hasNotif > 0);
//
		LblUserID.text = info.mBasicInfo.uid.ToString();
		LblUserName.text = info.mBasicInfo.name;
		LblUserLevel.text = info.mBasicInfo.lv.ToString();
		LblUserCoins.text = info.mBasicInfo.gold.ToString();

		Texture2D photoTex = PhotoGen.GetInstance().getPhotoTexture(info.mBasicInfo.photo);
		ImgAvatar.sprite=Sprite.Create (photoTex, new Rect (0, 0, photoTex.width, photoTex.height), new Vector2 (0.5f, 0.5f));
	}

	public void BackToMain() {
		SceneManager.LoadScene ("Login");

	}

}
