﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using ProtoBuf;
using pbmsg;
using pbmsg.login;
using localSave;

public class RegisterAccountScene : MonoBehaviour {

	public InputField eEmailInput;
	public InputField ePasswordInput;
	public Text eWarningText;
	public Text eErrorMsg;

	public const int EMAIL_LEN = 50;
	public const int PWD_LEN = 15;


	// Use this for initialization
	void Start () {
	
	}

	bool IsValidEmail(string email){
		try{
			var addr = new System.Net.Mail.MailAddress(email);
			return addr.Address == email;
		}catch{
			return false;
		}
	
	}


	public void OnClickReg(){

		string email = eEmailInput.text;
		string password = ePasswordInput.text;
		if (email.Length < 0 || email.Length >= EMAIL_LEN) {
			eWarningText.text = "Input Length error";
		}

		if (email.Contains ("@") == false) {
			eErrorMsg.text = LanguageText.getStringByID ("STRINGS_INVALID_EMAIL");
			return;
		}

		if (password.Length < 0 || password.Length >= PWD_LEN) {
			eWarningText.text = "Password Length error";
		}

		bool ret = new CSRegMailMessage(email, password).Send (delegate(IExtensible msg) {
			HMessage message = (HMessage)msg;

			CSRegMailResponse response = message.response.regMail;
			if(response == null){
				Debug.LogError("response null , recv id :"+ (int)message.msgType);
				return false;			
			}

			if(response.ret == Ret.Success){
				UserBasicDataManager.GetInstance().GetData().mail = email;
				UserBasicDataManager.GetInstance().Save();
				SceneManager.LoadScene("MyPage");
			}else{
				Debug.LogError("Register failed");			
				eErrorMsg.text = LanguageText.getStringByID ("STRINGS_EXISTING_EMAIL");
			}



			return true;
		});

	}

	public void OnClickBack(){

		SceneManager.LoadScene ("Login");
	
	}

	public void OnClickLogin(){
	
		SceneManager.LoadScene ("LoginAccount");
	}
}
