[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

## A Unity MOBA Game 

This is the front-end of a multiplayer online game similar to 'Agar.io', developed in Unity.
The backend is located at https://gitlab.com/mars-ball/ball-server


1. Communicates with the server via TCP, using a communication protocol based on Protobuf. Protobuf is used to directly generate data definition files from the communication protocol (C# files for the client and C++ files for the server).
2. Some parts of the UI are written in Lua, allowing for hot updates.


[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555


[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-url]: https://www.linkedin.com/in/shaokun-feng/